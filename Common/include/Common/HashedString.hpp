/*
 * HashedString.h
 *
 *  Created on: 01/06/2014
 *      Author: Atridas
 */

#ifndef COMMON_HASHEDSTRING_H_
#define COMMON_HASHEDSTRING_H_

#include <cstdint>
#include <string>

#include "CompileDefines.hpp"

namespace qdrg {

class HashedString {
public:
	HashedString();
	HashedString(const std::string&);
	HashedString(const char*);
	HashedString(uint32_t);

	//HashedString(const HashedString& ) = default;
	//HashedString(HashedString&&) = default;
	~HashedString() {};

	//HashedString& operator=(const HashedString& ) = default;
	//HashedString& operator=(HashedString&&) = default;

	bool operator< (const HashedString& _Other) const { return m_CRC <  _Other.m_CRC; };
	bool operator<=(const HashedString& _Other) const { return m_CRC <= _Other.m_CRC; };
	bool operator==(const HashedString& _Other) const { return m_CRC == _Other.m_CRC; };
	bool operator>=(const HashedString& _Other) const { return m_CRC >= _Other.m_CRC; };
	bool operator> (const HashedString& _Other) const { return m_CRC >  _Other.m_CRC; };
	bool operator!=(const HashedString& _Other) const { return m_CRC != _Other.m_CRC; };

	uint32_t CRC() const { return m_CRC; };

	bool IsEmptyString() const { return m_CRC == 0; }

	const std::string& GetString() const
#if QUADRIGA_DEBUG
	{ return *m_String; }
#else
	;
#endif

private:

	uint32_t m_CRC;

#if QUADRIGA_DEBUG
	std::string* m_String;
#endif

};

} /* namespace qdrg */

namespace std {

template<>
struct hash<qdrg::HashedString>
{
	inline size_t operator()(const qdrg::HashedString& v) const
	{
		return v.CRC();
	}
};

} /* namespace std */

#endif /* COMMON_HASHEDSTRING_H_ */
