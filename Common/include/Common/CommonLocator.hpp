#pragma once

#include "Common.hpp"

namespace qdrg {

	class InputManager;

	class CommonLocator
	{
	public:
		class ClockCreator
		{
		public:
			virtual ~ClockCreator() {};

			virtual std::unique_ptr<Clock> CreateClock() = 0;
		};


		// Configuration. This should be called at engine initialization
		static void SetClockCreator(std::unique_ptr<ClockCreator>&& _ClockCreator);
		static void ProvideDefaultLogger(std::unique_ptr<Logger>&& logger);
		static void ProvideLogger(const HashedString& name, std::unique_ptr<Logger>&& logger);
		static void ProvideInputManager(std::unique_ptr<InputManager>&& inputManager);

		// Clean up
		static void CleanAllLoggers();

		// Getters. The whole point of having a Locator
		static std::unique_ptr<Clock> CreateClock();

		static Logger& GetLogger(const HashedString& name) { assert(s_DefaultLogger.get() != nullptr); auto it = s_Loggers.find(name); return (it == s_Loggers.end()) ? *s_DefaultLogger : *it->second; }
		static InputManager& GetInputManager() { assert(s_InputManager.get() != nullptr); return *s_InputManager.get(); };
		
	private:

		static std::unique_ptr<Logger> s_DefaultLogger;
		static std::unordered_map<HashedString, std::unique_ptr<Logger>> s_Loggers;

		static std::unique_ptr<InputManager> s_InputManager;
	};

}
