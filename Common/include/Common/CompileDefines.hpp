/*
 * Common.hpp
 *
 *  Created on: 29/05/2014
 *      Author: Atridas
 */

#ifndef COMMON_COMMON_DEFINES_HPP_
#define COMMON_COMMON_DEFINES_HPP_

#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif

#define PLATFORM_PC


// visual studio
#define constexpr
#define ALIGNMENT_16B __declspec(align(128))


#define QUADRIGA_DEBUG 1


#define QUADRIGA_DEBUG_MEMORY 1

#endif /* COMMON_COMMON_DEFINES_HPP_ */
