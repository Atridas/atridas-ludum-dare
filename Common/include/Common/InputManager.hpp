#pragma once

#include "Common.hpp"
#include "EventSystem.hpp"

namespace qdrg {

	class InputManager
	{
	public:
		InputManager() {};
		virtual ~InputManager() {};

		Event<>& GetEvent(HashedString _eventName);
		Event<float>& GetEventWithValue(HashedString _eventName);
		Event<const glm::vec2&>& GetEventWith2Value(HashedString _eventName);
		Event<const glm::vec3&>& GetEventWith3Value(HashedString _eventName);
		Event<const glm::vec4&>& GetEventWith4Value(HashedString _eventName);

		bool IsEventActive(HashedString _eventName) const { return m_ActiveEvents.count(_eventName) == 1; }

	protected:

		void CallEvent(HashedString _eventName, float value = 1.0f);
		void CallEvent(HashedString _eventName, const glm::vec2& value);
		void CallEvent(HashedString _eventName, const glm::vec3& value);
		void CallEvent(HashedString _eventName, const glm::vec4& value);
		void ActivateEvent(HashedString _eventName);
		void DeactivateEvent(HashedString _eventName);

	private:
		InputManager(const InputManager&){ assert(false); };
		const InputManager& operator=(const InputManager&) { assert(false); return *this; };

		std::unordered_map<HashedString, Event<>> m_Events;
		std::unordered_map<HashedString, Event<float>> m_EventsWithValue;
		std::unordered_map<HashedString, Event<const glm::vec2&>> m_EventsWith2Value;
		std::unordered_map<HashedString, Event<const glm::vec3&>> m_EventsWith3Value;
		std::unordered_map<HashedString, Event<const glm::vec4&>> m_EventsWith4Value;
		std::unordered_set<HashedString> m_ActiveEvents;
	};

}
