#pragma once

#include "Common.hpp"

#include <sstream>

#include "Clock.hpp"

namespace qdrg {

	class Logger {
	public:

		enum Level {
			VERVOSE,
			DEBUG,
			INFO,
			WARN,
			ERROR,
			ASSERT
		};

		struct LogMessage {
			Level level;
			std::string file;
			int line;
			Time time;
			std::string tag;
			std::string message;
		};

		virtual ~Logger() {};
		Logger(const Logger&) = delete; // no copy
		Logger& operator=(const Logger&) = delete;

		void Log(Level level, const std::string& file, int line, const Time& time, const std::string& tag, const std::string& message)
		{
			LogMessage lm;
			lm.level = level;
			lm.file = file;
			lm.line = line;
			lm.time = time;
			lm.tag = tag;
			lm.message = message;
			Log(std::move(lm));
		}

		void Log(Level level, const std::string& file, int line, const std::string& tag, const std::string& message) { Log(level, file, line, m_Clock->GetElapsedTime(), tag, message); };
		void Log(Level level, const std::string& file, int line, const std::string& tag, const std::stringstream& message) { Log(level, file, line, m_Clock->GetElapsedTime(), tag, message.str()); };
		void Log(Level level, const std::string& file, int line, const std::string& tag, const char* message) { Log(level, file, line, m_Clock->GetElapsedTime(), tag, std::string(message)); };

	protected:

		Logger(const std::shared_ptr<const Clock>& clock) :m_Clock(clock) {}

		virtual void Log(LogMessage&& logMessage) = 0;

	private:

		std::shared_ptr<const Clock> m_Clock;

	};
}

#define LOG(loggerName, level, tag, message)    qdrg::CommonLocator::GetLogger(loggerName).Log(level, __FILE__, __LINE__, message)

#define LOG_V(loggerName, tag, message)    qdrg::CommonLocator::GetLogger(loggerName).Log(qdrg::Logger::VERVOSE, __FILE__, __LINE__, tag, message)
#define LOG_D(loggerName, tag, message)    qdrg::CommonLocator::GetLogger(loggerName).Log(qdrg::Logger::DEBUG  , __FILE__, __LINE__, tag, message)
#define LOG_I(loggerName, tag, message)    qdrg::CommonLocator::GetLogger(loggerName).Log(qdrg::Logger::INFO   , __FILE__, __LINE__, tag, message)
#define LOG_W(loggerName, tag, message)    qdrg::CommonLocator::GetLogger(loggerName).Log(qdrg::Logger::WARN   , __FILE__, __LINE__, tag, message)
#define LOG_E(loggerName, tag, message)    qdrg::CommonLocator::GetLogger(loggerName).Log(qdrg::Logger::ERROR  , __FILE__, __LINE__, tag, message)
#define LOG_A(loggerName, tag, message)    qdrg::CommonLocator::GetLogger(loggerName).Log(qdrg::Logger::ASSERT , __FILE__, __LINE__, tag, message)
