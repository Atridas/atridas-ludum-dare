#include <vector>
#include <algorithm>

namespace qdrg {

	template<class... Args> class Event;

	class BaseDelegate
	{
	public:
		virtual ~BaseDelegate(){ }
	};

	template<class... Args> class AbstractDelegate : public BaseDelegate
	{
	protected:
		virtual ~AbstractDelegate();

		friend class Event<Args...>;

		void add(Event<Args...> *s){ v.push_back(s); }
		void remove(Event<Args...> *s){ v.erase(std::remove(v.begin(), v.end(), s), v.end()); }

		virtual void call(Args... args) = 0;

		std::vector<Event<Args...>*> v;
	};

	template<class T, class... Args> class ClassDelegate : public AbstractDelegate<Args...>
	{
	public:
		ClassDelegate(T *t, void(T::*f)(Args...), Event<Args...> &s);

	private:
		ClassDelegate(const ClassDelegate&);
		void operator=(const ClassDelegate&);

		friend class Event<Args...>;

		virtual void call(Args... args){ (t->*f)(args...); }

		T *t;
		void(T::*f)(Args...);
	};

	template<class... Args> class FunctionDelegate : public AbstractDelegate<Args...>
	{
	public:
		FunctionDelegate(std::function<void(Args...)> f, Event<Args...> &s);

	private:
		FunctionDelegate(const FunctionDelegate&);
		void operator=(const FunctionDelegate&);

		friend class Event<Args...>;

		virtual void call(Args... args){ f(args...); }

		std::function<void(Args...)> f;
	};

	template<class... Args> class Event
	{
	public:
		Event(){ }
		~Event(){ for (auto i : v) i->remove(this); }

		void connect(AbstractDelegate<Args...> &s){ v.push_back(&s); s.add(this); }
		void disconnect(AbstractDelegate<Args...> &s){ v.erase(std::remove(v.begin(), v.end(), &s), v.end()); }

		void operator()(Args... args){ for (auto i : v) i->call(args...); }

	private:
		Event(const Event&);
		void operator=(const Event&);

		std::vector<AbstractDelegate<Args...>*> v;
	};

	template<class... Args> AbstractDelegate<Args...>::~AbstractDelegate()
	{
		for (auto i : v) i->disconnect(*this);
	}

	template<class T, class... Args> ClassDelegate<T, Args...>::ClassDelegate(T *t, void(T::*f)(Args...), Event<Args...> &s) : t(t), f(f)
	{
		s.connect(*this);
	}

	template<class... Args> FunctionDelegate<Args...>::FunctionDelegate(std::function<void(Args...)> f, Event<Args...> &s) : f(f)
	{
		s.connect(*this);
	}

	class Delegate
	{
	public:
		Delegate(){ }
		~Delegate(){ for (auto i : v) delete i; }

		template<class... Args> void connectFunction(std::function<void(Args...)> &f, Event<Args...> &s){ v.push_back(new FunctionDelegate<Args...>(f, s)); }
		template<class T, class... Args> void connectObject(T *t, void(T::*f)(Args...), Event<Args...> &s){ v.push_back(new ClassDelegate<T, Args...>(t, f, s)); }

	private:
		Delegate(const Delegate&);
		void operator=(const Delegate&);

		std::vector<BaseDelegate*> v;
	};
}