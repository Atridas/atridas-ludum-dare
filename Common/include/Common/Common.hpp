/*
 * Common.hpp
 *
 *  Created on: 29/05/2014
 *      Author: Atridas
 */

#ifndef COMMON_COMMON_HPP_
#define COMMON_COMMON_HPP_

#include <cstdint>
#include <limits>
#include <memory>

#include <map>
#include <mutex>
#include <set>
#include <string>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include "CompileDefines.hpp"


#include "HashedString.hpp"
#include "Time.hpp"
#include "Clock.hpp"
#include "Logger.hpp"

#include "CommonLocator.hpp"


namespace qdrg {


	static const uint64_t s_LogicTimestepMicros = 31250;
	static const float s_LogicTimestep = (float)(s_LogicTimestepMicros / 1000000.0);
	//static_assert(s_LogicTimestep == 0.033333f, "round errors");


	static HashedString s_AllocatorLog("Allocator");


	static inline glm::quat rotate(const glm::quat& original, const glm::vec3& angular_speed, float time) { return glm::normalize(original + 0.5f * glm::quat(0, angular_speed.x, angular_speed.y, angular_speed.z) * original * time); }
	// less slow
	static inline glm::quat rotate_fast(const glm::quat& original, const glm::vec3& angular_speed, float time) { return original + 0.5f * glm::quat(0, angular_speed.x, angular_speed.y, angular_speed.z) * original * time; }

	std::string LoadFileToString(const std::string& _Path);
};

#endif /* COMMON_COMMON_HPP_ */
