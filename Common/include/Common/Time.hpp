
#ifndef COMMON_TIME_HPP_
#define COMMON_TIME_HPP_

#include "Common.hpp"

namespace qdrg {

	class Time
	{
	public:
		Time() :m_Microseconds(0) {}
		Time(uint64_t _Microseconds) :m_Microseconds(_Microseconds) {}

		float GetAsSeconds() const { return (float)(((double)m_Microseconds) / 1000000.0); }
		uint32_t GetAsMilliseconds() const { return (uint32_t)(m_Microseconds / 1000); };
		uint64_t GetAsMicroseconds() const { return m_Microseconds; };

	private:

		uint64_t m_Microseconds;
	};
}

#endif
