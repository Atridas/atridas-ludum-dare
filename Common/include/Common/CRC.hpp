/*
 * CRC.h
 *
 *  Created on: 01/06/2014
 *      Author: Atridas
 */

#ifndef COMMON_CRC_H_
#define COMMON_CRC_H_

#include "Common.hpp"

namespace qdrg {

class CRC {
public:
	CRC(const CRC&) = delete;
	CRC& operator=(const CRC&) = delete;

	static CRC& Instance();

	uint64_t CRC64(const uint8_t* data, uint64_t size);
	uint32_t CRC32(const uint8_t* data, uint64_t size);

private:
	CRC();
	~CRC() {};

	void InitLookupTable64(uint64_t _Polygon);
	void InitLookupTable32(uint32_t _Polygon);

	uint64_t m_LookupTable64[0x100];
	uint32_t m_LookupTable32[256];
};

} /* namespace qdrg */
#endif /* COMMON_CRC_H_ */
