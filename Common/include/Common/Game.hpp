#pragma once

namespace qdrg {

	class GLWrapper;

	// implement this to create the game
	namespace Game
	{
		void Initialize(GLWrapper& _wrapper);
		void Dispose(GLWrapper& _wrapper);

		void Update();
		void Render(float forward, GLWrapper& wrapper);

		glm::vec2 GetWindowSize(); // but this. This is implemented by platform wrapper
	};

}
