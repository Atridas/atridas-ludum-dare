
#ifndef COMMON_CLOCK_HPP_
#define COMMON_CLOCK_HPP_

#include "Common.hpp"

#include "Time.hpp"

namespace qdrg {

	// This class should be implemented on a per-platform basis
	class Clock
	{
	public:
		virtual ~Clock() {};

		// elapsed time since the last "restart" call
		virtual Time GetElapsedTime() const = 0;

		//  Restart the clock
		virtual Time Restart() = 0;

	protected:
		Clock() {};
	};
}

#endif
