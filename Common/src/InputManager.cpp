#include "stdafx.h"
#include "InputManager.hpp"

namespace qdrg {

	Event<>& InputManager::GetEvent(HashedString _eventName)
	{
		return m_Events[_eventName];
	}

	Event<float>& InputManager::GetEventWithValue(HashedString _eventName)
	{
		return m_EventsWithValue[_eventName];
	}

	Event<const glm::vec2&>& InputManager::GetEventWith2Value(HashedString _eventName)
	{
		return m_EventsWith2Value[_eventName];
	}

	Event<const glm::vec3&>& InputManager::GetEventWith3Value(HashedString _eventName)
	{
		return m_EventsWith3Value[_eventName];
	}

	Event<const glm::vec4&>& InputManager::GetEventWith4Value(HashedString _eventName)
	{
		return m_EventsWith4Value[_eventName];
	}

	void InputManager::CallEvent(HashedString _eventName, float value)
	{
		m_Events[_eventName]();
		m_EventsWithValue[_eventName](value);
	}

	void InputManager::CallEvent(HashedString _eventName, const glm::vec2& value)
	{
		m_Events[_eventName]();
		m_EventsWith2Value[_eventName](value);
	}

	void InputManager::CallEvent(HashedString _eventName, const glm::vec3& value)
	{
		m_Events[_eventName]();
		m_EventsWith3Value[_eventName](value);
	}

	void InputManager::CallEvent(HashedString _eventName, const glm::vec4& value)
	{
		m_Events[_eventName]();
		m_EventsWith4Value[_eventName](value);
	}

	void InputManager::ActivateEvent(HashedString _eventName)
	{
		//assert(m_ActiveEvents.count(_eventName) == 0);
		m_ActiveEvents.insert(_eventName);
	}

	void InputManager::DeactivateEvent(HashedString _eventName)
	{
		//assert(m_ActiveEvents.count(_eventName) == 1);
		m_ActiveEvents.erase(_eventName);
	}
}
