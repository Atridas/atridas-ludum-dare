/*
 * CRC.cpp
 *
 *  Created on: 01/06/2014
 *      Author: Atridas
 */

#include "stdafx.h"

#include "CRC.hpp"

namespace qdrg {

CRC::CRC() {
	InitLookupTable64(0xA17870F5D4F51B49L);
	InitLookupTable32(0xEDB88320);
}


void CRC::InitLookupTable64(uint64_t _Polygon)
{
	for(int i = 0; i < 0x100; i++) {
		uint64_t crc = i;
		for(int j = 0; j < 8; j++) {
			if((crc & 1) == 1) {
				crc = (crc >> 1) ^ _Polygon;
			} else {
				crc = (crc >> 1);
			}
		}
		m_LookupTable64[i] = crc;
	}
}

void CRC::InitLookupTable32(uint32_t _Polygon)
{
	for(int i = 0; i < 0x100; i++) {
		uint32_t crc = i;
		for(int j = 0; j < 8; j++) {
			if((crc & 1) == 1) {
				crc = (crc >> 1) ^ _Polygon;
			} else {
				crc = (crc >> 1);
			}
		}
		m_LookupTable32[i] = crc;
	}
}

CRC& CRC::Instance()
{
	static CRC *instance = new CRC();
	return *instance;
}

uint64_t CRC::CRC64(const uint8_t* data, uint64_t size)
{
	uint64_t checksum = 0;
	for(uint64_t i = 0; i < size; i++) {
		const uint32_t lookupIdX = (uint32_t) ((checksum ^ data[i]) & 0xff);
		checksum = (checksum >> 8) ^ m_LookupTable64[lookupIdX];
	}

	return checksum;
}

uint32_t CRC::CRC32(const uint8_t* data, uint64_t size)
{
	uint32_t checksum = 0;
	for(uint64_t i = 0; i < size; i++) {
		const uint32_t lookupIdX = (uint32_t) ((checksum ^ data[i]) & 0xff);
		checksum = (checksum >> 8) ^ m_LookupTable32[lookupIdX];
	}

	return checksum;
}

} /* namespace qdrg */
