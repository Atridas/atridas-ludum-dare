/*
 * HashedString.cpp
 *
 *  Created on: 01/06/2014
 *      Author: Atridas
 */

#include "stdafx.h"

#include "HashedString.hpp"

#include <string.h>

#include "CRC.hpp"

namespace qdrg {


static std::string s_EmptyString = "";

class HashedString_HashMap : public std::unordered_map<uint32_t, std::string>
{
public:
	HashedString_HashMap() {
		(*this)[0] = s_EmptyString;
	}
};


std::unordered_map<uint32_t, std::string>&  HashedString_StringMap()
{
	static HashedString_HashMap stringMap;
	return stringMap;
}

HashedString::HashedString()
:m_CRC(0)
{
#if QUADRIGA_DEBUG
	m_String = &s_EmptyString;
#endif
}

HashedString::HashedString(const std::string& _Str)
:m_CRC(CRC::Instance().CRC32((uint8_t*)_Str.c_str(), _Str.length()))
{
	std::unordered_map<uint32_t, std::string>& map = HashedString_StringMap();
	assert(map.find(m_CRC) == map.end() || map[m_CRC] == _Str);

	map[m_CRC] = _Str;

#if QUADRIGA_DEBUG
	m_String = &(map[m_CRC]);
#endif
}

HashedString::HashedString(const char* _Str)
:m_CRC(CRC::Instance().CRC32((uint8_t*)_Str, strlen(_Str)))
{
	std::unordered_map<uint32_t, std::string>& map = HashedString_StringMap();
	assert(map.find(m_CRC) == map.end() || map[m_CRC] == std::string(_Str));

	map[m_CRC] = _Str;

#if QUADRIGA_DEBUG
	m_String = &(map[m_CRC]);
#endif
}

HashedString::HashedString(uint32_t _CRC)
:m_CRC(_CRC)
{
#if QUADRIGA_DEBUG
	m_String = &s_EmptyString;
#endif
}


#if !QUADRIGA_DEBUG
const std::string& HashedString::GetString() const
{
	auto it = HashedString_StringMap().find(m_CRC);
	if(it != HashedString_StringMap().end())
	{
		return *it;
	}
	else
	{
		return "";
	}
}
#endif

} /* namespace qdrg */
