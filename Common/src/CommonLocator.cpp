#include "stdafx.h"
#include "CommonLocator.hpp"
#include "InputManager.hpp"

namespace qdrg {

	static std::unique_ptr<CommonLocator::ClockCreator> s_ClockCreator;

	void CommonLocator::SetClockCreator(std::unique_ptr<ClockCreator>&& _ClockCreator)
	{
		s_ClockCreator = std::move(_ClockCreator);
	}

	// --------------------------------------------------------------------------------------------------------------------
	

	void CommonLocator::ProvideDefaultLogger(std::unique_ptr<Logger>&& logger)
	{
		s_DefaultLogger = std::move(logger);
	}

	void CommonLocator::ProvideLogger(const HashedString& name, std::unique_ptr<Logger>&& logger)
	{
		s_Loggers[name] = std::move(logger);
	}

	std::unique_ptr<Clock> CommonLocator::CreateClock()
	{
		return s_ClockCreator->CreateClock();
	}

	void CommonLocator::ProvideInputManager(std::unique_ptr<InputManager>&& inputManager)
	{
		s_InputManager = std::move(inputManager);
	}


	void CommonLocator::CleanAllLoggers()
	{
		s_DefaultLogger.reset();
		s_Loggers.clear();
	}

	std::unique_ptr<Logger> CommonLocator::s_DefaultLogger;
	std::unordered_map<HashedString, std::unique_ptr<Logger>> CommonLocator::s_Loggers;
	std::unique_ptr<InputManager> CommonLocator::s_InputManager;
}
