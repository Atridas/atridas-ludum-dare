#include "stdafx.h"

#include "ClockCreator.hpp"

#include <SFML/System/Clock.hpp>

namespace qdrg {


	class SFMLClock : public Clock
	{
	public:

		// elapsed time since the last "restart" call
		Time GetElapsedTime() const
		{
			return Time(m_Clock.getElapsedTime().asMicroseconds());
		}

		//  Restart the clock
		Time Restart()
		{
			return Time(m_Clock.restart().asMicroseconds());
		}

	protected:
		
		sf::Clock m_Clock;

	};
	
	std::unique_ptr<Clock> ClockCreator::CreateClock()
	{
		return std::unique_ptr<Clock>(new SFMLClock());
	}

}
