#include "stdafx.h"

#include "ConsoleLogger.hpp"

#include <condition_variable>
#include <deque>
#include <iostream>
#include <mutex>
#include <thread>

namespace qdrg {

	static std::mutex s_Mutex;
	static std::condition_variable s_CV;
	std::deque<ConsoleLogger::LogMessage> s_Messages;
	static int s_NumLoggers = 0;

	ConsoleLogger::ConsoleLogger(const std::shared_ptr<const Clock>& clock) :Logger(clock)
	{
		std::lock_guard<std::mutex> lock(s_Mutex);
		if (s_NumLoggers == 0) {
			std::thread worker(WorkerThread);
			worker.detach();
		}
		s_NumLoggers++;
	}


	ConsoleLogger::~ConsoleLogger()
	{
		std::unique_lock<std::mutex> lock(s_Mutex);
		s_NumLoggers--;
		s_CV.notify_one();

		if (s_NumLoggers == 0)
		{
			s_CV.wait(lock);
		}

		lock.unlock();
	}

	void ConsoleLogger::Log(LogMessage&& logMessage)
	{
		std::unique_lock<std::mutex> lock(s_Mutex);

		s_Messages.push_back(logMessage);

		lock.unlock();
		s_CV.notify_one();
	}
	
	std::string ConsoleLogger::CreateLogMessage(const LogMessage& logMessage)
	{
		std::stringstream ss;

		switch (logMessage.level)
		{
		case VERVOSE:
			ss << "VERVOSE ";
			break;
		case DEBUG:
			ss << "DEBUG ";
			break;
		case INFO:
			ss << "INFO ";
			break;
		case WARN:
			ss << "WARN ";
			break;
		case ERROR:
			ss << "ERROR ";
			break;
		case ASSERT:
			ss << "ASSERT ";
			break;
		default:
			ss << "LOG ";
			break;
		}

		ss << logMessage.file << ":" << logMessage.line << " [" << logMessage.time.GetAsMilliseconds() << "ms]" << std::endl;
		ss << '\t' << logMessage.tag << std::endl;
		ss << '\t' << logMessage.message << std::endl;

		return ss.str();
	}

	void ConsoleLogger::WorkerThread()
	{
		std::unique_lock<std::mutex> lock(s_Mutex);
		do {
			while (s_Messages.size() > 0)
			{
				LogMessage lmsg = s_Messages.front();
				s_Messages.pop_front();
				lock.unlock();
				std::cout << CreateLogMessage(lmsg);
				lock.lock();
			}

			s_CV.wait(lock, []{return s_Messages.size() > 0 || s_NumLoggers == 0; });
		} while (s_NumLoggers > 0);

		lock.unlock();
		s_CV.notify_one();
	}

}
