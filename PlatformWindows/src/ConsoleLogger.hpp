#pragma once

#include <Common/Common.hpp>

namespace qdrg {

	class ConsoleLogger : public Logger
	{
	public:
		ConsoleLogger(const std::shared_ptr<const Clock>& clock);
		~ConsoleLogger();

		void Log(LogMessage&& logMessage) override;

	private:
		static void WorkerThread();
		static std::string CreateLogMessage(const LogMessage& logMessage);
	};

}
