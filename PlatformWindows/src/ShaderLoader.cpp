/*
 * GLSWShaderLoader.cpp
 *
 *  Created on: 01/06/2014
 *      Author: Atridas
 */

#include "stdafx.h"

#include <Render/ShaderLoader.hpp>

#include <glsw/glsw.h>


namespace qdrg {

	static HashedString s_RendererLogger("Renderer");

class GLSWShaderLoader {
public:

	static GLSWShaderLoader& Instance() {
		static GLSWShaderLoader *instance = new GLSWShaderLoader();
		return *instance;
	}

	std::string LoadShader(const std::string& _Key) {
		if(!m_resourcesLoaded) {
		    glswInit();
		    glswSetPath("./Data/Effects/", ".glsw"); // TODO take this from a config file
		    glswAddDirectiveToken("GL32", "#version 150");
		    glswAddDirectiveToken("GL33", "#version 330");
		    glswAddDirectiveToken("GL40", "#version 400");
		    glswAddDirectiveToken("GL41", "#version 410");
		    glswAddDirectiveToken("GL42", "#version 420");
			m_resourcesLoaded = true;
		}

		const char* s = glswGetShader(_Key.c_str());
		if(s == nullptr) {
			// TODO log:
			LOG_E(s_RendererLogger, "GLSWShaderLoader", glswGetError());
			return "";
		} else {
			return s;
		}
	}

	void CleanResources() {
		if(m_resourcesLoaded) {
			glswShutdown();
			m_resourcesLoaded = false;
		}
	}

private:
	GLSWShaderLoader()
	:m_resourcesLoaded(false)
	{ }

	~GLSWShaderLoader() {
		glswShutdown();
	}

	bool m_resourcesLoaded;
};


// --------------------------------------------------------------------------------------------------------------



ShaderLoader::ShaderLoader() {
	// --
}

ShaderLoader::~ShaderLoader() {
	GLSWShaderLoader::Instance().CleanResources();
}

std::string ShaderLoader::LoadShader(HashedString _Key) {
	// TODO other ways to load shaders
	std::string shader = GLSWShaderLoader::Instance().LoadShader(_Key.GetString());

	// TODO if(shader.length() == 0)

	return shader;
}

} /* namespace qdrg */
