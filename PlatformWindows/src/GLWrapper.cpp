/*
 * GLWrapper.cpp
 *
 *  Created on: 29/05/2014
 *      Author: Atridas
 */

#include "stdafx.h"

#include <Render/GLWrapper.hpp>
#include <GL/glew.h>

#include <cstring>
#include <iostream>

namespace qdrg {


void _check_gl_error(const char *file, const char *function, int line) {
	GLenum err(glGetError());

	while (err != GL_NO_ERROR) {
		std::string error;

		switch (err) {
		case GL_INVALID_OPERATION:
			error = "INVALID_OPERATION";
			break;
		case GL_INVALID_ENUM:
			error = "INVALID_ENUM";
			break;
		case GL_INVALID_VALUE:
			error = "INVALID_VALUE";
			break;
		case GL_OUT_OF_MEMORY:
			error = "OUT_OF_MEMORY";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			error = "INVALID_FRAMEBUFFER_OPERATION";
			break;
		}

		std::cout << "GL_" << error.c_str() << " - " << file << ":" << line << std::endl;
		std::cout << "calling " << function << std::endl;
		err = glGetError();
	}
}

#define CHECK_GL_ERROR(fun) _check_gl_error(__FILE__, fun, __LINE__ )

GLWrapper::VertexArrayObject::VertexArrayObject(GLWrapper *_wrapper)
:wrapper(_wrapper)
{
	glGenVertexArrays(1, &vao);
	CHECK_GL_ERROR("glGenVertexArrays");
};

GLWrapper::GLWrapper()
:m_ClearColor(-1,-1,-1,-1)
,m_ViewportOrigin(0,0)
,m_ViewportSize(0,0)
,m_CurrentDepthMode(DepthMode::DISABLED)
,m_BuffersToBeDeleted(nullptr)
,m_BuffersToBeDeletedSize(0)
,m_BuffersToBeDeletedValidElements(0)
,m_VAOsToBeDeleted(nullptr)
,m_VAOsToBeDeletedSize(0)
,m_VAOsToBeDeletedValidElements(0)
,m_bindedVertexBuffer(nullptr)
,m_bindedIndexBuffer(nullptr)
,m_bindedVAO(nullptr)
,m_CurrentTextureUnit(-1)
{
    // create glew
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
      /* Problem: glewInit failed, something is seriously wrong. */
    	std::cout << "Error: " << glewGetErrorString(err) << std::endl;
    }
	std::cout << "Status: Using GLEW " << glewGetString(GLEW_VERSION) << std::endl;

	for (int i = 0; i < MAX_TEXTURE_UNITS; ++i)
	{
		m_bindedTexture2Ds[i] = nullptr;
	}

}

GLWrapper::~GLWrapper() {
	// ---

	assert(m_BuffersToBeDeletedValidElements == 0);
	if(m_BuffersToBeDeletedSize > 0) {
		delete[] m_BuffersToBeDeleted;
	}

}

void GLWrapper::SetClearColor(const glm::vec4& _Color) {
	if(_Color != m_ClearColor) {
		glClearColor(_Color.r, _Color.g, _Color.b, _Color.a);
		CHECK_GL_ERROR("glClearColor");
		m_ClearColor = _Color;
	}
}

void GLWrapper::SetViewport(const glm::ivec2& _origin, const glm::ivec2& _size)
{
	if(_origin != m_ViewportOrigin || _size != m_ViewportSize) {
		glViewport(_origin.x, _origin.y, _size.x, _size.y);
		CHECK_GL_ERROR("glViewport");
		m_ViewportOrigin = _origin;
		m_ViewportSize   = _size;
	}

}

void GLWrapper::Clear(bool _ColorBuffer, bool _DepthBuffer, bool _StencilBuffer)
{
	assert(_ColorBuffer   == 0 || _ColorBuffer   == 1);
	assert(_DepthBuffer   == 0 || _DepthBuffer   == 1);
	assert(_StencilBuffer == 0 || _StencilBuffer == 1);

	GLbitfield mask = (_ColorBuffer * GL_COLOR_BUFFER_BIT) | (_DepthBuffer * GL_DEPTH_BUFFER_BIT) | (_StencilBuffer * GL_STENCIL_BUFFER_BIT);

	glClear(mask);
	CHECK_GL_ERROR("glClear");
}

void GLWrapper::SetDepthTestMode(DepthMode _depthMode)
{
	if (_depthMode != m_CurrentDepthMode)
	{
		switch (_depthMode)
		{
		case DepthMode::DISABLED:
			glDisable(GL_DEPTH_TEST);
			break;
		case DepthMode::ENABLED:
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_TRUE);
			break;
		case DepthMode::READ_ONLY:
			glEnable(GL_DEPTH_TEST);
			glDepthMask(GL_FALSE);
			break;
		default:
			break;
		}

		m_CurrentDepthMode = _depthMode;
	}
}

std::unique_ptr<GLWrapper::VertexBuffer> GLWrapper::CreateVertexBuffer()
{
	GLuint bo;
	glGenBuffers(1, &bo);
	CHECK_GL_ERROR("glGenBuffers");
	return std::unique_ptr<VertexBuffer>(new VertexBuffer(bo, this));
}

std::unique_ptr<GLWrapper::IndexBuffer>  GLWrapper::CreateIndexBuffer()
{
	GLuint bo;
	glGenBuffers(1, &bo);
	CHECK_GL_ERROR("glGenBuffers");
	return std::unique_ptr<IndexBuffer>(new IndexBuffer(bo, this));
}

static GLenum GetType(const GLWrapper::VertexAttrib& o) {
	switch(o.type) {
	case GLWrapper::Type::BYTE:
		return GL_UNSIGNED_BYTE;
	case GLWrapper::Type::INT16:
		return GL_SHORT;
	case GLWrapper::Type::UINT16:
		return GL_UNSIGNED_SHORT;
	case GLWrapper::Type::INT32:
		return GL_INT;
	case GLWrapper::Type::UINT32:
		return GL_UNSIGNED_INT;
	case GLWrapper::Type::FLOAT:
		return GL_FLOAT;
	default:
		return 0;
	}
}

static GLboolean GetNormalize(const GLWrapper::VertexAttrib& o) {
	switch(o.normalize) {
	case GLWrapper::Normalization::NORMALIZE:
		return GL_TRUE;
	case GLWrapper::Normalization::UNMODIFIED:
		return GL_FALSE;
	default:
		return 0;
	}
}

std::unique_ptr<GLWrapper::VertexArrayObject> GLWrapper::CreateVertexArrayObject(
		const std::vector<VertexAttrib>& _vertexAttribs,
		const IndexBuffer* _indexBuffer)
{
	std::unique_ptr<VertexArrayObject> l_vao(new VertexArrayObject(this));
	BindVertexArrayObject(l_vao.get());
	for(auto vertexAttrib : _vertexAttribs) {
		BindVertexBuffer(vertexAttrib.buffer);
		glEnableVertexAttribArray(vertexAttrib.index);
		CHECK_GL_ERROR("glEnableVertexAttribArray");
		GLenum type = GetType(vertexAttrib);
		GLboolean norm = GetNormalize(vertexAttrib);
		glVertexAttribPointer(vertexAttrib.index, vertexAttrib.size, type, norm, vertexAttrib.stride, (GLvoid*)(0+vertexAttrib.offset));
		CHECK_GL_ERROR("glVertexAttribPointer");
	}

	if (_indexBuffer != nullptr)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexBuffer->bo);
		CHECK_GL_ERROR("glBindBuffer");
		l_vao->indexed = true;
	}
	else
	{
		l_vao->indexed = false;
	}

	return l_vao;
}

static constexpr GLenum CreateUsage(GLWrapper::BufferUssageFrequency _freq, GLWrapper::BufferUssageNature _nature)
{
	return (_freq == GLWrapper::BufferUssageFrequency::STATIC) ?
		((_nature == GLWrapper::BufferUssageNature::DRAW) ? GL_STATIC_DRAW : ((_nature == GLWrapper::BufferUssageNature::READ) ? GL_STATIC_READ : GL_STATIC_COPY))
			:(
			(_freq == GLWrapper::BufferUssageFrequency::STREAM) ?
			((_nature == GLWrapper::BufferUssageNature::DRAW) ? GL_STREAM_DRAW : ((_nature == GLWrapper::BufferUssageNature::READ) ? GL_STREAM_READ : GL_STREAM_COPY))
				:(
				(_nature == GLWrapper::BufferUssageNature::DRAW) ? GL_DYNAMIC_DRAW : ((_nature == GLWrapper::BufferUssageNature::READ) ? GL_DYNAMIC_READ : GL_DYNAMIC_COPY))
				);
}

void GLWrapper::BufferData(const VertexBuffer* _buffer, const void* _bufferData, size_t _bufferSize, BufferUssageFrequency _freq, BufferUssageNature _nature)
{
	BindVertexBuffer(_buffer);
	glBufferData(GL_ARRAY_BUFFER, _bufferSize, _bufferData, CreateUsage(_freq, _nature));
	CHECK_GL_ERROR("glBufferData");
}

void GLWrapper::BufferData(const IndexBuffer* _buffer, const void* _bufferData, size_t _bufferSize, BufferUssageFrequency _freq, BufferUssageNature _nature)
{
	BindIndexBuffer(_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, _bufferSize, _bufferData, CreateUsage(_freq, _nature));
	CHECK_GL_ERROR("glBufferData");
}

void GLWrapper::ReplaceBufferData(const VertexBuffer* _buffer, const BufferReplaceData* _bufferReplaceData, uint_fast32_t _numChunks)
{
	if(_numChunks == 0)
	{
		return;
	}
	BindVertexBuffer(_buffer);
	uintptr_t firstOffset = _bufferReplaceData[0].offset;
	uintptr_t lastOffset  = _bufferReplaceData[_numChunks - 1].offset + _bufferReplaceData[_numChunks - 1].size;
	uint8_t* buffer = (uint8_t*)glMapBufferRange(GL_ARRAY_BUFFER, firstOffset, lastOffset - firstOffset, GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);
	CHECK_GL_ERROR("glMapBufferRange");

	for(uint_fast32_t i = 0; i < _numChunks; i++) {
		assert(i == 0 || _bufferReplaceData[i].offset > _bufferReplaceData[i-1].offset); // ens assegurem que les daden entrin ordenades
		memcpy((void*)(buffer + _bufferReplaceData[i].offset - firstOffset), _bufferReplaceData[i].data, _bufferReplaceData[i].size);
		glFlushMappedBufferRange(GL_ARRAY_BUFFER, _bufferReplaceData[i].offset - firstOffset, _bufferReplaceData[i].size);
		CHECK_GL_ERROR("glFlushMappedBufferRange");
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);
	CHECK_GL_ERROR("glUnmapBuffer");
}

void GLWrapper::Draw(const VertexArrayObject* _vao, Primitive _primitive, uint32_t _firstIndex, uint32_t _indexCount)
{
	assert(_vao->indexed);

	GLenum primitive;
	switch (_primitive) {
	case Primitive::TRIANGLES:
		assert(_indexCount % 3 == 0);
		primitive = GL_TRIANGLES;
		break;
	case Primitive::LINES:
		assert(_indexCount % 2 == 0);
		primitive = GL_LINES;
		break;
	default:
		primitive = GL_TRIANGLES;
		break;
	}

	BindVertexArrayObject(_vao);
	CHECK_GL_ERROR("BindVertexArrayObject");
	glDrawElements(primitive, _indexCount, GL_UNSIGNED_SHORT, (GLvoid*)(0 + _firstIndex));
	CHECK_GL_ERROR("glDrawElements");
}

void GLWrapper::DrawUnindexed(const VertexArrayObject* _vao, Primitive _primitive, uint32_t _firstIndex, uint32_t _indexCount)
{
	assert(!_vao->indexed);

	GLenum primitive;
	switch (_primitive) {
	case Primitive::TRIANGLES:
		assert(_indexCount % 3 == 0);
		primitive = GL_TRIANGLES;
		break;
	case Primitive::LINES:
		assert(_indexCount % 2 == 0);
		primitive = GL_LINES;
		break;
	default:
		primitive = GL_TRIANGLES;
		break;
	}

	BindVertexArrayObject(_vao);
	CHECK_GL_ERROR("BindVertexArrayObject");
	glDrawArrays(primitive, _firstIndex, _indexCount);
	CHECK_GL_ERROR("glDrawElements");
}

void GLWrapper::BindVertexBuffer(const VertexBuffer* _vb)
{
	if(_vb != m_bindedVertexBuffer) {
		glBindBuffer(GL_ARRAY_BUFFER, _vb->bo);
		CHECK_GL_ERROR("glBindBuffer");
		m_bindedVertexBuffer = _vb;
	}
}

void GLWrapper::BindIndexBuffer(const IndexBuffer* _ib)
{
	if(_ib != m_bindedIndexBuffer) {
		if(m_bindedVAO != nullptr) {
			glBindVertexArray(0);
			CHECK_GL_ERROR("glBindVertexArray");
			m_bindedVAO = nullptr;
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ib->bo);
		CHECK_GL_ERROR("glBindBuffer");
		m_bindedIndexBuffer = _ib;
	}
}


void GLWrapper::BindVertexArrayObject(const VertexArrayObject* _vao)
{
	if(_vao != m_bindedVAO) {
		glBindVertexArray(_vao->vao);
		CHECK_GL_ERROR("glBindVertexArray");
		m_bindedVAO = _vao;
	}
}

std::unique_ptr<GLWrapper::Texture2DObject> GLWrapper::CreateTexture2D(bool _withMipmaps)
{
	GLuint textureName;
	glGenTextures(1, &textureName);

	return std::unique_ptr<Texture2DObject>(new Texture2DObject(textureName, this, _withMipmaps));
}

void GLWrapper::SetTextureData(const Texture2DObject* _texture, int _internalFormat, int _externalFormat, int _type, int width, int height, int levels, int *widths, int *heights, uint8_t **data)
{
	assert(_texture != nullptr);
	assert(_texture->withMipmaps || levels == 1);
	assert(!_texture->withMipmaps || levels > 1);
	assert(levels > 0);
	
	BindTexture(0, _texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0)");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, levels - 1);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, -----)");

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_RED);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_RED)");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_GREEN);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_GREEN)");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_BLUE);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_BLUE)");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_ALPHA);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_ALPHA)");

	glTexStorage2D(GL_TEXTURE_2D, GLenum(levels), GLenum(_internalFormat), GLsizei(width), GLsizei(height));
	CHECK_GL_ERROR("glTexStorage2D");

	for (int level = 0; level < levels; ++level)
	{
		glTexSubImage2D(
			GL_TEXTURE_2D,
			GLint(level),
			0, 0,
			GLsizei(widths[level]),
			GLsizei(heights[level]),
			GLenum(_externalFormat),
			GLenum(_type),
			data[level]);
		CHECK_GL_ERROR("glTexSubImage2D");
	}
}

void GLWrapper::SetTextureCompressedData(const Texture2DObject* _texture, int _format, int levels, int width, int height, int *widths, int *heights, int *sizes, uint8_t **data)
{
	assert(_texture != nullptr);
	assert(_texture->withMipmaps || levels == 1);
	assert(!_texture->withMipmaps || levels > 1);
	assert(levels > 0);

	BindTexture(0, _texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0)");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, levels - 1);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, -----)");

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_RED);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_RED)");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_GREEN);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_GREEN)");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_BLUE);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_BLUE)");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_ALPHA);
	CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_ALPHA)");

	glTexStorage2D(GL_TEXTURE_2D, GLenum(levels), GLenum(_format), GLsizei(width), GLsizei(height));
	CHECK_GL_ERROR("glTexStorage2D");

	for (int level = 0; level < levels; ++level)
	{
		glCompressedTexSubImage2D(
			GL_TEXTURE_2D,
			GLint(level),
			0, 0,
			GLsizei(widths[level]),
			GLsizei(heights[level]),
			GLenum(_format),
			GLsizei(sizes[level]),
			data[level]);
		CHECK_GL_ERROR("glCompressedTexSubImage2D");
	}
}

void GLWrapper::SetTextureFilter(const Texture2DObject* _texture, TextureFilter _filterMode)
{
	assert(_texture != nullptr);

	BindTexture(0, _texture);

	if (_texture->withMipmaps)
	{
		switch (_filterMode)
		{
		case qdrg::GLWrapper::TextureFilter::NEAREST:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
			CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST)");
			break;
		case qdrg::GLWrapper::TextureFilter::LINEAR:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
			CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR)");
			break;
		case qdrg::GLWrapper::TextureFilter::TRILINEAR:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)");
			break;
		default:
			assert(false);
			break;
		}
	}
	else
	{
		switch (_filterMode)
		{
		case qdrg::GLWrapper::TextureFilter::NEAREST:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)");
			break;
		case qdrg::GLWrapper::TextureFilter::LINEAR:
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			CHECK_GL_ERROR("glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)");
			break;
		case qdrg::GLWrapper::TextureFilter::TRILINEAR:
			assert(false);
			break;
		default:
			assert(false);
			break;
		}
	}
}

void GLWrapper::BindTexture(int _unit, const Texture2DObject* _texture)
{
	assert(_texture != nullptr);
	assert(_unit >= 0 && _unit < MAX_TEXTURE_UNITS);
	if (m_bindedTexture2Ds[_unit] != _texture)
	{
		SetTextureUnit(_unit);
		glBindTexture(GL_TEXTURE_2D, _texture->texture);
		CHECK_GL_ERROR("glBindTexture");
		m_bindedTexture2Ds[_unit] = _texture;
	}
}

void GLWrapper::SetTextureUnit(int _unit)
{
	assert(_unit >= 0 && _unit < MAX_TEXTURE_UNITS);
	if (m_CurrentTextureUnit != _unit)
	{
		glActiveTexture(GL_TEXTURE0 + _unit);
		CHECK_GL_ERROR("glActiveTexture");
		m_CurrentTextureUnit = _unit;
	}
}

void GLWrapper::AddObjectToBeDeleted(
		const GenericGLObject& _object,
		GLWrapper::GenericGLObject** objectsToBeDeleted_,
		int& objectsToBeDeletedValidElements_,
		int& objectsToBeDeletedSize_
		)
{
	if(objectsToBeDeletedValidElements_ < objectsToBeDeletedSize_) {
		(*objectsToBeDeleted_)[objectsToBeDeletedValidElements_] = _object;
		objectsToBeDeletedValidElements_++;
	} else if(objectsToBeDeletedSize_ == 0) {
		objectsToBeDeletedSize_ = 5;
		(*objectsToBeDeleted_) = new GenericGLObject[objectsToBeDeletedSize_];
		//AddObjectToBeDeleted(_object, objectsToBeDeleted_, objectsToBeDeletedValidElements_, objectsToBeDeletedSize_);

		(*objectsToBeDeleted_)[objectsToBeDeletedValidElements_] = _object;
		objectsToBeDeletedValidElements_++;
	} else {
		GenericGLObject *tmp = *objectsToBeDeleted_;
		objectsToBeDeletedSize_ += (objectsToBeDeletedSize_ < 100)? 5 : 50;
		*objectsToBeDeleted_ = new GenericGLObject[objectsToBeDeletedSize_];
		for(int i = 0; i < objectsToBeDeletedValidElements_; i++) {
			(*objectsToBeDeleted_)[i] = tmp[i];
		}
		delete[] tmp;
		//AddObjectToBeDeleted(_object, objectsToBeDeleted_, objectsToBeDeletedValidElements_, objectsToBeDeletedSize_);

		(*objectsToBeDeleted_)[objectsToBeDeletedValidElements_] = _object;
		objectsToBeDeletedValidElements_++;
	}
}

void GLWrapper::DeleteUnusedResources()
{
	if (m_BuffersToBeDeletedValidElements > 0) {
		glDeleteBuffers(m_BuffersToBeDeletedValidElements, (const GLuint*)m_BuffersToBeDeleted);
		CHECK_GL_ERROR("glDeleteBuffers");
		m_BuffersToBeDeletedValidElements = 0;
	}
	if (m_VAOsToBeDeletedValidElements > 0) {
		glDeleteVertexArrays(m_VAOsToBeDeletedValidElements, (const GLuint*)m_VAOsToBeDeleted);
		CHECK_GL_ERROR("glDeleteVertexArrays");
		m_VAOsToBeDeletedValidElements = 0;
	}
	if (m_TexturesToBeDeletedValidElements > 0) {
		glDeleteTextures(m_TexturesToBeDeletedValidElements, (const GLuint*)m_TexturesToBeDeleted);
		CHECK_GL_ERROR("glDeleteTextures");
		m_TexturesToBeDeletedValidElements = 0;
	}
}

} /* namespace luapp11 */
