/*
 * ShaderManager.cpp
 *
 *  Created on: 01/06/2014
 *      Author: Atridas
 */

#include "stdafx.h"

#include <Render/ShaderManager.hpp>
#include <Render/ShaderManagerFactory.hpp>

#ifdef PLATFORM_PC
extern "C" {
#include <GL/glew.h>
}
#endif

#include <Render/ShaderLoader.hpp>

namespace qdrg {

	static HashedString s_LogName("Renderer");

	static const size_t s_MaxNumberOfPrograms = 20;

struct ShaderManagerData
{
	std::unordered_map<HashedString, size_t> programsIndexes;

	struct {
		GLuint id;
		GLint uniformNames[(size_t)ShaderManager::Uniform::NUM_UNIFORMS];
		glm::vec4 color;
		float time;
	} programInfo[s_MaxNumberOfPrograms];
	
};

class ShaderManager_IMPL : public ShaderManager
{
public:
	ShaderManager_IMPL(std::unordered_map<HashedString, GLuint>&& _programs, std::unordered_map<HashedString, std::unordered_map<ShaderManager::Uniform, GLint>>&& _uniformNames)
	:ShaderManager(&m_data)
	{
		size_t index = 0;
		for (auto program : _programs) {
			m_data.programsIndexes[program.first] = index;
			m_data.programInfo[index].id = program.second;

			auto uniformNames = _uniformNames.find(program.first);
			for (size_t uniform = 0; uniform < (size_t)Uniform::NUM_UNIFORMS; uniform++) {
				if (uniformNames == _uniformNames.end())
				{
					m_data.programInfo[index].uniformNames[uniform] = -1;
				}
				else
				{
					auto uniformName = uniformNames->second.find((ShaderManager::Uniform)uniform);
					if (uniformName == uniformNames->second.end()) {
						m_data.programInfo[index].uniformNames[uniform] = -1;
					}
					else
					{
						m_data.programInfo[index].uniformNames[uniform] = uniformName->second;
					}
				}
			}

			index++;
		}
	};


private:
	ShaderManagerData m_data;
};

ShaderManager::ShaderManager(ShaderManagerData* _dataLoc)
:m_data(_dataLoc)
,m_ResourcesDeleted(false)
{
	// TODO
}

ShaderManager::~ShaderManager()
{
	assert(m_ResourcesDeleted);
}

void ShaderManager::DeleteResources()
{
	assert(!m_ResourcesDeleted);
	for (auto it : m_data->programsIndexes) {
		glDeleteProgram(m_data->programInfo[it.second].id);
	}
	m_ResourcesDeleted = true;
}


void ShaderManager::UseProgram(HashedString _program)
{
	assert(!m_ResourcesDeleted);
	if(_program != m_CurrentProgram) {
		assert(m_data->programsIndexes.find(_program) != m_data->programsIndexes.end());
		m_CurrentProgramIndex = m_data->programsIndexes[_program];
		glUseProgram(m_data->programInfo[m_CurrentProgramIndex].id);

		m_CurrentProgram = _program;
	}
}

void ShaderManager::SetWorldMatrix(const glm::mat4& m)
{
	assert(!m_ResourcesDeleted);
	auto location = m_data->programInfo[m_CurrentProgramIndex].uniformNames[(size_t)Uniform::WORLD_MATRIX];
	assert(location >= 0);
	glUniformMatrix4fv(location, 1, GL_FALSE, &m[0][0]);
}

void ShaderManager::SetViewMatrix(const glm::mat4& m)
{
	assert(!m_ResourcesDeleted);
	auto location = m_data->programInfo[m_CurrentProgramIndex].uniformNames[(size_t)Uniform::VIEW_MATRIX];
	assert(location >= 0);
	glUniformMatrix4fv(location, 1, GL_FALSE, &m[0][0]);
}

void ShaderManager::SetProjectionMatrix(const glm::mat4& m)
{
	assert(!m_ResourcesDeleted);
	auto location = m_data->programInfo[m_CurrentProgramIndex].uniformNames[(size_t)Uniform::PROJECTION_MATRIX];
	assert(location >= 0);
	glUniformMatrix4fv(location, 1, GL_FALSE, &m[0][0]);
}

void ShaderManager::SetColor(const glm::vec4& c)
{
	assert(!m_ResourcesDeleted);
	auto location = m_data->programInfo[m_CurrentProgramIndex].uniformNames[(size_t)Uniform::COLOR];
	assert(location >= 0);
	if (c != m_data->programInfo[m_CurrentProgramIndex].color)
	{
		glUniform4fv(location, 1, &c[0]);
		m_data->programInfo[m_CurrentProgramIndex].color = c;
	}
}

void ShaderManager::SetTime(float t)
{
	assert(!m_ResourcesDeleted);
	auto location = m_data->programInfo[m_CurrentProgramIndex].uniformNames[(size_t)Uniform::TIME];
	assert(location >= 0);
	if (t != m_data->programInfo[m_CurrentProgramIndex].time)
	{
		glUniform1f(location, t);
		m_data->programInfo[m_CurrentProgramIndex].time = t;
	}
}

// ------------------------------------------------------------------------------------------------------------------------------



class ShaderManagerFactory_IMPL : public ShaderManagerFactory
{
public:
	ShaderManagerFactory_IMPL()
	:m_shaderManagerCreated(false)
	{
		static bool shaderManagerFactoryCreated = false;
		assert(!shaderManagerFactoryCreated);
		shaderManagerFactoryCreated = true;
	}
	~ShaderManagerFactory_IMPL()
	{
		assert(m_shaderManagerCreated);
	};

	void LoadVertexShader(HashedString _ShaderKey)
	{
		assert(_ShaderKey != HashedString());
		assert(!m_shaderManagerCreated);
		if(m_vertexShaders.find(_ShaderKey) == m_vertexShaders.end()) {
			GLuint shaderObj = LoadShader(GL_VERTEX_SHADER, _ShaderKey);
			assert(shaderObj > 0);

			m_vertexShaders[_ShaderKey] = shaderObj;
		}
	}

	void LoadGeometryShader(HashedString _ShaderKey)
	{
		assert(_ShaderKey != HashedString());
		assert(!m_shaderManagerCreated);
		if(m_geometryShaders.find(_ShaderKey) == m_geometryShaders.end()) {
			GLuint shaderObj = LoadShader(GL_GEOMETRY_SHADER, _ShaderKey);
			assert(shaderObj > 0);

			m_geometryShaders[_ShaderKey] = shaderObj;
		}
	}

	void LoadFragmentShader(HashedString _ShaderKey)
	{
		assert(_ShaderKey != HashedString());
		assert(!m_shaderManagerCreated);
		if(m_fragmentShaders.find(_ShaderKey) == m_fragmentShaders.end()) {
			GLuint shaderObj = LoadShader(GL_FRAGMENT_SHADER, _ShaderKey);
			assert(shaderObj > 0);

			m_fragmentShaders[_ShaderKey] = shaderObj;
		}
	}

	void SaveUniforms(HashedString _ProgramKey, GLuint _ShaderProgram, const std::map<ShaderManager::Uniform, std::string>& _Uniforms)
	{
		//std::unordered_map<HashedString, >
		std::unordered_map<ShaderManager::Uniform, GLint> uniforms;
		for (auto uniform : _Uniforms)
		{
			auto uniformLocation = glGetUniformLocation(_ShaderProgram, uniform.second.c_str());
			assert(uniformLocation >= 0);
			uniforms[uniform.first] = uniformLocation;

			switch (uniform.first)
			{
			case ShaderManager::Uniform::ALBEDO_TEXTURE:
				glUniform1i(uniformLocation, 0);
				break;
			}
		}

		m_uniforms[_ProgramKey] = uniforms;
	}

	void CreateProgram(HashedString _ProgramKey, const std::map<ShaderManager::Uniform, std::string>& _Uniforms, HashedString _Vertex, HashedString _Fragment, HashedString _Geometry) override
	{
		assert(_ProgramKey != HashedString());
		assert(!m_shaderManagerCreated);
		if(m_programs.find(_ProgramKey) == m_programs.end()) {
			assert(_Vertex != HashedString());
			assert(_Fragment != HashedString());

			GLuint shaderProgram = glCreateProgram();

			assert(m_vertexShaders.find(_Vertex) != m_vertexShaders.end());
			glAttachShader(shaderProgram, m_vertexShaders[_Vertex]);
			if(m_geometryShaders.find(_Geometry) != m_geometryShaders.end()) { // This stage is optional
				glAttachShader(shaderProgram, m_geometryShaders[_Geometry]);
			}
			assert(m_fragmentShaders.find(_Fragment) != m_fragmentShaders.end());
			glAttachShader(shaderProgram, m_fragmentShaders[_Fragment]);

			glLinkProgram(shaderProgram);

			GLint success;
			glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
			if (success == GL_FALSE) {
			    GLchar infoLog[1024];
			    glGetProgramInfoLog(shaderProgram, sizeof(infoLog), NULL, infoLog);
			    
				std::stringstream ss;

			    ss << "Error linking shader program '" << _ProgramKey.GetString() << "'";
			    ss << " with vertex shader '" << _Vertex.GetString() << "'";
			    ss << " with geometry shader '" << _Geometry.GetString() << "'";
			    ss << " with fragment shader '" << _Fragment.GetString() << "'";
			    ss << ": " << infoLog << std::endl;

				LOG_E(s_LogName, "ShaderManager", ss.str());

			    glDeleteProgram(shaderProgram);
			} else {
				m_programs[_ProgramKey] = shaderProgram;
				m_programComposition[_ProgramKey] = std::make_tuple(_Vertex, _Geometry, _Fragment);

				glUseProgram(shaderProgram);
				SaveUniforms(_ProgramKey, shaderProgram, _Uniforms);
			}
		}
		else
		{
			assert(std::get<0>(m_programComposition[_ProgramKey]) == _Vertex);
			assert(std::get<1>(m_programComposition[_ProgramKey]) == _Geometry);
			assert(std::get<2>(m_programComposition[_ProgramKey]) == _Fragment);
		}
	}

	std::unique_ptr<ShaderManager> CreateShaderManager(GLWrapper& wrapper)
	{
		assert(!m_shaderManagerCreated);

		for(auto it : m_vertexShaders) {
			glDeleteShader(it.second);
		}
		for(auto it : m_geometryShaders) {
			glDeleteShader(it.second);
		}
		for(auto it : m_fragmentShaders) {
			glDeleteShader(it.second);
		}

		m_shaderManagerCreated = true;

		glUseProgram(0);
		return std::unique_ptr<ShaderManager>(new ShaderManager_IMPL(std::move(m_programs), std::move(m_uniforms)));
	}


private:
	std::unordered_map<HashedString, GLuint> m_vertexShaders, m_geometryShaders, m_fragmentShaders;
	std::unordered_map<HashedString, GLuint> m_programs;
	std::unordered_map<HashedString, std::tuple<HashedString,HashedString,HashedString> > m_programComposition;
	std::unordered_map<HashedString, std::unordered_map<ShaderManager::Uniform, GLint>> m_uniforms;

	ShaderLoader m_shaderLoader;

	bool m_shaderManagerCreated;


	GLuint LoadShader(GLenum type, HashedString _ShaderKey) {
		GLuint shaderObj = glCreateShader(type);

		assert(shaderObj != 0);

		std::string source = m_shaderLoader.LoadShader(_ShaderKey);
		assert(source.length() > 0);

		const GLchar* p[] = {source.c_str()};
		GLint lengths[] = {(GLint)source.length()};
		glShaderSource(shaderObj, 1, p, lengths);

		glCompileShader(shaderObj);

		GLint success;
		glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &success);
		if (!success) {
		    GLchar infoLog[1024];
		    glGetShaderInfoLog(shaderObj, sizeof(infoLog), NULL, infoLog);
		    // TODO log system
			std::stringstream ss;
		    ss << "Error compiling shader " << _ShaderKey.GetString() << " type " << type << ": " << infoLog << std::endl;

			LOG_E(s_LogName, "ShaderManager", ss.str());

		    glDeleteShader(shaderObj);
		    return 0;
		}

		return shaderObj;
	}
};

std::unique_ptr<ShaderManagerFactory> ShaderManagerFactory::CreateShaderManagerFactory()
{
	return std::unique_ptr<ShaderManagerFactory_IMPL>(new ShaderManagerFactory_IMPL());
}

} /* namespace qdrg */
