/*
 * main.cpp
 *
 *  Created on: 15/06/2014
 *      Author: Atridas
 */

#include "stdafx.h"

#include <SFML/Window.hpp>
#include <gli/gli.hpp>
#include <GL/glew.h>

#include <iostream>

#include <Common/Common.hpp>
#include <Common/CommonLocator.hpp>
#include <Common/Clock.hpp>
#include <Common/Game.hpp>
#include <Common/InputManager.hpp>

#include <Render/GLWrapper.hpp>

#include "ClockCreator.hpp"
#include "ConsoleLogger.hpp"

using namespace qdrg;

static HashedString s_MainLogger("Main");

static bool s_Running = true;


class WinInputManager : public InputManager
{
public:

	void CallEvent(HashedString _eventName, float _value = 1.0f) { InputManager::CallEvent(_eventName, _value); }
	void ActivateEvent(HashedString _eventName) { InputManager::ActivateEvent(_eventName); }
	void DeactivateEvent(HashedString _eventName) { InputManager::DeactivateEvent(_eventName); }

	void CallEvent(HashedString _eventName, const glm::vec2& _value) { InputManager::CallEvent(_eventName, _value); };
	void CallEvent(HashedString _eventName, const glm::vec3& _value) { InputManager::CallEvent(_eventName, _value); };
	void CallEvent(HashedString _eventName, const glm::vec4& _value) { InputManager::CallEvent(_eventName, _value); };
};

static WinInputManager *s_InputManager = nullptr;

//GLuint CreateImage(const char* path)
//{
//	gli::texture2D texture(gli::load_dds(path));
//
//	GLuint textureName;
//	glGenTextures(1, &textureName);
//
//	glBindTexture(GL_TEXTURE_2D, textureName);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, GLint(texture.levels() - 1));
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_RED);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_GREEN);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_BLUE);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_ALPHA);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//	glTexStorage2D(GL_TEXTURE_2D,
//		GLint(texture.levels()),
//		GLenum(gli::internal_format(texture.format())),
//		GLsizei(texture.dimensions().x),
//		GLsizei(texture.dimensions().y));
//	if (gli::is_compressed(texture.format()))
//	{
//		for (gli::texture2D::size_type Level = 0; Level < texture.levels(); ++Level)
//		{
//			glCompressedTexSubImage2D(GL_TEXTURE_2D,
//				GLint(Level),
//				0, 0,
//				GLsizei(texture[Level].dimensions().x),
//				GLsizei(texture[Level].dimensions().y),
//				GLenum(gli::internal_format(texture.format())),
//				GLsizei(texture[Level].size()),
//				texture[Level].data());
//		}
//	}
//	else
//	{
//		for (gli::texture2D::size_type Level = 0; Level < texture.levels(); ++Level)
//		{
//			glTexSubImage2D(GL_TEXTURE_2D,
//				GLint(Level),
//				0, 0,
//				GLsizei(texture[Level].dimensions().x),
//				GLsizei(texture[Level].dimensions().y),
//				GLenum(gli::external_format(texture.format())),
//				GLenum(gli::type_format(texture.format())),
//				texture[Level].data());
//		}
//	}
//
//	return textureName;
//}

static HashedString s_CameraForward("CameraForward");
static HashedString s_CameraBackward("CameraBackward");
static HashedString s_CameraLeft("CameraLeft");
static HashedString s_CameraRight("CameraRight");
static HashedString s_CameraRotateLeft("CameraRotateLeft");
static HashedString s_CameraRotateRight("CameraRotateRight");
static HashedString s_MouseClick("MouseClick");

void ProcessInput(sf::Window& window, GLWrapper& wrapper)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			// end the program
			s_Running = false;
			break;
		case sf::Event::Resized:
			// adjust the viewport when the window is resized
			//glViewport(0, 0, event.size.width, event.size.height);
			wrapper.SetViewport(glm::ivec2(0,0), glm::ivec2(event.size.width, event.size.height));
			break;
		case sf::Event::KeyPressed:
			switch (event.key.code)
			{
			case sf::Keyboard::W:
				s_InputManager->ActivateEvent(s_CameraForward);
				break;
			case sf::Keyboard::A:
				s_InputManager->ActivateEvent(s_CameraLeft);
				break;
			case sf::Keyboard::S:
				s_InputManager->ActivateEvent(s_CameraBackward);
				break;
			case sf::Keyboard::D:
				s_InputManager->ActivateEvent(s_CameraRight);
				break;
			case sf::Keyboard::Q:
				s_InputManager->ActivateEvent(s_CameraRotateLeft);
				break;
			case sf::Keyboard::E:
				s_InputManager->ActivateEvent(s_CameraRotateRight);
				break;
			}
			break;
		case sf::Event::KeyReleased:
			switch (event.key.code)
			{
			case sf::Keyboard::W:
				s_InputManager->DeactivateEvent(s_CameraForward);
				break;
			case sf::Keyboard::A:
				s_InputManager->DeactivateEvent(s_CameraLeft);
				break;
			case sf::Keyboard::S:
				s_InputManager->DeactivateEvent(s_CameraBackward);
				break;
			case sf::Keyboard::D:
				s_InputManager->DeactivateEvent(s_CameraRight);
				break;
			case sf::Keyboard::Q:
				s_InputManager->DeactivateEvent(s_CameraRotateLeft);
				break;
			case sf::Keyboard::E:
				s_InputManager->DeactivateEvent(s_CameraRotateRight);
				break;
			}
			break;
		case sf::Event::MouseButtonPressed:
			switch (event.mouseButton.button)
			{
			case sf::Mouse::Button::Left:
				s_InputManager->CallEvent(s_MouseClick, glm::vec2(event.mouseButton.x, event.mouseButton.y));
			default:
				break;
			}
		}
	}
}

namespace qdrg {

	namespace Game
	{
		glm::vec2 GetWindowSize()
		{
			return glm::vec2(800, 600);
		}
	}

	// TODO moure aix� a algun altre lloc, per� meh
	std::string LoadFileToString(const std::string& _Path)
	{
		std::ifstream t(_Path);
		std::string str((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>());
		return str;
	}
}

int main(int argc, char *argv[])
{
    // create the window
	sf::Window window(sf::VideoMode(qdrg::Game::GetWindowSize().x, qdrg::Game::GetWindowSize().y), "PUTO TEST D'OpenGL V", sf::Style::Default, sf::ContextSettings(32, 0, 0, 3, 3));
    window.setVerticalSyncEnabled(false);

    GLWrapper wrapper;

	CommonLocator::SetClockCreator(std::unique_ptr<CommonLocator::ClockCreator>(new ClockCreator()));
	std::shared_ptr<Clock> consoleClock = CommonLocator::CreateClock();
	CommonLocator::ProvideDefaultLogger(std::unique_ptr<Logger>(new ConsoleLogger(consoleClock)));
	CommonLocator::ProvideLogger(s_MainLogger, std::unique_ptr<Logger>(new ConsoleLogger(consoleClock)));

	s_InputManager = new WinInputManager();
	CommonLocator::ProvideInputManager(std::unique_ptr<InputManager>(s_InputManager));

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Game::Initialize(wrapper);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    uint64_t lag = 0;
    s_Running = true;
    sf::Clock clock;
    auto previous = clock.getElapsedTime().asMicroseconds();
	while (s_Running)
	{
		// read timestep
		uint64_t elapsed;
		{
			auto current = clock.getElapsedTime().asMicroseconds();
			elapsed = current - previous;
			previous = current;
			lag += elapsed;
		}

		if (lag > s_LogicTimestepMicros) {
			// handle events
			ProcessInput(window, wrapper);

			Game::Update();
			lag = lag % s_LogicTimestepMicros;
		}

		Game::Render((float)(lag * 0.000001), wrapper);
		window.display();
	}


    // release resources...
	Game::Dispose(wrapper);

    //shaderManager->DeleteResources();
    wrapper.DeleteUnusedResources();

	CommonLocator::CleanAllLoggers();

    return 0;
}

