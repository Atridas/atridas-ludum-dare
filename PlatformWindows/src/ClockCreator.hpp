#pragma once

#include <Common/Common.hpp>

namespace qdrg {

	class ClockCreator : public CommonLocator::ClockCreator
	{
	public:
		std::unique_ptr<Clock> CreateClock();
	};

}
