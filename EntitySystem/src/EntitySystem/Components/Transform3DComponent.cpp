#include "stdafx.h"
#include "EntitySystem/Components/Transform3DComponent.hpp"


namespace qdrg {

	namespace entities {

		HashedString const Transform3DComponent::ComponentName("Transform3D");

	}
}