#include "stdafx.h"
#include "EntitySystem/Components/MeshComponent.hpp"


namespace qdrg {

	namespace entities {

		HashedString const MeshComponent::ComponentName("Mesh");

	}
}
