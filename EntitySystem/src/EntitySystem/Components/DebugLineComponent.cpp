#include "stdafx.h"
#include "EntitySystem/Components/DebugLineComponent.hpp"


namespace qdrg {

	namespace entities {

		HashedString const DebugLineComponent::ComponentName("Debug Line");

	}
}
