#include "stdafx.h"
#include "EntitySystem/Templates/TemplateCamera.hpp"
#include "EntitySystem/Manager/EntityManager.hpp"

namespace qdrg {

	namespace entities {

		namespace templates {

			TemplateCamera::TemplateCamera(const Transform3DComponent& _transform3D, const CameraComponent& _camera)
				: transform3D(_transform3D)
				, camera(_camera)
				, withTransform(true)
			{
			}

			TemplateCamera::TemplateCamera(const CameraComponent& _camera, bool _withTransform)
				: camera(_camera)
				, withTransform(_withTransform)
			{
			}


			TemplateCamera::~TemplateCamera()
			{
			}

			size_t TemplateCamera::GetExpectedMaxSize() const
			{
				return sizeof(CameraComponent)+(withTransform ? sizeof(Transform3DComponent) : 0);
			}

			void TemplateCamera::ExecuteCreation(EntityReference _entity, EntityManager& entityManager) const
			{
				if (withTransform)
				{
					entityManager.AddComponent(_entity, transform3D);
				}
				entityManager.AddComponent(_entity, camera);
			}
		}
	}
}
