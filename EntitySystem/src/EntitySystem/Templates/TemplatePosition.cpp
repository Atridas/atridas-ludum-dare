#include "stdafx.h"
#include "EntitySystem/Templates/TemplatePosition.hpp"
#include "EntitySystem/Manager/EntityManager.hpp"

namespace qdrg {

	namespace entities {

		namespace templates {

			TemplatePosition::TemplatePosition(const Transform3DComponent& _transform3D)
				: transform3D(_transform3D)
			{

			}

			TemplatePosition::TemplatePosition(const glm::vec3& _position, const glm::quat& _rotation, float _scale, EntityReference _parent)
			{
				transform3D.position = _position;
				transform3D.rotation = _rotation;
				transform3D.scale = _scale;
				transform3D.parent = _parent;
			}


			TemplatePosition::~TemplatePosition()
			{
			}

			size_t TemplatePosition::GetExpectedMaxSize() const
			{
				return sizeof(Transform3DComponent);
			}

			void TemplatePosition::ExecuteCreation(EntityReference _entity, EntityManager& entityManager) const
			{
				entityManager.AddComponent(_entity, transform3D);
			}
		}
	}
}