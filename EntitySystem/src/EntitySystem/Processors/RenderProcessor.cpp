#include "stdafx.h"
#include "EntitySystem/Processors/RenderProcessor.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include <Render/Camera.hpp>
#include <Render/ShaderManager.hpp>
#include <Render/GLWrapper.hpp>


#include "EntitySystem/Manager/EntityCollection.hpp"

#include "EntitySystem/Components/CameraComponent.hpp"
#include "EntitySystem/Components/DebugLineComponent.hpp"

#include "EntitySystem/Processors/RenderProcessorStructures/MaterialModifiers.hpp"

namespace qdrg {

	namespace entities {

		RenderProcessor::RenderProcessor(EntityManager *_EntityManager)
			: m_EntityManager(_EntityManager)
			, m_LargestDebugLines(0)
		{
			assert(m_EntityManager != nullptr);

			m_EntitiesWithMeshes = m_EntityManager->GetAllEntitiesWithComponents({ Transform3DComponent::ComponentName, MeshComponent::ComponentName });
			assert(m_EntitiesWithMeshes->SizeOfCollection() == 0);
			m_EntitiesWithCamera = m_EntityManager->GetAllEntitiesWithComponents({ CameraComponent::ComponentName });
			assert(m_EntitiesWithCamera->SizeOfCollection() == 0);
			m_EntitiesWithDebugLines = m_EntityManager->GetAllEntitiesWithComponents({ DebugLineComponent::ComponentName });
			assert(m_EntitiesWithDebugLines->SizeOfCollection() == 0);
		}

		RenderProcessor::~RenderProcessor()
		{
			m_EntityManager = nullptr;
		}

		void RenderProcessor::AddMesh(HashedString _MeshKey, std::unique_ptr<Mesh>&& _Mesh)
		{
			m_AddedMeshes.push_back(std::make_tuple(_MeshKey, std::move(_Mesh))); // TODO thread save
		}

		void RenderProcessor::AddMaterial(HashedString _MeshKey, std::unique_ptr<Material>&& _Material)
		{
			m_Materials[_MeshKey] = std::move(_Material);
		}

		void RenderProcessor::AddTexture(HashedString _TextureKey, std::unique_ptr<Texture>&& _Texture)
		{
			m_NotLoadedTextures.push_back(std::make_tuple(_TextureKey, std::move(_Texture))); // TODO thread save
		}

		const Texture* RenderProcessor::GetTexture(HashedString _Name) const
		{
			auto it = m_Textures.find(_Name);
			if (it == m_Textures.end())
			{
				return nullptr;
			}
			else
			{
				return it->second.get();
			}
		}


		void RenderProcessor::Update()
		{ 
			// TODO thread save
			for (unsigned int i = 0; i < m_NotLoadedTextures.size(); ++i)
			{
				auto tex = std::move(std::get<1>(m_NotLoadedTextures[i]));
				tex->LoadFromDisk();
				m_NotUploadedTextures.push_back(std::make_tuple(std::get<0>(m_NotLoadedTextures[i]), std::move(tex)));
			}
			m_NotLoadedTextures.clear();
		}

		void RenderProcessor::Render(float _forward, GLWrapper& _wrapper, ShaderManager* _ShaderManager)
		{
			// 1 - add pending meshes
			for (unsigned int i = 0; i < m_AddedMeshes.size(); ++i)
			{
				assert(m_Meshes.count(std::get<0>(m_AddedMeshes[i])) == 0);
				m_Meshes[std::get<0>(m_AddedMeshes[i])] = std::move(std::get<1>(m_AddedMeshes[i]));
				m_Meshes[std::get<0>(m_AddedMeshes[i])]->PrepareGPU_State(_wrapper);
			}
			m_AddedMeshes.clear();
			for (unsigned int i = 0; i < m_NotUploadedTextures.size(); ++i)
			{
				assert(m_Textures.count(std::get<0>(m_NotUploadedTextures[i])) == 0);
				m_Textures[std::get<0>(m_NotUploadedTextures[i])] = std::move(std::get<1>(m_NotUploadedTextures[i]));
				m_Textures[std::get<0>(m_NotUploadedTextures[i])]->PrepareGPU_State(_wrapper);
			}
			m_NotUploadedTextures.clear();


			// - Find camera
			glm::mat4 viewMatrix, projectionMatrix;
			GetCameraParams(_forward, viewMatrix, projectionMatrix);

			// ------------------------------------------------
			Material::SharedData l_SharedData;
			Material::InstanceData l_InstanceData;

			l_InstanceData.forward = l_SharedData.forward = _forward;
			l_InstanceData.wrapper = l_SharedData.wrapper = &_wrapper;
			l_InstanceData.shaderManager = l_SharedData.shaderManager = _ShaderManager;

			l_SharedData.viewMatrix = &viewMatrix;
			l_SharedData.projectionMatrix = &projectionMatrix;
			l_SharedData.textureManager = this;

			std::map<EntityReference, glm::mat4> l_MatrixCache;
			l_InstanceData.entityManager = m_EntityManager;
			l_InstanceData.matrixCache = &l_MatrixCache;

			// 2 - Render shit
			for (auto it = m_EntitiesWithMeshes->begin(); it != m_EntitiesWithMeshes->end(); ++it)
			{
				Transform3DComponent* transform3D = m_EntityManager->GetComponent<Transform3DComponent>(*it);
				MeshComponent       * meshComponent = m_EntityManager->GetComponent<MeshComponent>(*it);

				Mesh* mesh = m_Meshes[meshComponent->meshName].get();

				Material *material = m_Materials[meshComponent->materialName].get();

				l_InstanceData.transform3D = transform3D;
				l_InstanceData.mesh = meshComponent;
				l_InstanceData.entityReference = &(*it);

				material->SetSharedData(l_SharedData);

				material->SetInstanceData(l_InstanceData);

				mesh->Draw(_wrapper);
			}

			// 3 - Render debug Lines
			std::vector<VertexPE1> linepoints;
			for (auto it = m_EntitiesWithDebugLines->begin(); it != m_EntitiesWithDebugLines->end(); ++it)
			{
				Transform3DComponent* transform3D = m_EntityManager->GetComponent<Transform3DComponent>(*it);
				DebugLineComponent* debugLine = m_EntityManager->GetComponent<DebugLineComponent>(*it);

				if (transform3D != nullptr)
				{
					//glm::mat4 GetCachedWorldMatrix(float forward, const EntityReference& entity, EntityManager *em, std::map<EntityReference, glm::mat4> *worldMatrixCache);
					glm::mat4 worldMatrix = GetCachedWorldMatrix(_forward, *it, m_EntityManager, &l_MatrixCache);
				}

			}
		}

		void RenderProcessor::DebugRender(float _forward, GLWrapper& _wrapper, ShaderManager* _ShaderManager)
		{
			glm::mat4 viewMatrix, projectionMatrix;
			GetCameraParams(_forward, viewMatrix, projectionMatrix);

			Material::SharedData l_SharedData;
			Material::InstanceData l_InstanceData;

			l_InstanceData.forward = l_SharedData.forward = _forward;
			l_InstanceData.wrapper = l_SharedData.wrapper = &_wrapper;
			l_InstanceData.shaderManager = l_SharedData.shaderManager = _ShaderManager;

			l_SharedData.viewMatrix = &viewMatrix;
			l_SharedData.projectionMatrix = &projectionMatrix;

			std::map<EntityReference, glm::mat4> l_MatrixCache;
			l_InstanceData.entityManager = m_EntityManager;
			l_InstanceData.matrixCache = &l_MatrixCache;

			// 2 - Render debug Lines
			std::vector<VertexPE1> linepointsWithZTest;
			std::vector<VertexPE1> linepointsWithoutZTest;
			for (auto it = m_EntitiesWithDebugLines->begin(); it != m_EntitiesWithDebugLines->end(); ++it)
			{
				Transform3DComponent* transform3D = m_EntityManager->GetComponent<Transform3DComponent>(*it);
				DebugLineComponent* debugLine = m_EntityManager->GetComponent<DebugLineComponent>(*it);

				VertexPE1 v1, v2;

				if (transform3D != nullptr)
				{
					glm::mat4 worldMatrix = GetCachedWorldMatrix(_forward, *it, m_EntityManager, &l_MatrixCache);

					glm::vec4 vp1 = worldMatrix * glm::vec4(debugLine->p1, 1.0);
					glm::vec4 vp2 = worldMatrix * glm::vec4(debugLine->p2, 1.0);

					v1.p = glm::vec3(vp1.x, vp1.y, vp1.z);
					v2.p = glm::vec3(vp2.x, vp2.y, vp2.z);
				}
				else
				{
					v1.p = debugLine->p1;
					v2.p = debugLine->p2;
				}

				v1.e1 = v2.e1 = debugLine->color;

				if (debugLine->zTest)
				{
					linepointsWithZTest.push_back(v1);
					linepointsWithZTest.push_back(v2);
				}
				else
				{
					linepointsWithoutZTest.push_back(v1);
					linepointsWithoutZTest.push_back(v2);
				}
			}


			// 2.2 - now render all that info
			if (linepointsWithZTest.size() > 0)
			{
				if (linepointsWithZTest.size() > m_LargestDebugLines)
				{
					if (m_LargestDebugLines == 0) // never drawed before
					{
						m_DebugLinesVB = _wrapper.CreateVertexBuffer();

						m_DebugLinesVAO = _wrapper.CreateVertexArrayObject(
						{
							GLWrapper::VertexAttrib(0, 3, sizeof(VertexPE1), VertexPE1::PositionOffset(), GLWrapper::Type::FLOAT, GLWrapper::Normalization::UNMODIFIED, m_DebugLinesVB.get()),
							GLWrapper::VertexAttrib(4, 4, sizeof(VertexPE1), VertexPE1::ExtraVector1Offset(), GLWrapper::Type::FLOAT, GLWrapper::Normalization::UNMODIFIED, m_DebugLinesVB.get())
						});
					}

					_wrapper.BufferData(m_DebugLinesVB.get(), &linepointsWithZTest[0], linepointsWithZTest.size() * sizeof(VertexPE1), GLWrapper::BufferUssageFrequency::DYNAMIC);

					m_LargestDebugLines = linepointsWithZTest.size();
				}
				else
				{
					GLWrapper::BufferReplaceData brd;
					brd.offset = 0;
					brd.size = linepointsWithZTest.size() * sizeof(VertexPE1);
					brd.data = &linepointsWithZTest[0];

					_wrapper.ReplaceBufferData(m_DebugLinesVB.get(), &brd, 1);
				}

				static HashedString perVertexColorShader("perVertexColor"); // TODO

				_ShaderManager->UseProgram(perVertexColorShader);
				_wrapper.SetDepthTestMode(GLWrapper::DepthMode::ENABLED);

				_ShaderManager->SetProjectionMatrix(projectionMatrix);
				_ShaderManager->SetViewMatrix(viewMatrix);
				_ShaderManager->SetWorldMatrix(glm::mat4());

				_wrapper.DrawUnindexed(m_DebugLinesVAO.get(), GLWrapper::Primitive::LINES, 0, linepointsWithZTest.size());
			}
			if (linepointsWithoutZTest.size() > 0)
			{
				if (linepointsWithoutZTest.size() > m_LargestDebugLines)
				{
					if (m_LargestDebugLines == 0) // never drawed before
					{
						m_DebugLinesVB = _wrapper.CreateVertexBuffer();

						m_DebugLinesVAO = _wrapper.CreateVertexArrayObject(
						{
							GLWrapper::VertexAttrib(0, 3, sizeof(VertexPE1), VertexPE1::PositionOffset(), GLWrapper::Type::FLOAT, GLWrapper::Normalization::UNMODIFIED, m_DebugLinesVB.get()),
							GLWrapper::VertexAttrib(4, 4, sizeof(VertexPE1), VertexPE1::ExtraVector1Offset(), GLWrapper::Type::FLOAT, GLWrapper::Normalization::UNMODIFIED, m_DebugLinesVB.get())
						});
					}

					_wrapper.BufferData(m_DebugLinesVB.get(), &linepointsWithoutZTest[0], linepointsWithoutZTest.size() * sizeof(VertexPE1), GLWrapper::BufferUssageFrequency::DYNAMIC);

					m_LargestDebugLines = linepointsWithoutZTest.size();
				}
				else
				{
					GLWrapper::BufferReplaceData brd;
					brd.offset = 0;
					brd.size = linepointsWithoutZTest.size() * sizeof(VertexPE1);
					brd.data = &linepointsWithoutZTest[0];

					_wrapper.ReplaceBufferData(m_DebugLinesVB.get(), &brd, 1);
				}

				static HashedString perVertexColorShader("perVertexColor"); // TODO

				_ShaderManager->UseProgram(perVertexColorShader);
				_wrapper.SetDepthTestMode(GLWrapper::DepthMode::DISABLED);

				_ShaderManager->SetProjectionMatrix(projectionMatrix);
				_ShaderManager->SetViewMatrix(viewMatrix);
				_ShaderManager->SetWorldMatrix(glm::mat4());

				_wrapper.DrawUnindexed(m_DebugLinesVAO.get(), GLWrapper::Primitive::LINES, 0, linepointsWithoutZTest.size());
			}
		}


		void RenderProcessor::GetCameraParams(float _forward, glm::mat4& viewMatrix_, glm::mat4& projectionMatrix_)
		{
			// - Find camera
			EntityReference l_CameraEntity;
			CameraComponent *l_CameraComponent = nullptr;
			for (auto it = m_EntitiesWithCamera->begin(); it != m_EntitiesWithCamera->end(); ++it)
			{
				// Choose current camera, now selects the last one
				auto l_CameraCandidate = m_EntityManager->GetComponent<CameraComponent>(*it);
				if (l_CameraCandidate->active)
				{
					l_CameraEntity = *it;
					l_CameraComponent = l_CameraCandidate;
				}
			}

			if (l_CameraComponent == nullptr) {
				return; // no point rendering anything without a camera
			}

			viewMatrix_ = l_CameraComponent->camera.GetViewMatrix(); // TODO if the camera has a transform3D, this should be different
			projectionMatrix_ = l_CameraComponent->camera.GetProjectionMatrix();



			// -----------------------------------------------------
			if (Transform3DComponent* cameraTransform = m_EntityManager->GetComponent<Transform3DComponent>(l_CameraEntity))
			{
				glm::quat rot = cameraTransform->rotation;
				rot = rotate(rot, cameraTransform->rotation_speed, _forward);
				rot.w = -rot.w;
				glm::mat4 rotateMatrix = glm::mat4_cast(rot);
				glm::mat4 translationMatrix(1.0f);

				translationMatrix[3] = -glm::vec4(cameraTransform->position + cameraTransform->position_speed * _forward, -1.f);

				viewMatrix_ = viewMatrix_ * rotateMatrix * translationMatrix;
			}
			// -----------------------------------------------------
		}
	}
}
