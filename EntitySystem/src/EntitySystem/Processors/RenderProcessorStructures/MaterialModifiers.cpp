#include "stdafx.h"
#include "EntitySystem/Processors/RenderProcessorStructures/MaterialModifiers.hpp"

#include <Render/GLWrapper.hpp>
#include <Render/ShaderManager.hpp>
#include <Render/Texture.hpp>

#include "EntitySystem/Manager/EntityManager.hpp"
#include "EntitySystem/Components/Transform3DComponent.hpp"
#include "EntitySystem/Components/MeshComponent.hpp"


namespace qdrg {

	namespace entities {

		void CameraMaterialModifier::SetSharedData(const Material::SharedData& _SharedData) const
		{

			_SharedData.shaderManager->SetViewMatrix(*(_SharedData.viewMatrix));
			_SharedData.shaderManager->SetProjectionMatrix(*(_SharedData.projectionMatrix));

		}

		inline glm::mat4 GetCachedWorldMatrix(float forward, const EntityReference& entity, EntityManager *em, std::map<EntityReference, glm::mat4> *worldMatrixCache)
		{
			auto it = worldMatrixCache->find(entity);
			if (it != worldMatrixCache->end())
			{
				return it->second;
			}
			else
			{
				auto transform3D = em->GetComponent<Transform3DComponent>(entity);

				auto rot = rotate_fast(transform3D->rotation, transform3D->rotation_speed, forward);
				glm::mat4 worldMatrix = glm::mat4_cast(rot);
				worldMatrix *= transform3D->scale;
				worldMatrix[3] = glm::vec4(transform3D->position + transform3D->position_speed * forward, 1.0f);

				if (!transform3D->parent.IsEmptyEntity())
				{
					worldMatrix *= GetCachedWorldMatrix(forward, transform3D->parent, em, worldMatrixCache);
				}

				worldMatrixCache->insert(std::make_pair(entity, worldMatrix));

				return worldMatrix;
			}
		}

		void TransformMaterialModifier::SetInstanceData(const Material::InstanceData& _InstanceData) const
		{
			auto it = _InstanceData.matrixCache->find(*_InstanceData.entityReference);
			glm::mat4 worldMatrix;
			if (it != _InstanceData.matrixCache->end())
			{
				worldMatrix = it->second;
			}
			else
			{
				auto rot = rotate_fast(_InstanceData.transform3D->rotation, _InstanceData.transform3D->rotation_speed, _InstanceData.forward);
				worldMatrix = glm::mat4_cast(rot);
				worldMatrix *= _InstanceData.transform3D->scale;
				worldMatrix[3] = glm::vec4(_InstanceData.transform3D->position + _InstanceData.transform3D->position_speed * _InstanceData.forward, 1.0f);

				if (!_InstanceData.transform3D->parent.IsEmptyEntity())
				{
					auto parentMatrix = GetCachedWorldMatrix(_InstanceData.forward, _InstanceData.transform3D->parent, _InstanceData.entityManager, _InstanceData.matrixCache);
					worldMatrix = parentMatrix * worldMatrix;
				}
				_InstanceData.matrixCache->insert(std::make_pair(*_InstanceData.entityReference, worldMatrix));
			}

			_InstanceData.shaderManager->SetWorldMatrix(worldMatrix);
		}

		void ColorMaterialModifier::SetInstanceData(const Material::InstanceData& _InstanceData) const
		{
			_InstanceData.shaderManager->SetColor(_InstanceData.mesh->color);
		}


		TextureMaterialModifier::TextureMaterialModifier(const std::vector<HashedString>& _textures)
		{
			for (int i = 0; i < GLWrapper::MAX_TEXTURE_UNITS; i++)
			{
				if (i < _textures.size())
				{
					textures[i] = _textures[i];
				}
				else
				{
					textures[i] = HashedString();
				}
			}
		}

		TextureMaterialModifier::TextureMaterialModifier(HashedString _textures[GLWrapper::MAX_TEXTURE_UNITS])
		{
			for (int i = 0; i < GLWrapper::MAX_TEXTURE_UNITS; i++)
			{
				textures[i] = _textures[i];
			}
		}

		void TextureMaterialModifier::SetSharedData(const Material::SharedData& _SharedData) const
		{
			for (int i = 0; i < GLWrapper::MAX_TEXTURE_UNITS; i++)
			{
				if (!textures[i].IsEmptyString())
				{
					const Texture* texture = _SharedData.textureManager->GetTexture(textures[i]);
					_SharedData.wrapper->BindTexture(i, texture->GetTextureObject());
				}
			}
		}

	}
}