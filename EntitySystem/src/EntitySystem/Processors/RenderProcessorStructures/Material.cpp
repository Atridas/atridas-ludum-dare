#include "stdafx.h"
#include "EntitySystem/Processors/RenderProcessorStructures/Material.hpp"

#include <Render/GLWrapper.hpp>
#include <Render/ShaderManager.hpp>


namespace qdrg {

	namespace entities {

		void Material::AddModifier(std::shared_ptr<MaterialModifier> _MaterialModifier)
		{
			if (_MaterialModifier->ImplementsSharedData())
			{
				m_SharedDataModifiers.push_back(_MaterialModifier);
			}
			if (_MaterialModifier->ImplementsInstanceData())
			{
				m_InstanceDataModifiers.push_back(_MaterialModifier);
			}
		}

		void Material::SetSharedData(const SharedData& _SharedData) const
		{
			_SharedData.shaderManager->UseProgram(m_ShaderName);
			_SharedData.wrapper->SetDepthTestMode(m_DepthMode);

			for (auto modifier : m_SharedDataModifiers)
			{
				modifier->SetSharedData(_SharedData);
			}
		}

		void Material::SetInstanceData(const InstanceData& _InstanceData) const
		{
			for (auto modifier : m_InstanceDataModifiers)
			{
				modifier->SetInstanceData(_InstanceData);
			}
		}

	}
}