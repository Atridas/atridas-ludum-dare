#include "stdafx.h"
#include "EntitySystem/ESLocator.hpp"

#include "EntitySystem/Manager/EntityManager.hpp"

#include "EntitySystem/Processors/RenderProcessor.hpp"

namespace qdrg {

	namespace entities {

		namespace ESLocator {

			static EntityManager   *s_EntityManager   = nullptr;
			static RenderProcessor *s_RenderProcessor = nullptr;

			static std::function<void()> s_ClearFunction = [](){};

			void CreateEntityManager()
			{
				s_EntityManager = new EntityManager();

				auto l_ClearFunction = s_ClearFunction;

				s_ClearFunction = [l_ClearFunction]()
				{
					delete s_EntityManager;
					s_EntityManager = nullptr;
					l_ClearFunction();
				};
			}

			void CreateRenderProcessor()
			{
				assert(s_EntityManager != nullptr);
				s_RenderProcessor = new RenderProcessor(s_EntityManager);

				auto l_ClearFunction = s_ClearFunction;

				s_ClearFunction = [l_ClearFunction]()
				{
					delete s_RenderProcessor;
					s_RenderProcessor = nullptr;
					l_ClearFunction();
				};
			}

			void ClearAllSystems()
			{
				s_ClearFunction();
				s_ClearFunction = [](){};
			}

			EntityManager* GetEntityManager()
			{
				return s_EntityManager;
			}

			RenderProcessor* GetRenderProcessor()
			{
				return s_RenderProcessor;
			}


		}

	}
}