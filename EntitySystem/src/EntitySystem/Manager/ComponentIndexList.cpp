#include "stdafx.h"
#include "EntitySystem/Manager/ComponentIndexList.hpp"

namespace qdrg {

	namespace entities {


		ComponentIndexList::ComponentIndexList()
		{
			memset(m_Buffer, 0, sizeof(m_Buffer));
		}


		ComponentIndexList::~ComponentIndexList()
		{

		}

		void ComponentIndexList::Update(const std::vector<ComponentIndex>& _modify, const std::vector<ComponentIndex>& _add, const std::vector<unsigned int>& _remove)
		{
			size_t modifyIndex = 0;
			size_t addIndex = 0;
			size_t removeIndex = 0;

			unsigned int modifyId = (modifyIndex < _modify.size()) ? _modify[modifyIndex].entityId : UINT_MAX;
			unsigned int addId = (addIndex < _add.size()) ? _add[addIndex].entityId : UINT_MAX;
			unsigned int removeId = (removeIndex < _remove.size()) ? _remove[removeIndex] : UINT_MAX;
			
			while (modifyId < UINT_MAX || addId < UINT_MAX || removeId < UINT_MAX)
			{
				if (removeId < addId)
				{
					if (removeId < modifyId)
					{
						assert(Get(removeId) != 0); // remove something that is here!
						SetAtPosition(removeId, removeId, 0);

						assert((removeIndex + 1 == _remove.size()) || _remove[removeIndex + 1] > removeId); // que tot vingui ben ordenat
						removeIndex++;
						removeId = (removeIndex < _remove.size()) ? _remove[removeIndex] : UINT_MAX;
					}
					else
					{
						assert(modifyId < removeId); // modify something that is here!
						assert(Get(modifyId) != 0);
						assert(_modify[modifyIndex].componentOffset != 0); // 0 means remove!!!!
						SetAtPosition(modifyId, _modify[modifyIndex]);

						assert((modifyIndex + 1 == _modify.size()) || _modify[modifyIndex + 1].entityId > modifyId); // que tot vingui ben ordenat
						modifyIndex++;
						modifyId = (modifyIndex < _modify.size()) ? _modify[modifyIndex].entityId : UINT_MAX;
					}
				}
				else
				{
					if (addId < modifyId)
					{
						assert(Get(addId) == 0); // add something that is not here!
						assert(_add[addIndex].componentOffset != 0); // 0 means remove!!!!
						SetAtPosition(addId, _add[addIndex]);

						assert((addIndex + 1 == _add.size()) || _add[addIndex + 1].entityId > addId); // que tot vingui ben ordenat
						addIndex++;
						addId = (addIndex < _add.size()) ? _add[addIndex].entityId : UINT_MAX;
					}
					else
					{
						assert(modifyId < addId); // modify something that is here!

						assert(Get(modifyId) != 0);
						assert(_modify[modifyIndex].componentOffset != 0); // 0 means remove!!!!
						SetAtPosition(modifyId, _modify[modifyIndex]);

						assert((modifyIndex + 1 == _modify.size()) || _modify[modifyIndex + 1].entityId > modifyId); // que tot vingui ben ordenat
						modifyIndex++;
						modifyId = (modifyIndex < _modify.size()) ? _modify[modifyIndex].entityId : UINT_MAX;
					}
				}
			}

		}


		unsigned int ComponentIndexList::Get(unsigned int _entityId) const
		{
			return GetComponentOffsetAtPosition(_entityId);
		}


		unsigned int ComponentIndexList::GetEntityIdAtPosition(unsigned int _position) const
		{
			unsigned int baseIndex = (_position / 2) * 7;
			unsigned int internalIndex = _position % 2;


			unsigned int first = m_Buffer[baseIndex + internalIndex * 3 + 0];
			unsigned int second = (m_Buffer[baseIndex + internalIndex * 3 + 1] << 8);
			unsigned int third = m_Buffer[baseIndex + 6];

			third &= (0x08 << (internalIndex * 4));
			third <<= 13 - (internalIndex * 4);

			unsigned int result = first | second | third;
			return result;
		}

		unsigned int ComponentIndexList::GetComponentOffsetAtPosition(unsigned int _position) const
		{
			unsigned int baseIndex = (_position / 2) * 7;
			unsigned int internalIndex = _position % 2;

			unsigned int first = m_Buffer[baseIndex + internalIndex * 3 + 2];
			unsigned int second = m_Buffer[baseIndex + 6];

			second &= (0x07 << (internalIndex * 4));
			second <<= 8 - (internalIndex * 4);

			unsigned int result = first | second;

			result <<= 4;

			return result;
		}

		ComponentIndexList::ComponentIndex ComponentIndexList::GetAtPosition(unsigned int _position) const
		{
			unsigned int baseIndex = (_position / 2) * 7;
			unsigned int internalIndex = _position % 2;

			unsigned int mixed = m_Buffer[baseIndex + 6];

			unsigned int firstE = m_Buffer[baseIndex + internalIndex * 3 + 0];
			unsigned int secondE = (m_Buffer[baseIndex + internalIndex * 3 + 1] << 8);

			unsigned int thirdE = mixed & (0x08 << (internalIndex * 4));
			thirdE <<= 13 - (internalIndex * 4);

			unsigned int resultE = firstE | secondE | thirdE;
			
			unsigned int firstC = m_Buffer[baseIndex + internalIndex * 3 + 2];

			unsigned int secondC = mixed & (0x07 << (internalIndex * 4));
			secondC <<= 8 - (internalIndex * 4);

			unsigned int resultC = firstC | secondC;

			resultC <<= 4;

			return ComponentIndex(resultE, resultC);
		}

		void ComponentIndexList::SetAtPosition(uint8_t *_Buffer, unsigned int _position, unsigned int _entityId, unsigned int _componentOffset)
		{
			assert(_entityId < (1 << 17));
			assert(_componentOffset % 16 == 0);
			assert((_componentOffset / 16) < ((1 << 11) - 1));

			unsigned int baseIndex = (_position / 2) * 7;
			unsigned int internalIndex = _position % 2;

			unsigned int firstId = _entityId   & 0x000FF;
			unsigned int secondId = (_entityId & 0x0FF00) >> 8;
			unsigned int thirdId = ((_entityId & 0x10000) >> 13);
			_Buffer[baseIndex + internalIndex * 3 + 0] = (uint8_t)firstId;
			_Buffer[baseIndex + internalIndex * 3 + 1] = (uint8_t)secondId;

			unsigned int componentOffset = _componentOffset >> 4;

			unsigned int firstCmp = (componentOffset & 0x00FF);
			unsigned int secondCmp = (componentOffset & 0x0700) >> 8;
			_Buffer[baseIndex + internalIndex * 3 + 2] = (uint8_t)firstCmp;

			unsigned int mixed = thirdId | secondCmp;
			mixed <<= internalIndex * 4;

			unsigned int other = _Buffer[baseIndex + 6] & (0xF0 >> (internalIndex * 4));

			_Buffer[baseIndex + 6] = mixed | other;
		}

		void ComponentIndexList::SetAtPosition(unsigned int _position, unsigned int _entityId, unsigned int _componentOffset)
		{
			SetAtPosition(m_Buffer, _position, _entityId, _componentOffset);
		}

	}
}