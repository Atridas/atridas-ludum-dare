#include "stdafx.h"
#include "EntitySystem/Manager/EntityAdressList.hpp"

namespace qdrg {

	namespace entities {

		EntityAdressList::EntityAdressList()
		{
			memset(m_AdressList, 0, sizeof(m_AdressList));
		}


		EntityAdressList::~EntityAdressList()
		{
		}

		void EntityAdressList::SetEntityAddress(uint32_t _EntityIndex, uint32_t _Adress)
		{
			assert(_EntityIndex < (1 << 17));
			assert(_Adress < (1 << 29));
			assert((_Adress % 8)  == 0);

			unsigned int baseIndex = (_EntityIndex / 4) * 13;
			unsigned int subIndex = _EntityIndex % 4;

			_Adress >>= 3;

			unsigned int b0 = _Adress & 0x000000FF;
			unsigned int b1 = _Adress & 0x0000FF00;
			unsigned int b2 = _Adress & 0x00FF0000;
			unsigned int b3 = _Adress & 0xFF000000;

			b3 >>= (24 - (subIndex * 2));
			unsigned int mask = 0x3 << (subIndex * 2);

			m_AdressList[baseIndex + subIndex * 3 + 0] = (uint8_t)(b0 >> 0);
			m_AdressList[baseIndex + subIndex * 3 + 1] = (uint8_t)(b1 >> 8);
			m_AdressList[baseIndex + subIndex * 3 + 2] = (uint8_t)(b2 >> 16);

			m_AdressList[baseIndex + 12] ^= (uint8_t)(mask & (b3 ^ m_AdressList[baseIndex + 12]));
		}

		uint32_t EntityAdressList::GetEntityAddress(uint32_t _EntityIndex) const
		{
			assert(_EntityIndex < (1 << 17));

			unsigned int baseIndex = (_EntityIndex / 4) * 13;
			unsigned int subIndex = _EntityIndex % 4;

			unsigned int mask = 0x3 << (subIndex * 2);

			unsigned int b0 = m_AdressList[baseIndex + subIndex * 3 + 0];
			unsigned int b1 = m_AdressList[baseIndex + subIndex * 3 + 1];
			unsigned int b2 = m_AdressList[baseIndex + subIndex * 3 + 2];
			unsigned int b3 = m_AdressList[baseIndex + 12] & mask;

			b3 <<= 24 - (subIndex * 2);

			uint32_t adress = b0 | (b1 << 8) | (b2 << 16) | b3;

			return (adress << 3);
		}
	}
}
