#include "stdafx.h"
#include "EntitySystem/Manager/EntityManager.hpp"

#include "EntitySystem/Manager/EntityTemplate.hpp"
#include "EntitySystem/Manager/EntityCollection.hpp"

#include "EntitySystem/Components/Transform3DComponent.hpp"
#include "EntitySystem/Components/MeshComponent.hpp"


namespace qdrg {

	namespace entities {

		/*
		 *
		 * Entity Layout
		 * 0-7 B   (uint64_t): entity id -> 0-47 b: salt, 48-63 b: index
		 * 8-9 B   (uint16_t): offset to the first free adress (0: no free space)
		 * 10-11 B (uint16_t): total entity space
		 * 12-15 B (uint32_t): index to m_EntityTypes
		 * 16: component block / free space block
		 *
		 * [component block]
		 * component data
		 *
		 * [free space block]
		 * 0-3 B: free space
		 * 4-7 B: next block
		 *
		**/

		uint64_t EntityManager::GetEntityBlockId(uint32_t _EntityBlockAddress) const
		{
			return *(reinterpret_cast<const uint64_t*>(m_Data + _EntityBlockAddress + 0));
		}
		uint16_t EntityManager::GetEntityBlockFirstFreeSpace(uint32_t _EntityBlockAddress) const
		{
			return *(reinterpret_cast<const uint16_t*>(m_Data + _EntityBlockAddress + 8));
		}
		uint16_t EntityManager::GetEntityBlockTotalBlockSize(uint32_t _EntityBlockAddress) const
		{
			return *(reinterpret_cast<const uint16_t*>(m_Data + _EntityBlockAddress + 10));
		}
		uint32_t EntityManager::GetEntityBlockEntityTypeIndex(uint32_t _EntityBlockAddress) const
		{
			return *(reinterpret_cast<const uint32_t*>(m_Data + _EntityBlockAddress + 12));
		}

		void EntityManager::SetEntityBlockId(uint32_t _EntityBlockAddress, uint64_t _BlockId)
		{
			*(reinterpret_cast<uint64_t*>(m_Data + _EntityBlockAddress + 0)) = _BlockId;
		}
		void EntityManager::SetEntityBlockFirstFreeSpace(uint32_t _EntityBlockAddress, uint16_t _FirstFreeSpace)
		{
			*(reinterpret_cast<uint16_t*>(m_Data + _EntityBlockAddress + 8)) = _FirstFreeSpace;
		}
		void EntityManager::SetEntityBlockTotalBlockSize(uint32_t _EntityBlockAddress, uint16_t _TotalBlockSize)
		{
			*(reinterpret_cast<uint16_t*>(m_Data + _EntityBlockAddress + 10)) = _TotalBlockSize;
		}
		void EntityManager::SetEntityBlockEntityTypeIndex(uint32_t _EntityBlockAddress, uint32_t _EntityTypeIndex)
		{
			*(reinterpret_cast<uint32_t*>(m_Data + _EntityBlockAddress + 12)) = _EntityTypeIndex;
		}

		uint32_t EntityManager::GetComponentBlockSize(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset) const
		{
			return *(reinterpret_cast<const uint32_t*>(m_Data + _EntityBlockAddress + _ComponentAddressOffset + 0));
		}
		uint32_t EntityManager::GetComponentBlockNext(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset) const
		{
			return *(reinterpret_cast<const uint32_t*>(m_Data + _EntityBlockAddress + _ComponentAddressOffset + 4));
		}

		void EntityManager::SetComponentBlock(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset, uint32_t _Size, uint32_t _Next)
		{
			*(reinterpret_cast<uint32_t*>(m_Data + _EntityBlockAddress + _ComponentAddressOffset + 0)) = _Size;
			*(reinterpret_cast<uint32_t*>(m_Data + _EntityBlockAddress + _ComponentAddressOffset + 4)) = _Next;
		}

		void EntityManager::SetComponentBlockSize(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset, uint32_t _Size)
		{
			*(reinterpret_cast<uint32_t*>(m_Data + _EntityBlockAddress + _ComponentAddressOffset + 0)) = _Size;
		}
		void EntityManager::SetComponentBlockNext(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset, uint32_t _Next)
		{
			*(reinterpret_cast<uint32_t*>(m_Data + _EntityBlockAddress + _ComponentAddressOffset + 4)) = _Next;
		}

		/*
		 * [Empty space]
		 * 0-7   (uint64_t):0 (invalid entity id)
		 * 8-11  (uint32_t):block size
		 * 12-15 (uint32_t):next block adress (UINT32_MAX: no more blocks!)
		 *
		**/

		uint32_t EntityManager::GetEmptyBlockSize(uint32_t _EmptyBlockAddress) const
		{
			return *(reinterpret_cast<const uint32_t*>(m_Data + _EmptyBlockAddress + 8));
		}
		uint32_t EntityManager::GetEmptyBlockNext(uint32_t _EmptyBlockAddress) const
		{
			return *(reinterpret_cast<const uint32_t*>(m_Data + _EmptyBlockAddress + 12));
		}

		void EntityManager::SetEmptyBlock(uint32_t _EmptyBlockAddress, uint32_t _Size, uint32_t _Next)
		{
			*(reinterpret_cast<uint64_t*>(m_Data + _EmptyBlockAddress + 0)) = 0;
			*(reinterpret_cast<uint32_t*>(m_Data + _EmptyBlockAddress + 8)) = _Size;
			*(reinterpret_cast<uint32_t*>(m_Data + _EmptyBlockAddress + 12)) = _Next;
		}

		// ------------------------------------------------



		uint32_t EntityManager::GetEntityTypeIndex(EntityReference _entity)
		{
			uint32_t l_EntityAddress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entity.m_Id));
			assert(GetEntityBlockId(l_EntityAddress) == _entity.m_Id);

			return GetEntityBlockEntityTypeIndex(l_EntityAddress);
		}

		// ------------------------------------------------

#define DATA_SIZE (512 * 1024 * 1024)

		EntityManager::EntityManager()
			: m_LastIdUsed(0)
		{
			m_OriginalData = new uint8_t[DATA_SIZE + 16];

			uintptr_t alignment = reinterpret_cast<uintptr_t>(&m_OriginalData[0]) % 16;
			if (alignment == 0)
			{
				m_Data = m_OriginalData;
			}
			else
			{
				m_Data = m_OriginalData + 16 - alignment;
			}

			assert((reinterpret_cast<uintptr_t>(&m_Data[0]) % 16) == 0);

			memset(m_Data, 0, DATA_SIZE); // everything to 0

			// set
			m_FirstFreeAdress = 0;
			SetEmptyBlock(0, DATA_SIZE, UINT32_MAX);

			m_EntityTypes.push_back(std::unordered_set<HashedString>()); // first type is empty set, no components

			//

			m_AllComponents[Transform3DComponent::ComponentName] = &m_Transform3DComponents;
			m_AllComponents[MeshComponent::ComponentName] = &m_MeshComponents;
			// TODO add here all hardcoded component maps

		}


		EntityManager::~EntityManager()
		{
			delete[] m_OriginalData;
		}

#define CREATE_ENTITY_INTERNAL_ERROR 0xFFFFFFFF
		uint32_t EntityManager::CreateEntityInternal(uint32_t _Size, uint64_t _EntityId)
		{
			if (_Size % 16 > 0)
			{
				_Size += 16 - _Size % 16;
			}
			assert(_Size % 16 == 0); // alignment and such

			// step 1 search for the first block with enought space
			uint32_t l_CurrentBlockAdress = m_FirstFreeAdress;
			uint32_t l_LastBlockAdress = l_CurrentBlockAdress;
			uint32_t l_CurrentBlockSize = GetEmptyBlockSize(l_CurrentBlockAdress);
			while (l_CurrentBlockSize < _Size)
			{
				uint32_t l_NextBlock = GetEmptyBlockNext(l_CurrentBlockAdress);
				if (l_NextBlock != UINT32_MAX)
				{
					l_LastBlockAdress = l_CurrentBlockAdress;
					l_CurrentBlockAdress = l_NextBlock;
					l_CurrentBlockSize = GetEmptyBlockSize(l_CurrentBlockAdress);
				}
				else
				{
					// TODO error!!!!!
					return CREATE_ENTITY_INTERNAL_ERROR;
				}
			}

			// step 2 reserve the space
			if (l_CurrentBlockSize < _Size + 16 + 32) // don't create blocks with less than 32 Bytes (16 are header, wich is not accounted in entityExpectedSize
			{
				// just take the full block
				if (l_CurrentBlockAdress == l_LastBlockAdress)
				{
					// this is the first free block
					assert(l_CurrentBlockAdress == m_FirstFreeAdress);
					m_FirstFreeAdress = GetEmptyBlockNext(l_CurrentBlockAdress);
				}
				else
				{
					// last block now points to the next block
					assert(l_CurrentBlockAdress == GetEmptyBlockNext(l_LastBlockAdress));
					SetEmptyBlock(l_LastBlockAdress, GetEmptyBlockSize(l_LastBlockAdress), GetEmptyBlockNext(l_CurrentBlockAdress));
				}
			}
			else
			{
				// split this block
				if (l_CurrentBlockAdress == l_LastBlockAdress)
				{
					// this is the first free block
					assert(l_CurrentBlockAdress == m_FirstFreeAdress);
					m_FirstFreeAdress = l_CurrentBlockAdress + _Size + 16;
				}
				else
				{
					// last block now points to the next block
					assert(l_CurrentBlockAdress == GetEmptyBlockNext(l_LastBlockAdress));
					SetEmptyBlock(l_LastBlockAdress, GetEmptyBlockSize(l_LastBlockAdress), l_CurrentBlockAdress + _Size + 16);
				}

				SetEmptyBlock(l_CurrentBlockAdress + (_Size + 16), l_CurrentBlockSize - (_Size + 16), GetEmptyBlockNext(l_CurrentBlockAdress));

				l_CurrentBlockSize = _Size + 16; // we will use this
			}

			// step 4 write the entity header and save it's address
			SetEntityBlockId(l_CurrentBlockAdress, _EntityId);
			SetEntityBlockFirstFreeSpace(l_CurrentBlockAdress, 16);
			SetEntityBlockTotalBlockSize(l_CurrentBlockAdress, l_CurrentBlockSize);
			SetEntityBlockEntityTypeIndex(l_CurrentBlockAdress, 0);

			SetComponentBlock(l_CurrentBlockAdress, 16, l_CurrentBlockSize - 16, 0);


			m_EntityAdressList.SetEntityAddress(GET_ENTITY_INDEX(_EntityId), l_CurrentBlockAdress);
			m_LiveEntities.push_back(_EntityId);
			return l_CurrentBlockAdress;
		}

		EntityReference EntityManager::CreateEntity(std::shared_ptr<EntityTemplate> _entityTemplate)
		{
			// step 3 get a new entity id
			uint64_t l_EntityId;
			if (m_DeletedEntities.size() > 0) {
				auto first = m_DeletedEntities.begin();
				l_EntityId = first->second + 131072;
				m_DeletedEntities.erase(first);
			}
			else
			{
				m_LastIdUsed++;
				l_EntityId = m_LastIdUsed;
			}

			uint32_t l_EntityExpectedSize = _entityTemplate->GetExpectedMaxSize();
			uint32_t l_CurrentBlockAdress = CreateEntityInternal(l_EntityExpectedSize, l_EntityId);

			if (l_CurrentBlockAdress == CREATE_ENTITY_INTERNAL_ERROR)
			{
				return EntityReference(0); // TODO error!
			}

			// step 5 execute template
			_entityTemplate->ExecuteCreation(EntityReference(l_EntityId), *this);

			// step 6 return id
			return EntityReference(l_EntityId);
		}

		void EntityManager::SetEntityName(EntityReference _entity, HashedString _name)
		{
			assert(m_EntitiesByName.find(_name) == m_EntitiesByName.end());
			auto it = m_NameOfEntities.find(_entity);
			if (it != m_NameOfEntities.end())
			{
				// we are changing the name of the entity
				m_EntitiesByName.erase(it->second); // remove the old name
			}
			m_EntitiesByName[_name] = _entity;
			m_NameOfEntities[_entity] = _name;
		}

		EntityReference EntityManager::CreateEntity(HashedString _name, std::shared_ptr<EntityTemplate> _entityTemplate)
		{
			EntityReference ref = CreateEntity(_entityTemplate);
			assert(m_EntitiesByName.find(_name) == m_EntitiesByName.end());
			m_EntitiesByName[_name] = ref;
			m_NameOfEntities[ref] = _name;
			return ref;
		}

		void EntityManager::RemoveEntity(EntityReference _entity)
		{
			m_EntitiesToRemove.insert(_entity.m_Id);
		}

		unsigned int EntityManager::RemoveEntityInternal(uint64_t _entityId, bool _removeName)
		{
			// step 1 get the entity address
			uint32_t l_EntityAddress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entityId));
			if (GetEntityBlockId(l_EntityAddress) != _entityId)
			{
				return 0; // this entity has already been removed
			}

			auto l_EntityTypeIndex = GetEntityBlockEntityTypeIndex(l_EntityAddress);

			// step 2 search the blocks before and after the entity
			uint32_t l_NextBlockAdress = m_FirstFreeAdress;
			uint32_t l_LastBlockAdress = l_NextBlockAdress;
			while (l_NextBlockAdress < l_EntityAddress)
			{
				uint32_t l_NextBlock = GetEmptyBlockNext(l_NextBlockAdress);
				if (l_NextBlock != UINT32_MAX)
				{
					l_LastBlockAdress = l_NextBlockAdress;
					l_NextBlockAdress = l_NextBlock;
				}
				else
				{
					// TODO error!!!!! <- not found
					assert(false);
					return 0;
				}
			}

			// step 3 remove all components    -   skip this, it is handled externally
			//for (HashedString componentType : m_EntityTypes[GetEntityBlockEntityTypeIndex(l_EntityAddress)])
			//{
			//	m_ComponentsToRemove[componentType].push_back(GET_ENTITY_INDEX(_entityId));
			//}


			// step 4 merge this block with next, and copy block header
			uint32_t l_EntityBlockSize = GetEntityBlockTotalBlockSize(l_EntityAddress);
			if (l_EntityAddress + l_EntityBlockSize == l_NextBlockAdress)
			{
				//the "next" address is consecutive to the entity address
				SetEmptyBlock(l_EntityAddress, l_EntityBlockSize + GetEmptyBlockSize(l_NextBlockAdress), GetEmptyBlockNext(l_NextBlockAdress));
				l_EntityBlockSize = GetEmptyBlockSize(l_EntityAddress);
			}
			else
			{
				SetEmptyBlock(l_EntityAddress, l_EntityBlockSize, l_NextBlockAdress);
			}

			// step 5 merge this block with prev (if possible), or update block header
			if (l_LastBlockAdress == l_NextBlockAdress)
			{
				// this means we only checked the first address
				m_FirstFreeAdress = l_EntityAddress;
			}
			else if (l_LastBlockAdress + GetEmptyBlockSize(l_LastBlockAdress) == l_EntityAddress)
			{
				// we can merge
				SetEmptyBlock(l_LastBlockAdress, GetEmptyBlockSize(l_LastBlockAdress) + l_EntityBlockSize, GetEmptyBlockNext(l_EntityAddress));
			}
			else
			{
				SetEmptyBlock(l_LastBlockAdress, GetEmptyBlockSize(l_LastBlockAdress), l_EntityAddress);
			}

			bool found = false;
			for (auto it = m_LiveEntities.begin(); it != m_LiveEntities.end(); ++it)
			{
				if (*it == _entityId)
				{
					found = true;
					m_LiveEntities.erase(it);
					break;
				}
			}
			assert(found);

			if (_removeName)
			{
				auto it = m_NameOfEntities.find(EntityReference(_entityId));
				if (it != m_NameOfEntities.end())
				{
					// entity had a name
					m_EntitiesByName.erase(it->second);
					m_NameOfEntities.erase(it);
				}
			}

			return l_EntityTypeIndex;
		}

		void EntityManager::InternalComponentAddEvent(HashedString _componentType, unsigned int _entityIndex, unsigned int _componentOffset)
		{
			if (m_ComponentRemoveEvents[_componentType].count(_entityIndex) == 1)
			{
				m_ComponentRemoveEvents.erase(_componentType);
				m_ComponentModifyEvents[_componentType][_entityIndex] = _componentOffset;
			}
			else
			{
				auto addEventsForThisType = m_ComponentAddEvents.find(_componentType);
				if (addEventsForThisType == m_ComponentAddEvents.end())
				{
					if (m_AllComponents.count(_componentType) == 0)
					{
						ComponentIndexList* cil = new ComponentIndexList();
						m_OtherComponents[_componentType].reset(cil);
						m_AllComponents[_componentType] = cil;
					}
					m_ComponentAddEvents[_componentType] = std::map<unsigned int, unsigned int>();

					addEventsForThisType = m_ComponentAddEvents.find(_componentType);
				}

				assert(m_ComponentAddEvents[_componentType].count(_entityIndex) == 0); // can not add a component we are adding!
				assert(m_ComponentModifyEvents[_componentType].count(_entityIndex) == 0); // can not add a component we are modifying!

				addEventsForThisType->second[_entityIndex] = _componentOffset;
			}
		}
		void EntityManager::InternalComponentModifyEvent(HashedString _componentType, unsigned int _entityIndex, unsigned int _componentOffset)
		{
			if (m_ComponentAddEvents[_componentType].count(_entityIndex) == 1)
			{
				m_ComponentAddEvents[_componentType][_entityIndex] = _componentOffset;
			}
			else
			{
				assert(m_ComponentRemoveEvents[_componentType].count(_entityIndex) == 0); // cannot modify a component we are removing!
				m_ComponentModifyEvents[_componentType][_entityIndex] = _componentOffset;
			}
		}
		void EntityManager::InternalComponentRemoveEvent(HashedString _componentType, unsigned int _entityIndex)
		{
			if (m_ComponentAddEvents[_componentType].count(_entityIndex) == 1)
			{
				m_ComponentAddEvents[_componentType].erase(_entityIndex);
				m_ComponentRemoveEvents[_componentType].insert(_entityIndex);
			}
			else if (m_ComponentModifyEvents[_componentType].count(_entityIndex) == 1)
			{
				m_ComponentModifyEvents[_componentType].erase(_entityIndex);
				m_ComponentRemoveEvents[_componentType].insert(_entityIndex);
			}
			else
			{
				assert(m_ComponentRemoveEvents[_componentType].count(_entityIndex) == 0); // cannot remove a component we are already removing!
				m_ComponentRemoveEvents[_componentType].insert(_entityIndex);
			}
		}

		EntityReference EntityManager::FindEntity(HashedString _name) const
		{
			auto it = m_EntitiesByName.find(_name);
			if (it == m_EntitiesByName.end())
			{
				return EntityReference();
			}
			else
			{
				return it->second;
			}
		}

		bool EntityManager::IsEntityAlive(EntityReference _entity) const
		{
			uint32_t l_EntityAddress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entity.m_Id));
			return (GetEntityBlockId(l_EntityAddress) == _entity.m_Id);
		}

		std::shared_ptr<EntityCollection> EntityManager::GetAllEntitiesWithComponents(const std::set<HashedString>& _componentSet)
		{
			// step 1: search if this collection is already created
			for (auto wptr : m_EntityCollections)
			{
				if (auto entityCollection = wptr.lock()) // cast weak pointer to strong pointer, we ignore "lost" collections here
				{
					if (_componentSet.size() != entityCollection->m_ComponentRequeriments.size())
					{
						continue; // sets are different, test next one
					}
					bool equal = true; // equal true until proved false
					for (auto componentName : entityCollection->m_ComponentRequeriments)
					{
						if (_componentSet.find(componentName) == _componentSet.end())
						{
							equal = false;
							break; // we don't need to test anymore
						}
					}
					if (equal)
					{
						return entityCollection; // we found the collection we where looking for
					}
				}
			}

			//step 2, construct the collection
			std::shared_ptr<EntityCollection> l_Collection(new EntityCollection(_componentSet));

			//step 3, fill the collection
			for (auto entityId : m_LiveEntities)
			{
				uint32_t entityIndex = GET_ENTITY_INDEX(entityId);
				uint32_t entityAddress = m_EntityAdressList.GetEntityAddress(entityIndex);
				uint32_t entityTypeIndex = GetEntityBlockEntityTypeIndex( entityAddress );
				
				if (l_Collection->FullfillsRequeriments(m_EntityTypes[entityTypeIndex]))
				{
					l_Collection->m_EntitiesWithRequeriments.push_back(EntityReference(entityId));
				}

			}

			m_EntityCollections.push_back(l_Collection);

			return l_Collection;
		}


		void EntityManager::RelocateResizeAndDefragmentEntity(EntityReference _entityReference, size_t _newSize)
		{
			// step 0 we must have all pointers updated 
			PushInternalComponentOffsetIndexes();


			// step 1 : save all component data

			uint32_t l_EntityAddress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entityReference.m_Id));
			assert(GetEntityBlockId(l_EntityAddress) == _entityReference.m_Id);

			auto l_EntityType = GetEntityBlockEntityTypeIndex(l_EntityAddress);

			// component id                 , component size,   component data
			std::vector< std::tuple<HashedString, size_t, std::shared_ptr<uint8_t>> > m_SavedComponents;// (m_EntityTypes[l_EntityType].size());

			for (auto componentType : m_EntityTypes[l_EntityType])
			{
				assert(m_ComponentSizes.count(componentType) == 1);
				size_t l_ComponentSize = m_ComponentSizes[componentType];
				std::shared_ptr<uint8_t> l_ComponentData = std::shared_ptr<uint8_t>(new uint8_t[l_ComponentSize], [](uint8_t* _ptr)
				{
					delete [] _ptr;
				});

				uint8_t* l_OriginalComponentData = reinterpret_cast<uint8_t*>(GetComponent(_entityReference, componentType));
				memcpy(l_ComponentData.get(), l_OriginalComponentData, l_ComponentSize);

				m_SavedComponents.push_back(  std::make_tuple(componentType, l_ComponentSize, l_ComponentData)  );
			}

			// step 2 remove it
			RemoveEntityInternal(_entityReference.m_Id, false);

			// step 3 create it again
			CreateEntityInternal(_newSize, _entityReference.m_Id);

			// step 4 add the components again
			for (auto componentInfo : m_SavedComponents)
			{
				uint32_t l_NewOffset = AddComponentInternal(_entityReference, std::get<0>(componentInfo), std::get<1>(componentInfo), std::get<2>(componentInfo).get());

				InternalComponentModifyEvent(std::get<0>(componentInfo), GET_ENTITY_INDEX(_entityReference.m_Id), l_NewOffset);
			}
		}

		uint32_t EntityManager::AddComponentInternal(EntityReference _entity, const HashedString& _componentType, size_t _componentSize, uint8_t* _componentData)
		{
			// step 0 save the component size
			size_t l_SizeToReserve = _componentSize;
			if (l_SizeToReserve % 16 > 0)
			{
				l_SizeToReserve += 16 - l_SizeToReserve % 16;
			}
			assert(l_SizeToReserve % 16 == 0);
			assert(l_SizeToReserve >= _componentSize);
			assert(l_SizeToReserve < _componentSize + 16);
			{
				auto it = m_ComponentSizes.find(_componentType);
				if (it == m_ComponentSizes.end())
				{
					m_ComponentSizes[_componentType] = l_SizeToReserve;
				}
				else
				{
					assert(it->second == l_SizeToReserve);
				}
			}

			// step 1 get the entity address
			uint32_t l_EntityAddress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entity.m_Id));
			assert(GetEntityBlockId(l_EntityAddress) == _entity.m_Id);
			assert(m_EntityTypes[GetEntityBlockEntityTypeIndex(l_EntityAddress)].count(_componentType) == 0);

			// -------
			uint32_t l_Offset = GetEntityBlockFirstFreeSpace(l_EntityAddress);
			if (l_Offset == 0)
			{
				// resize entity so this component can be allocated
				RelocateResizeAndDefragmentEntity(_entity, GetEntityBlockTotalBlockSize(l_EntityAddress) + _componentSize);
				return AddComponentInternal(_entity, _componentType, _componentSize, _componentData);
			}
			uint32_t l_LastOffset = 0;
			uint32_t l_ComponentBlockSize = GetComponentBlockSize(l_EntityAddress, l_Offset);
			while (l_ComponentBlockSize < l_SizeToReserve)
			{
				l_LastOffset = l_Offset;
				l_Offset = GetComponentBlockNext(l_EntityAddress, l_Offset);
				if (l_Offset == 0)
				{
					// resize entity so this component can be allocated
					RelocateResizeAndDefragmentEntity(_entity, GetEntityBlockTotalBlockSize(l_EntityAddress) + _componentSize);
					return AddComponentInternal(_entity, _componentType, _componentSize, _componentData);
				}
			}
			
			if (l_ComponentBlockSize == l_SizeToReserve)
			{
				uint32_t l_NextOffset = GetComponentBlockNext(l_EntityAddress, l_Offset);
				if (l_LastOffset == 0)
				{
					SetEntityBlockFirstFreeSpace(l_EntityAddress, l_NextOffset);
				}
				else
				{
					SetComponentBlockNext(l_EntityAddress, l_LastOffset, l_NextOffset);
				}
			}
			else
			{
				uint32_t l_NextOffset = GetComponentBlockNext(l_EntityAddress, l_Offset);
				uint32_t l_NewOffset = l_Offset + l_SizeToReserve;
				uint32_t l_NewSize = l_ComponentBlockSize - l_SizeToReserve;

				SetComponentBlock(l_EntityAddress, l_NewOffset, l_NewSize, l_NextOffset);

				if (l_LastOffset == 0)
				{
					SetEntityBlockFirstFreeSpace(l_EntityAddress, l_NewOffset);
				}
				else
				{
					SetComponentBlockNext(l_EntityAddress, l_LastOffset, l_NewOffset);
				}
			}

			memcpy(m_Data + l_EntityAddress + l_Offset, _componentData, _componentSize);

			const std::unordered_set<HashedString> &l_PrevEntityType = m_EntityTypes[GetEntityBlockEntityTypeIndex(l_EntityAddress)];
			for (unsigned int i = 0; i < m_EntityTypes.size(); i++)
			{
				if (m_EntityTypes[i].size() != l_PrevEntityType.size() + 1)
				{
					continue; // we added one component, so we are searching an entity type with one more component
				}
				bool isCandidate = true;
				for (auto l_ComponentType : l_PrevEntityType)
				{
					if (m_EntityTypes[i].count(l_ComponentType) != 1)
					{
						isCandidate = false;
						break;
					}
				}
				if (isCandidate && m_EntityTypes[i].count(_componentType) == 1)
				{
					SetEntityBlockEntityTypeIndex(l_EntityAddress, i);
					return l_Offset;
				}
			}

			// if we are here, we haven't found a matching entity type, so we have to create it
			std::unordered_set<HashedString> l_EntityType = l_PrevEntityType;
			l_EntityType.insert(_componentType);

			SetEntityBlockEntityTypeIndex(l_EntityAddress, m_EntityTypes.size());
			m_EntityTypes.push_back(l_EntityType);

			return l_Offset;
		}

		void EntityManager::RemoveComponentInternal(EntityReference _entity, const HashedString& _componentType)
		{
			// step 1 get the entity address
			uint32_t l_EntityAddress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entity.m_Id));
			assert(GetEntityBlockId(l_EntityAddress) == _entity.m_Id);

			uint32_t l_ComponentOffset = m_AllComponents[_componentType]->Get(GET_ENTITY_INDEX(_entity.m_Id));

			// step 2 search the blocks before and after the component
			uint32_t l_NextBlockAdress = GetEntityBlockFirstFreeSpace(l_EntityAddress);
			uint32_t l_LastBlockAdress = l_NextBlockAdress;
			while (l_NextBlockAdress < l_ComponentOffset)
			{
				uint32_t l_NextBlock = GetComponentBlockNext(l_EntityAddress, l_NextBlockAdress);
				if (l_NextBlock != 0)
				{
					l_LastBlockAdress = l_NextBlockAdress;
					l_NextBlockAdress = l_NextBlock;
				}
				else
				{
					break; // after this component there is no free space
				}
			}


			// step 4 merge this block with next, and copy block header
			assert(m_ComponentSizes.count(_componentType) == 1);
			uint32_t l_ComponentBlockSize = m_ComponentSizes[_componentType];

			if (GetComponentBlockNext(l_EntityAddress, l_NextBlockAdress) == 0)
			{
				// there is no free space after this component
				SetComponentBlock(l_EntityAddress, l_ComponentOffset, l_ComponentBlockSize, 0);
			}
			else if (l_ComponentOffset + l_ComponentBlockSize == l_NextBlockAdress)
			{
				//the "next" address is consecutive to the entity address
				auto l_NewSize = l_ComponentBlockSize + GetComponentBlockSize(l_EntityAddress, l_NextBlockAdress);
				auto l_NextAddress = GetComponentBlockNext(l_EntityAddress, l_NextBlockAdress);
				SetComponentBlock(l_EntityAddress, l_ComponentOffset, l_NewSize, l_NextAddress);
				l_ComponentBlockSize = l_NewSize;
			}
			else
			{
				SetComponentBlock(l_EntityAddress, l_ComponentOffset, l_ComponentBlockSize, l_NextBlockAdress);
			}

			// step 5 merge this block with prev (if possible), or update block header
			if (l_LastBlockAdress == l_NextBlockAdress)
			{
				// this means we only checked the first address
				SetEntityBlockFirstFreeSpace(l_EntityAddress, l_ComponentOffset);
			}
			else if (l_LastBlockAdress + GetComponentBlockSize(l_EntityAddress, l_LastBlockAdress) == l_ComponentOffset)
			{
				// we can merge
				auto l_NewSize = l_ComponentBlockSize + GetComponentBlockSize(l_EntityAddress, l_LastBlockAdress);
				auto l_NextAddress = GetComponentBlockNext(l_EntityAddress, l_ComponentOffset);
				SetComponentBlock(l_EntityAddress, l_LastBlockAdress, l_NewSize, l_NextAddress);
			}
			else
			{
				SetComponentBlock(l_EntityAddress, l_LastBlockAdress, GetComponentBlockSize(l_EntityAddress, l_LastBlockAdress), l_ComponentOffset);
			}

			// step 6 update entity type
			const std::unordered_set<HashedString> &l_PrevEntityType = m_EntityTypes[GetEntityBlockEntityTypeIndex(l_EntityAddress)];
			for (unsigned int i = 0; i < m_EntityTypes.size(); i++)
			{
				if (m_EntityTypes[i].size() != l_PrevEntityType.size() - 1)
				{
					continue; // we added one component, so we are searching an entity type with one more component
				}
				bool isCandidate = true;
				for (auto l_ComponentType : l_PrevEntityType)
				{
					int hasThisComponentType = m_EntityTypes[i].count(l_ComponentType);
					if (hasThisComponentType != 1)
					{
						if (hasThisComponentType == 0 && l_ComponentType == _componentType) // the component we are missing is the one removed
						{
							continue;
						}
						else
						{
							isCandidate = false;
							break;
						}
					}
				}
				if (isCandidate)
				{
					SetEntityBlockEntityTypeIndex(l_EntityAddress, i);
					return;
				}
			}

			// if we are here, we haven't found a matching entity type, so we have to create it
			std::unordered_set<HashedString> l_EntityType = l_PrevEntityType;
			l_EntityType.erase(_componentType);

			SetEntityBlockEntityTypeIndex(l_EntityAddress, m_EntityTypes.size());
			m_EntityTypes.push_back(l_EntityType);
		}

		void* EntityManager::GetComponent(EntityReference _entityReference, HashedString _componentName)
		{
			auto it = m_AllComponents.find(_componentName);
			if (it != m_AllComponents.end())
			{
				uint32_t entityAdress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entityReference.GetId()));
				// check that the entity saved there is the same we are referencing
				uint64_t entityId = *(reinterpret_cast<uint64_t*>(m_Data + entityAdress));
				if (entityId == _entityReference.GetId())
				{
					ComponentIndexList* cil = it->second;
					auto componentAdress = cil->Get(GET_ENTITY_INDEX(entityId));
					if (componentAdress > 0)
					{
						return (void*)(m_Data + entityAdress + componentAdress);
					}
					else
					{
						return nullptr;
					}
				}
				else
				{
					return nullptr;
				}
			}
			else
			{
				return nullptr;
			}
		}

		void EntityManager::PushChanges()
		{
			// step 0: clean unused entity collections
			for (auto it = m_EntityCollections.begin(); it != m_EntityCollections.end(); ++it)
			{
				if (!it->lock())
				{
					do
					{
						m_EntityCollections.erase(it);
						it = m_EntityCollections.begin();
					} while (it != m_EntityCollections.end() && !it->lock());
				}
			}

			// step 1: remove all entities

			for (auto it : m_EntitiesToRemove)
			{
				auto l_EntityTypeIndex = RemoveEntityInternal(it);

				m_DeletedEntities.emplace(GET_ENTITY_INDEX(it), it);

				for (auto componentType : m_EntityTypes[l_EntityTypeIndex])
				{
					//m_ComponentRemoveCache[componentType].insert(GET_ENTITY_INDEX(it));
					InternalComponentRemoveEvent(componentType, GET_ENTITY_INDEX(it));
				}
				for (auto weakEntityCollection : m_EntityCollections)
				{
					auto entityCollection = weakEntityCollection.lock();
					assert(entityCollection);
					if (entityCollection->FullfillsRequeriments(m_EntityTypes[l_EntityTypeIndex]))
					{
						entityCollection->RemoveEntity(EntityReference(it)); // TODO create some events for this, maybe?
					}
				}
			}

			// step 2: remove all components

			for (auto componentsOfAType : m_ComponentsToRemove)
			{
				for (auto entitesFromWhereToRemove : componentsOfAType.second)
				{
					RemoveComponentInternal(entitesFromWhereToRemove, componentsOfAType.first);

					//m_ComponentRemoveCache[componentsOfAType.first].insert(GET_ENTITY_INDEX(entitesFromWhereToRemove.m_Id));
					InternalComponentRemoveEvent(componentsOfAType.first, GET_ENTITY_INDEX(entitesFromWhereToRemove.m_Id));

					auto entityTypeIndex = GetEntityTypeIndex(entitesFromWhereToRemove);

					for (auto weakEntityCollection : m_EntityCollections)
					{
						auto entityCollection = weakEntityCollection.lock();
						assert(entityCollection);

						if (entityCollection->m_ComponentRequeriments.count(componentsOfAType.first) == 0)
						{
							continue;
						}
						bool fulfiledReq = true;
						for (auto componentEntityStillHas : m_EntityTypes[entityTypeIndex])
						{
							if (entityCollection->m_ComponentRequeriments.count(componentEntityStillHas) == 0)
							{
								fulfiledReq = false;
								break;
							}
						}
						if (fulfiledReq)
						{
							entityCollection->RemoveEntity(entitesFromWhereToRemove); // TODO create some events for this, maybe?
						}
					}
				}
			}

			// step 3: add new components

			for (auto componentsOfAType : m_ComponentsToAdd)
			{
				for (auto componentInfo : componentsOfAType.second)
				{
					EntityReference entityId = std::get<0>(componentInfo);
					uint32_t componentOffset = AddComponentInternal(entityId, componentsOfAType.first, std::get<1>(componentInfo), std::get<2>(componentInfo).get());

					if (componentOffset != 0)
					{
						uint32_t entityIndex = GET_ENTITY_INDEX(entityId.m_Id);

						InternalComponentAddEvent(componentsOfAType.first, GET_ENTITY_INDEX(entityId.m_Id), componentOffset);


						auto entityTypeIndex = GetEntityTypeIndex(entityId);

						for (auto weakEntityCollection : m_EntityCollections)
						{
							auto entityCollection = weakEntityCollection.lock();
							assert(entityCollection);

							if (entityCollection->m_ComponentRequeriments.count(componentsOfAType.first) == 0)
							{
								continue;
							}
							else if (entityCollection->FullfillsRequeriments(m_EntityTypes[entityTypeIndex]))
							{
								entityCollection->AddEntity(entityId);
							}

						}
					}
				}
			}

			// reset this arrays
			m_EntitiesToRemove.clear();
			m_ComponentsToRemove.clear();
			m_ComponentsToAdd.clear();

			PushInternalComponentOffsetIndexes();

			// step 4: send creation / deletion events
			for (auto it = m_EntityCollections.begin(); it != m_EntityCollections.end(); ++it)
			{
				if (auto entityCollection = it->lock())
				{
					entityCollection->CallEvents();
				}
			}
		}

		void EntityManager::PushInternalComponentOffsetIndexes()
		{

			std::vector<ComponentIndexList::ComponentIndex> l_modify;
			std::vector<ComponentIndexList::ComponentIndex> l_add;
			std::vector<unsigned int> l_remove;

			// update references
			for (auto it : m_AllComponents)
			{
				auto type = it.first;
				auto cil = it.second;

				if (m_ComponentAddEvents[type].size() == 0 && m_ComponentModifyEvents[type].size() == 0 && m_ComponentRemoveEvents[type].size() == 0)
				{
					continue;
				}

				// resize vectors
				l_add.resize(m_ComponentAddEvents[type].size());
				l_modify.resize(m_ComponentModifyEvents[type].size());
				l_remove.resize(m_ComponentRemoveEvents[type].size());

				// fill vectors
				int i = 0;
				for (auto it2 : m_ComponentAddEvents[type])
				{
					l_add[i] = ComponentIndexList::ComponentIndex(it2.first, it2.second);
					i++;
				}
				i = 0;
				for (auto it2 : m_ComponentModifyEvents[type])
				{
					l_modify[i] = ComponentIndexList::ComponentIndex(it2.first, it2.second);
					i++;
				}
				i = 0;
				for (auto it2 : m_ComponentRemoveEvents[type])
				{
					l_remove[i] = it2;
					i++;
				}

				cil->Update(l_modify, l_add, l_remove);

				m_ComponentAddEvents[type].clear();
				m_ComponentModifyEvents[type].clear();
				m_ComponentRemoveEvents[type].clear();
			}
		}
	}
}