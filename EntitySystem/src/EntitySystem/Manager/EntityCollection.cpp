#include "stdafx.h"
#include "EntitySystem/Manager/EntityCollection.hpp"


namespace qdrg {

	namespace entities {

		EntityCollection::EntityCollection(const std::set<HashedString>& _ComponentRequeriments)
			:m_ComponentRequeriments(_ComponentRequeriments)
		{
		}


		EntityCollection::~EntityCollection()
		{
		}

		bool EntityCollection::FullfillsRequeriments(const std::unordered_set<HashedString>& _EntityComponents) const
		{
			for (auto req : m_ComponentRequeriments)
			{
				if (_EntityComponents.find(req) == _EntityComponents.end())
				{
					return false;
				}
			}

			return true;
		}

		void EntityCollection::AddEntity(EntityReference _entityToAdd)
		{
			m_EntitiesWithRequeriments.push_back(_entityToAdd);

			m_AddedEntities.insert(_entityToAdd);
			m_RemovedEntities.erase(_entityToAdd);
		}

		void EntityCollection::RemoveEntity(EntityReference _entityToRemove)
		{
			for (auto it = m_EntitiesWithRequeriments.begin(); it != m_EntitiesWithRequeriments.end(); ++it)
			{
				if (*it == _entityToRemove)
				{
					m_EntitiesWithRequeriments.erase(it);

					m_AddedEntities.erase(_entityToRemove);
					m_RemovedEntities.insert(_entityToRemove);
					return;
				}
			}
		}

		void EntityCollection::CallEvents()
		{
			for (auto addedEntity : m_AddedEntities)
			{
				m_EntityAddedToTheCollectionEvent(addedEntity);
			}
			for (auto removedEntity : m_RemovedEntities)
			{
				m_EntityRemovedfromTheCollectionEvent(removedEntity);
			}

			m_AddedEntities.clear();
			m_RemovedEntities.clear();
		}
	}
}
