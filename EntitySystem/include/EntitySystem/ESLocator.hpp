#pragma once

namespace qdrg {

	namespace entities {

		class EntityManager;
		class RenderProcessor;

		namespace ESLocator
		{

			void CreateEntityManager();
			void CreateRenderProcessor();

			void ClearAllSystems();

			EntityManager*   GetEntityManager();
			RenderProcessor* GetRenderProcessor();

		}

	}
}
