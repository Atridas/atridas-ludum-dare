#pragma once

#include "../Common.hpp"
#include "../Manager/EntityTemplate.hpp"
#include "../Components/Transform3DComponent.hpp"

namespace qdrg {

	namespace entities {

		namespace templates {

			class TemplatePosition : public EntityTemplate
			{
			public:
				TemplatePosition(const Transform3DComponent& _transform3D);
				TemplatePosition(const glm::vec3& _position, const glm::quat& _rotation, float _scale, EntityReference _parent = EntityReference());
				~TemplatePosition();

				size_t GetExpectedMaxSize() const;
				void ExecuteCreation(EntityReference _entity, EntityManager& entityManager) const;

			private:
				Transform3DComponent transform3D;
			};

		}
	}
}