#pragma once

#include "../Common.hpp"
#include "../Manager/EntityTemplate.hpp"
#include "../Components/Transform3DComponent.hpp"
#include "../Components/CameraComponent.hpp"

namespace qdrg {

	namespace entities {

		namespace templates {

			class TemplateCamera : public EntityTemplate
			{
			public:
				TemplateCamera(const Transform3DComponent& _transform3D, const CameraComponent& _camera);
				TemplateCamera(const CameraComponent& _camera, bool _withTransform = false);
				~TemplateCamera();

				size_t GetExpectedMaxSize() const;
				void ExecuteCreation(EntityReference _entity, EntityManager& entityManager) const;

			private:
				Transform3DComponent transform3D;
				CameraComponent camera;
				bool withTransform;
			};

		}
	}
}

