#pragma once

#include "../Common.hpp"
#include "../Manager/EntityTemplate.hpp"

namespace qdrg {

	namespace entities {

		namespace templates {

			template<class... Components> class TemplatedTemplate;

			template<class FirstComponent, class ...OtherComponents> class TemplatedTemplate<FirstComponent, OtherComponents...> : public EntityTemplate
			{

				static_assert(std::is_standard_layout<FirstComponent>::value, "FirstComponent should have standard layout");

			public:
				TemplatedTemplate(const FirstComponent& _first, const OtherComponents& ..._others) : first(_first), others(_others...) {};

				size_t GetExpectedMaxSize() const
				{
					return sizeof(FirstComponent) + others.GetExpectedMaxSize();
				}

				void ExecuteCreation(EntityReference _entity, EntityManager& entityManager) const
				{
					entityManager.AddComponent(_entity, first);
					others.ExecuteCreation(_entity, entityManager);
				};

			private:
				FirstComponent first;
				TemplatedTemplate<OtherComponents...> others;
			};

			template<> class TemplatedTemplate<> : public EntityTemplate
			{
			public:
				TemplatedTemplate() {};

				size_t GetExpectedMaxSize() const
				{
					return 0;
				}

				void ExecuteCreation(EntityReference _entity, EntityManager& entityManager) const { };
			};

		}
	}
}