#pragma once

#include "TemplatedTemplate.hpp"
#include "../Components/Transform3DComponent.hpp"
#include "../Components/MeshComponent.hpp"

namespace qdrg {

	namespace entities {

		namespace templates {

			typedef TemplatedTemplate<Transform3DComponent, MeshComponent> TemplateSimpleStaticMesh;

			/*class TemplateSimpleStaticMesh : public EntityTemplate
			{
			public:
				TemplateSimpleStaticMesh(const Transform3DComponent& _transform3D, const MeshComponent& _mesh);
				~TemplateSimpleStaticMesh();

				size_t GetExpectedMaxSize() const;
				void ExecuteCreation(EntityReference _entity, EntityManager& entityManager) const;

			private:
				Transform3DComponent transform3D;
				MeshComponent mesh;
			};*/

		}
	}
}
