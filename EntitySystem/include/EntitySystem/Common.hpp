#pragma once

#include <Common/Common.hpp>

namespace qdrg {

	namespace entities {

		static const size_t s_MaxEntities = 131072; // 2^17

	}
}