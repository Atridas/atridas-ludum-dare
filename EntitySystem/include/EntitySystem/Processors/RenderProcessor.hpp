#pragma once

#include <Render/GLWrapper.hpp>
#include <Render/Mesh.hpp>
#include <Render/Texture.hpp>

#include "../Manager/EntityManager.hpp"

#include "../Components/MeshComponent.hpp"
#include "../Components/Transform3DComponent.hpp"

#include "RenderProcessorStructures/Material.hpp"

namespace qdrg {

	class ShaderManager;

	namespace entities {

		class RenderProcessor : public TextureManager
		{
		public:

			RenderProcessor(EntityManager *_EntityManager);
			~RenderProcessor();

			void AddMesh(HashedString _MeshKey, std::unique_ptr<Mesh>&& _Mesh);
			void AddMaterial(HashedString _MaterialKey, std::unique_ptr<Material>&& _Material);
			void AddTexture(HashedString _TextureKey, std::unique_ptr<Texture>&& _Texture);

			void Update();
			void Render(float _forward, GLWrapper& _wrapper, ShaderManager* _ShaderManager);
			void DebugRender(float _forward, GLWrapper& _wrapper, ShaderManager* _ShaderManager);

			void GetCameraParams(float _forward, glm::mat4& viewMatrix_, glm::mat4& projectionMatrix_);

			const Texture* GetTexture(HashedString _Name) const override;

		private:

			std::map<HashedString, std::unique_ptr<Mesh> > m_Meshes;
			std::map<HashedString, std::unique_ptr<Material> > m_Materials;
			std::map<HashedString, std::unique_ptr<Texture> > m_Textures;

			std::shared_ptr<EntityCollection> m_EntitiesWithMeshes;
			std::shared_ptr<EntityCollection> m_EntitiesWithCamera;
			std::shared_ptr<EntityCollection> m_EntitiesWithDebugLines;

			// transition data
			std::vector< std::tuple<HashedString, std::unique_ptr<Mesh> > > m_AddedMeshes;
			std::vector< std::tuple<HashedString, std::unique_ptr<Texture> > > m_NotLoadedTextures;
			std::vector< std::tuple<HashedString, std::unique_ptr<Texture> > > m_NotUploadedTextures;

			// debug data
			unsigned int m_LargestDebugLines;
			std::unique_ptr<GLWrapper::VertexBuffer> m_DebugLinesVB;
			std::unique_ptr<GLWrapper::VertexArrayObject> m_DebugLinesVAO;

			EntityManager *m_EntityManager;
		};

	}
}
