#pragma once

#include <Common/Common.hpp>

#include <Render/GLWrapper.hpp>

#include "Material.hpp"

namespace qdrg {

	namespace entities {

		glm::mat4 GetCachedWorldMatrix(float forward, const EntityReference& entity, EntityManager *em, std::map<EntityReference, glm::mat4> *worldMatrixCache);

		class CameraMaterialModifier : public Material::MaterialModifier
		{
		public:

			bool ImplementsSharedData() const override { return true; }
			void SetSharedData(const Material::SharedData& _SharedData) const override;

		};

		class TransformMaterialModifier : public Material::MaterialModifier
		{
		public:

			bool ImplementsInstanceData() const override { return true; }
			void SetInstanceData(const Material::InstanceData& _InstanceData) const override;

		};

		class ColorMaterialModifier : public Material::MaterialModifier
		{
		public:

			bool ImplementsInstanceData() const override { return true; }
			void SetInstanceData(const Material::InstanceData& _InstanceData) const override;

		};

		class TextureMaterialModifier : public Material::MaterialModifier
		{
		public:
			TextureMaterialModifier(const std::vector<HashedString>& _textures);
			TextureMaterialModifier(HashedString _textures[GLWrapper::MAX_TEXTURE_UNITS]);

			bool ImplementsSharedData() const override { return true; }
			void SetSharedData(const Material::SharedData& _SharedData) const override;

		private:
			HashedString textures[GLWrapper::MAX_TEXTURE_UNITS];
		};

	}
}