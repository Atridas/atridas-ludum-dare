#pragma once

#include <Common/Common.hpp>

#include <Render/GLWrapper.hpp>

namespace qdrg {

	class ShaderManager;
	class TextureManager;

	namespace entities {
		
		struct Transform3DComponent;
		struct MeshComponent;
		class EntityReference;
		class EntityManager;

		class Material
		{
		public:

			struct SharedData
			{
				float forward;
				GLWrapper* wrapper;
				ShaderManager* shaderManager;
				const glm::mat4* viewMatrix;
				const glm::mat4* projectionMatrix;
				const TextureManager* textureManager;
			};

			struct InstanceData
			{
				float forward;
				GLWrapper* wrapper;
				ShaderManager* shaderManager;
				const Transform3DComponent* transform3D;
				const MeshComponent* mesh;
				const EntityReference* entityReference;
				EntityManager* entityManager;
				std::map<EntityReference, glm::mat4> *matrixCache;
			};

			class MaterialModifier
			{
			public:
				virtual ~MaterialModifier() {}

				virtual bool ImplementsSharedData() const { return false; }
				virtual bool ImplementsInstanceData() const { return false; }

				virtual void SetSharedData(const SharedData& _SharedData) const {};
				virtual void SetInstanceData(const InstanceData& _InstanceData) const {};

			};

		public:
			Material(HashedString _ShaderName, GLWrapper::DepthMode _DepthMode = GLWrapper::DepthMode::ENABLED) : m_ShaderName(_ShaderName), m_DepthMode(_DepthMode){};
			~Material() {}

			void AddModifier(std::shared_ptr<MaterialModifier> _MaterialModifier);

			void SetSharedData(const SharedData& _SharedData) const;
			void SetInstanceData(const InstanceData& _InstanceData) const;

		private:
			HashedString m_ShaderName;
			GLWrapper::DepthMode m_DepthMode;

			std::vector<std::shared_ptr<MaterialModifier>> m_SharedDataModifiers, m_InstanceDataModifiers;
		};

	}
}
