#pragma once

#include <Common/Common.hpp>

#include "EntitySystem/Manager/EntityReference.hpp"

namespace qdrg {

	namespace entities {

		struct DebugLineComponent
		{
			glm::vec3 p1, p2;
			glm::vec4 color;
			bool zTest;

			HashedString GetComponentName() const { return ComponentName; }
			static const HashedString ComponentName;
		};

		static_assert(std::is_standard_layout<DebugLineComponent>::value, "DebugLineComponent should have standard layout");
	}
}