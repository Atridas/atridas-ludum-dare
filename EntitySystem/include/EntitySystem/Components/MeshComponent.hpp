#pragma once

#include <Common/Common.hpp>

namespace qdrg {

	namespace entities {

		struct MeshComponent
		{
			HashedString meshName;
			HashedString materialName;

			glm::vec4 color;

			HashedString GetComponentName() const { return ComponentName; }
			static const HashedString ComponentName;
		};

		static_assert(std::is_standard_layout<MeshComponent>::value, "MeshComponent should have standard layout");

	}
}
