#pragma once

#include <Common/Common.hpp>

#include <Render/Camera.hpp>

#include "EntitySystem/Manager/EntityReference.hpp"

namespace qdrg {

	namespace entities {

		struct CameraComponent
		{
			Camera camera;
			bool active;

			HashedString GetComponentName() const { return ComponentName; }
			static const HashedString ComponentName;

			CameraComponent() : active(true) {}
		};

		static_assert(std::is_standard_layout<CameraComponent>::value, "CameraComponent should have standard layout");
	}
}

