#pragma once

#include <Common/Common.hpp>

#include "EntitySystem/Manager/EntityReference.hpp"

namespace qdrg {

	namespace entities {

		struct Transform3DComponent
		{
			glm::vec3 position;
			glm::quat rotation;
			float scale;

			glm::vec3 position_speed;
			glm::vec3 rotation_speed; // rotating len() radiants arrount this very axis
			float scale_speed;

			EntityReference parent;

			HashedString GetComponentName() const { return ComponentName; }
			static const HashedString ComponentName;

			Transform3DComponent() : scale(1), scale_speed(0) {}
		};

		static_assert(std::is_standard_layout<Transform3DComponent>::value, "Transform3DComponent should have standard layout");
	}
}
