


template<class Component>
void EntityManager::AddComponent(EntityReference _entity, const Component& _component)
{
	static_assert(std::is_standard_layout<Component>::value, "Compoments should have standard layout");
	m_ComponentsToAdd[Component::ComponentName].push_back(
		std::make_tuple(
			_entity,
			sizeof(Component),
			std::shared_ptr<uint8_t>(
				reinterpret_cast<uint8_t*>(new Component(_component)),
				[](uint8_t* _ptr)
				{
					delete reinterpret_cast<Component*>(_ptr);
				}
			)
		)
	);
}

template<class Component>
void EntityManager::RemoveComponent(EntityReference _entity)
{
	static_assert(std::is_standard_layout<Component>::value, "Compoments should have standard layout");
	m_ComponentsToRemove[Component::ComponentName].push_back(_entity);
}

template<>
inline Transform3DComponent* EntityManager::GetComponent<Transform3DComponent>(EntityReference _entityReference)
{
	uint32_t entityAdress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entityReference.GetId()));
	// check that the entity saved there is the same we are referencing
	uint64_t entityId = *(reinterpret_cast<uint64_t*>(m_Data + entityAdress));
	if (entityId == _entityReference.GetId())
	{
		auto componentAdress = m_Transform3DComponents.Get(GET_ENTITY_INDEX(entityId));
		if (componentAdress > 0)
		{
			return (Transform3DComponent*)(m_Data + entityAdress + componentAdress);
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		return nullptr;
	}
}

template<class Component>
Component* EntityManager::GetComponent(EntityReference _entityReference)
{
	static_assert(std::is_standard_layout<Component>::value, "Compoments should have standard layout");
	auto it = m_AllComponents.find(Component::ComponentName);
	if (it != m_AllComponents.end())
	{
		uint32_t entityAdress = m_EntityAdressList.GetEntityAddress(GET_ENTITY_INDEX(_entityReference.GetId()));
		// check that the entity saved there is the same we are referencing
		uint64_t entityId = *(reinterpret_cast<uint64_t*>(m_Data + entityAdress));
		if (entityId == _entityReference.GetId())
		{
			ComponentIndexList* cil = it->second;
			auto componentAdress = cil->Get(GET_ENTITY_INDEX(entityId));
			if (componentAdress > 0)
			{
				return (Component*)(m_Data + entityAdress + componentAdress);
			}
			else
			{
				return nullptr;
			}
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		return nullptr;
	}
}

