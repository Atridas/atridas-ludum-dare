#pragma once

#include "../Common.hpp"

namespace UnitTest
{
	class ComponentIndexListUnitTest;
};

namespace qdrg {

	namespace entities {

		class ComponentIndexList
		{
		public:
			struct ComponentIndex {
				unsigned int entityId;
				unsigned int componentOffset;

				ComponentIndex() {}
				ComponentIndex(unsigned int _entityId, unsigned int _componentOffset) :entityId(_entityId), componentOffset(_componentOffset) {}
			};
		public:
			ComponentIndexList();
			~ComponentIndexList();

			void Update(const std::vector<ComponentIndex>& _modify, const std::vector<ComponentIndex>& _add, const std::vector<unsigned int>& _remove); // must be ordered by entityId

			unsigned int Get(unsigned int _entityId) const; // returns UINT_MAX if the entity does not have this component

		private:
			ComponentIndexList(const ComponentIndexList&) {};
			ComponentIndexList& operator=(const ComponentIndexList&) { return *this; }; // disallow copy

			unsigned int GetEntityIdAtPosition(unsigned int _position) const;
			unsigned int GetComponentOffsetAtPosition(unsigned int _position) const;
			ComponentIndex GetAtPosition(unsigned int _position) const;

			static void SetAtPosition(uint8_t *_Buffer, unsigned int _position, unsigned int _entityId, unsigned int _componentOffset);
			void SetAtPosition(unsigned int _position, unsigned int _entityId, unsigned int _componentOffset);
			void SetAtPosition(unsigned int _position, ComponentIndex componentIndex) { SetAtPosition(_position, componentIndex.entityId, componentIndex.componentOffset); };

			uint8_t m_Buffer[s_MaxEntities * 7 / 2];

			friend class UnitTest::ComponentIndexListUnitTest;
		};

	}
}
