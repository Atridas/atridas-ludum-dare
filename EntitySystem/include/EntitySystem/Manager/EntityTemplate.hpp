#pragma once

#include "../Common.hpp"
#include "EntityReference.hpp"

namespace qdrg {

	namespace entities {

		class EntityManager;

		class EntityTemplate
		{
		public:
			virtual ~EntityTemplate();

			virtual size_t GetExpectedMaxSize() const = 0;

			virtual void ExecuteCreation(EntityReference _entity, EntityManager& entityManager) const = 0;

		protected:
			EntityTemplate();
		};

	}
}
