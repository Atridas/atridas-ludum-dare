#pragma once

#include "../Common.hpp"
#include <Common/EventSystem.hpp>
#include "EntityReference.hpp"

namespace qdrg {

	namespace entities {

		class EntityManager;

		class EntityCollection
		{
		public:
			~EntityCollection();

			const std::set<HashedString>& GetRequeriments() const { return m_ComponentRequeriments; }
			bool FullfillsRequeriments(const std::unordered_set<HashedString>& _EntityComponents) const;

			unsigned int SizeOfCollection() const { return m_EntitiesWithRequeriments.size(); };

			std::vector<EntityReference>::const_iterator begin() const { return m_EntitiesWithRequeriments.cbegin(); }
			std::vector<EntityReference>::const_iterator end() const { return m_EntitiesWithRequeriments.cend(); }

			Event<EntityReference>& EntityAddedToTheCollectionEvent() { return m_EntityAddedToTheCollectionEvent; };
			Event<EntityReference>& EntityRemovedfromTheCollectionEvent() { return m_EntityRemovedfromTheCollectionEvent; };

		private:
			EntityCollection(const std::set<HashedString>& _ComponentRequeriments);

			void AddEntity(EntityReference _entityToAdd);
			void RemoveEntity(EntityReference _entityToRemove);

			void CallEvents();

			std::set<HashedString> m_ComponentRequeriments;
			std::vector<EntityReference> m_EntitiesWithRequeriments;

			std::set<EntityReference> m_AddedEntities, m_RemovedEntities;

			Event<EntityReference> m_EntityAddedToTheCollectionEvent;
			Event<EntityReference> m_EntityRemovedfromTheCollectionEvent;

			friend class EntityManager;
		};

	}
}
