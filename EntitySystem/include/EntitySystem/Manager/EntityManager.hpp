#pragma once

#include "../Common.hpp"
#include "EntityAdressList.hpp"
#include "ComponentIndexList.hpp"
#include "EntityReference.hpp"

#include "../Components/MeshComponent.hpp"
#include "../Components/Transform3DComponent.hpp"

// 2^17 -1
#define ENTITY_INDEX_MASK               131071L
#define GET_ENTITY_INDEX(entityId)      ((uint32_t)(entityId & ENTITY_INDEX_MASK))

namespace qdrg {

	namespace entities {

		class EntityTemplate;
		class EntityCollection;

		class EntityManager
		{
		public:
			EntityManager();
			~EntityManager();

			EntityReference CreateEntity(std::shared_ptr<EntityTemplate> _entityTemplate);
			EntityReference CreateEntity(HashedString _name, std::shared_ptr<EntityTemplate> _entityTemplate);
			void RemoveEntity(EntityReference _entity);
			void SetEntityName(EntityReference _entity, HashedString _name);

			EntityReference FindEntity(HashedString _name) const;
			bool IsEntityAlive(EntityReference _entity) const;

			std::shared_ptr<EntityCollection> GetAllEntitiesWithComponents(const std::set<HashedString>& _componentSet);

			// this component will not be accessible until next frame!
			template<class Component> void AddComponent(EntityReference _entity, const Component& _component);
			template<class Component> void RemoveComponent(EntityReference _entity);

			void* GetComponent(EntityReference _entityReference, HashedString _componentName);
			template<class Component> Component* GetComponent(EntityReference _entityReference);

			void PushChanges();

		private:
			
			// --------
			uint64_t GetEntityBlockId(uint32_t _EntityBlockAddress) const;
			uint16_t GetEntityBlockFirstFreeSpace(uint32_t _EntityBlockAddress) const;
			uint16_t GetEntityBlockTotalBlockSize(uint32_t _EntityBlockAddress) const;
			uint32_t GetEntityBlockEntityTypeIndex(uint32_t _EntityBlockAddress) const;

			void SetEntityBlockId(uint32_t _EntityBlockAddress, uint64_t _BlockId);
			void SetEntityBlockFirstFreeSpace(uint32_t _EntityBlockAddress, uint16_t _FirstFreeSpace);
			void SetEntityBlockTotalBlockSize(uint32_t _EntityBlockAddress, uint16_t _TotalBlockSize);
			void SetEntityBlockEntityTypeIndex(uint32_t _EntityBlockAddress, uint32_t _EntityTypeIndex);

			uint32_t GetComponentBlockSize(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset) const;
			uint32_t GetComponentBlockNext(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset) const;

			void SetComponentBlock(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset, uint32_t _Size, uint32_t _Next);
			void SetComponentBlockSize(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset, uint32_t _Size);
			void SetComponentBlockNext(uint32_t _EntityBlockAddress, uint16_t _ComponentAddressOffset, uint32_t _Next);

			uint32_t GetEmptyBlockSize(uint32_t _EmptyBlockAddress) const;
			uint32_t GetEmptyBlockNext(uint32_t _EmptyBlockAddress) const;

			void SetEmptyBlock(uint32_t _EmptyBlockAddress, uint32_t _Size, uint32_t _Next);

			uint32_t GetEntityTypeIndex(EntityReference _entity);
			// --------

			uint32_t CreateEntityInternal(uint32_t _Size, uint64_t _EntityId);
			unsigned int RemoveEntityInternal(uint64_t _entityId, bool _removeName = true); // return entity type
			uint32_t AddComponentInternal(EntityReference _entity, const HashedString& _componentType, size_t _componentSize, uint8_t* _componentData);
			void RemoveComponentInternal(EntityReference _entity, const HashedString& _componentType);

			void RelocateResizeAndDefragmentEntity(EntityReference _entityReference, size_t _newSize);

			void InternalComponentAddEvent(HashedString _componentType, unsigned int _entityIndex, unsigned int _componentOffset);
			void InternalComponentModifyEvent(HashedString _componentType, unsigned int _entityIndex, unsigned int _componentOffset);
			void InternalComponentRemoveEvent(HashedString _componentType, unsigned int _entityIndex);

			void PushInternalComponentOffsetIndexes();

			// -------


			// entity data
			//ALIGNMENT_16B uint8_t m_Data[512 * 1024 * 1024]; // 512 MB
			uint8_t *m_Data, *m_OriginalData;

			uint32_t m_FirstFreeAdress;

			// entity addrecess
			EntityAdressList m_EntityAdressList;

			// entity ids has 2 parts: an index and a salt. Index are the lower 17 bits, salt are the higer 47.
			// Each time we need to recycle a entity id, we have to add 131072

			std::map<uint32_t, uint64_t> m_DeletedEntities; // entities that were deleted and can now be re-used
															// key is index, value is index + salt

			std::vector<uint64_t> m_LiveEntities; // list ordered by address of all living entities
			uint32_t m_LastIdUsed;

			std::unordered_map<HashedString, EntityReference> m_EntitiesByName;
			std::unordered_map<EntityReference, HashedString> m_NameOfEntities;

			std::vector<std::unordered_set<HashedString>> m_EntityTypes;

			// components to add
			std::unordered_map<HashedString, std::vector<std::tuple<EntityReference, size_t, std::shared_ptr<uint8_t> >> > m_ComponentsToAdd;

			// components to be removed
			std::unordered_map<HashedString, std::vector<EntityReference>> m_ComponentsToRemove; // by type

			std::unordered_map<HashedString, size_t> m_ComponentSizes;

			// entities to remove
			std::set<uint64_t> m_EntitiesToRemove;

			// hardcoded component maps
			ComponentIndexList m_Transform3DComponents;
			ComponentIndexList m_MeshComponents;

			// "regular" component maps
			std::unordered_map<HashedString, std::unique_ptr<ComponentIndexList>> m_OtherComponents;

			// map to all component maps
			std::unordered_map<HashedString, ComponentIndexList*> m_AllComponents;

			// Component Address modification lists cach�
			std::unordered_map<HashedString, std::map<unsigned int, unsigned int>> m_ComponentAddEvents, m_ComponentModifyEvents;
			std::unordered_map<HashedString, std::set<unsigned int>>               m_ComponentRemoveEvents;

			// collections of entities
			std::vector<std::weak_ptr<EntityCollection>> m_EntityCollections;
		};

#include "EntityManager.inl"
	}
}