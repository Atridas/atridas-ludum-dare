#pragma once

#include "../Common.hpp"

namespace qdrg {

	namespace entities {

		class EntityAdressList
		{
		public:
			EntityAdressList();
			~EntityAdressList();

			void SetEntityAddress(uint32_t _EntityIndex, uint32_t _Adress);
			uint32_t GetEntityAddress(uint32_t _EntityIndex) const;

		private:
			uint8_t m_AdressList[s_MaxEntities * 13 / 4];
		};

	}
}