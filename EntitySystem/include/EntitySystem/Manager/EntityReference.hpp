#pragma once

#include "../Common.hpp"

namespace qdrg {

	namespace entities {

		class EntityManager;

		class EntityReference
		{
		public:
			EntityReference();
			~EntityReference();

			bool operator==(const EntityReference& _other) const { return m_Id == _other.m_Id; }
			bool operator!=(const EntityReference& _other) const { return m_Id != _other.m_Id; }
			bool operator<(const EntityReference& _other) const { return m_Id < _other.m_Id; }

			uint64_t GetId() const { return m_Id; }

			bool IsEmptyEntity() const { return m_Id == 0; }

		private:
			EntityReference(uint64_t _Id) :m_Id(_Id) {};
			uint64_t m_Id;

			friend class EntityManager;
		};
	}
}

namespace std {

	template<>
	struct hash<qdrg::entities::EntityReference>
	{
		inline size_t operator()(const qdrg::entities::EntityReference& v) const
		{
			return (size_t)(v.GetId() & 0xffffffffL);
		}
	};

} /* namespace std */
