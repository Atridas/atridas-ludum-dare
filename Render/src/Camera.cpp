#include "stdafx.h"

#include "Camera.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace qdrg {

	Camera::Camera()
	{
	}


	Camera::~Camera()
	{
	}

	const glm::mat4& Camera::GetViewMatrix() const
	{
		if (!m_ViewValid)
		{
			m_View = glm::lookAt(m_Position, m_Center, m_Up);
			m_ViewValid = true;
		}
		return m_View;
	}

	const glm::mat4& Camera::GetProjectionMatrix() const
	{
		if (!m_ProjectionValid)
		{
			m_Projection = glm::perspectiveFov(m_Fov, m_Width, m_Height, m_Near, m_Far);
			m_ProjectionValid = true;
		}
		return m_Projection;
	}

}