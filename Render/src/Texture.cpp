#include "stdafx.h"
#include "Texture.hpp"

#include <gli/gli.hpp>

namespace qdrg {
	Texture::Texture()
	{
	}


	Texture::~Texture()
	{
	}

	void Texture::LoadFromDisk()
	{
		gli::texture2D texture(gli::load_dds(m_Path.c_str()));

		// TODO level offset if 
		assert(texture.levels() <= MAX_MIPMAP_LEVELS);

		m_Levels = texture.levels();
		m_Format = texture.format();

		m_Width = texture.dimensions().x;
		m_Height = texture.dimensions().y;
			
		for (int l = 0; l < m_Levels; ++l)
		{
			m_Widths[l] = texture[l].dimensions().x;
			m_Heights[l] = texture[l].dimensions().y;
			m_Sizes[l] = texture[l].size();

			m_Data[l].reset(new uint8_t[m_Sizes[l]]);
			memcpy(m_Data[l].get(), texture[l].data(), m_Sizes[l]);
		}

	}

	void Texture::PrepareGPU_State(GLWrapper& _wrapper)
	{
		m_TextureObject = _wrapper.CreateTexture2D(m_Levels > 1);

		gli::format format = gli::format(m_Format);

		uint8_t* dataPointers[MAX_MIPMAP_LEVELS];
		for (int l = 0; l < m_Levels; ++l)
		{
			dataPointers[l] = m_Data[l].get();
		}

		if (gli::is_compressed(format))
		{
			_wrapper.SetTextureCompressedData(m_TextureObject.get(), gli::internal_format(format), m_Levels, m_Width, m_Height, m_Widths, m_Heights, m_Sizes, dataPointers);
		}
		else
		{
			_wrapper.SetTextureData(m_TextureObject.get(), gli::internal_format(format), gli::external_format(format), gli::type_format(format), m_Levels, m_Width, m_Height, m_Widths, m_Heights, dataPointers);
		}

		if (m_Levels > 1)
		{
			_wrapper.SetTextureFilter(m_TextureObject.get(), GLWrapper::TextureFilter::TRILINEAR); // TODO make this parametrizable
		}
		else
		{
			_wrapper.SetTextureFilter(m_TextureObject.get(), GLWrapper::TextureFilter::LINEAR);
		}
	}
}
