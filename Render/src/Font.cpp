#include "stdafx.h"
#include "Font.hpp"

namespace qdrg {

	void Font::ParseTextFile(const std::string& _Path)
	{
		ParseText(LoadFileToString(_Path));
	}

	static void splitPair(const std::string& original, std::string& name, std::string& value)
	{
		size_t pos = original.find("=");
		name = original.substr(0, pos);
		value = original.substr(pos + 1, original.length() - (pos + 1));
	}

	void Font::ParseText(const std::string& _fnt)
	{
		std::stringstream ssScript(_fnt);

		std::string line;

		while (std::getline(ssScript, line))
		{
			if (line.size() == 0)
			{
				continue;
			}

			std::stringstream instruction(line);

			std::string action;
			instruction >> action;

			if (action == "common")
			{
				std::string pair;
				std::string name, value;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "lineHeight");
				std::stringstream(value) >> lineHeight;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "base");
				std::stringstream(value) >> base;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "scaleW");
				std::stringstream(value) >> scaleW;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "scaleH");
				std::stringstream(value) >> scaleH;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "pages");
				std::stringstream(value) >> pages;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "packed");
				std::stringstream(value) >> packed;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "alphaChnl");
				std::stringstream(value) >> alphaChnl;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "redChnl");
				std::stringstream(value) >> redChnl;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "greenChnl");
				std::stringstream(value) >> greenChnl;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "blueChnl");
				std::stringstream(value) >> blueChnl;
			}
			else if (action == "page")
			{
				std::string pair;
				std::string name, value;

				unsigned int id;
				std::string textureName;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "id");
				std::stringstream(value) >> id;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "file");
				textureName = value.substr(1, value.length() - 2);

				if (texturesPerPage.size() < id + 1)
				{
					texturesPerPage.resize(id + 1);
				}
				texturesPerPage[id] = HashedString(textureName);
			}
			else if (action == "char")
			{
				int id;
				Char c;

				std::string pair;
				std::string name, value;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "id");
				std::stringstream(value) >> id;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "x");
				std::stringstream(value) >> c.x;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "y");
				std::stringstream(value) >> c.y;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "width");
				std::stringstream(value) >> c.width;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "height");
				std::stringstream(value) >> c.height;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "xoffset");
				std::stringstream(value) >> c.xoffset;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "yoffset");
				std::stringstream(value) >> c.yoffset;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "xadvance");
				std::stringstream(value) >> c.xadvance;

				int aux;
				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "page");
				std::stringstream(value) >> aux;
				c.page = aux;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "chnl");
				std::stringstream(value) >> aux;
				c.chnl = aux;

				assert(c.page < texturesPerPage.size());
				// todo assert c.chnl

				characters[((wchar_t)id)] = c;
			}
			else if (action == "kerning")
			{
				int first, second, amount;

				std::string pair;
				std::string name, value;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "first");
				std::stringstream(value) >> first;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "second");
				std::stringstream(value) >> second;

				instruction >> pair;
				splitPair(pair, name, value);
				assert(name == "amount");
				std::stringstream(value) >> amount;

				assert(characters.find((wchar_t)first) != characters.end());
				assert(characters.find((wchar_t)second) != characters.end());
				assert(first != 0); // this is an assumption other functions take

				kernings[(wchar_t)first][(wchar_t)second] = amount;
			}

		}
	}

	int Font::FillVertexBufferWithText(const std::string& _text, VertexPT1* vb, int maxLen)
	{
		int currentVertex = 0;
		wchar_t last = 0;
		
		int currentX = 0, currentY = 0;

		// texture
		//             x       x + width
		//     y       3---------2
		//             |         |
		//             |         |
		//             |         |
		//             |         |
		//             |         |
		//             |         |
		// y + height  0---------1


		// texture
		//             x       x + width
		//     y       3---------2
		//             |         |
		//             |         |
		//             |         |
		//             |         |
		//             |         |
		//             |         |
		// y + height  0---------1

		for (char c : _text) {
			
			if (currentVertex + 4 >= maxLen)
			{
				return currentVertex;
			}

			if (c == '\n')
			{
				currentY += lineHeight;
				currentX = 0;
				last = 0;
				continue;
			}

			auto it = characters.find((wchar_t)c);
			if (it == characters.end()) continue;
			Char fc = it->second;

			int kerning = 0;
			auto it1 = kernings.find(last);
			if (it1 != kernings.end())
			{
				auto it2 = it1->second.find(last);
				if (it2 != it1->second.end())
				{
					kerning = it2->second;
				}
			}

			VertexPT1 v0, v1, v2, v3;

			v3.t1 = glm::vec2((float)fc.x / (float)scaleW, (float)fc.y / (float)scaleH);
			v1.t1 = v3.t1 + glm::vec2((float)fc.width / (float)scaleW, (float)fc.height / (float)scaleH);
			v0.t1 = glm::vec2(v3.t1.s, v1.t1.t);
			v2.t1 = glm::vec2(v1.t1.s, v3.t1.t);

			v0.p = glm::vec3(currentX + fc.xoffset + kerning, currentY - fc.yoffset - fc.height, 0);
			v2.p = v0.p + glm::vec3(fc.width, fc.height, 0);
			v1.p = glm::vec3(v2.p.x, v0.p.y, 0);
			v3.p = glm::vec3(v0.p.x, v2.p.y, 0);

			vb[currentVertex + 0] = v0;
			vb[currentVertex + 1] = v1;
			vb[currentVertex + 2] = v2;
			vb[currentVertex + 3] = v3;


			last = c;
			currentX += fc.xadvance;
			currentVertex += 4;
		}

		return currentVertex;
	}
}
