#include "stdafx.h"
#include "ShaderManagerFactory.hpp"

#include <string>
#include <sstream>

namespace qdrg {

	static std::map<ShaderManager::Uniform, std::string> ReadUniforms(std::stringstream& instruction, int numUniforms)
	{
		std::map<ShaderManager::Uniform, std::string> uniforms;
		std::string uniformType, uniformName;
		for (int i = 0; i < numUniforms; ++i)
		{
			instruction >> uniformType >> uniformName;
			if (uniformType == "WORLD_MATRIX")
			{
				uniforms[ShaderManager::Uniform::WORLD_MATRIX] = uniformName;
			}
			else if (uniformType == "VIEW_MATRIX")
			{
				uniforms[ShaderManager::Uniform::VIEW_MATRIX] = uniformName;
			}
			else if (uniformType == "PROJECTION_MATRIX")
			{
				uniforms[ShaderManager::Uniform::PROJECTION_MATRIX] = uniformName;
			}
			else if (uniformType == "COLOR")
			{
				uniforms[ShaderManager::Uniform::COLOR] = uniformName;
			}
			else if (uniformType == "TIME")
			{
				uniforms[ShaderManager::Uniform::TIME] = uniformName;
			}
		}
		return uniforms;
	}


	void ShaderManagerFactory::ExecuteScript(const std::string& _script)
	{
		std::stringstream ssScript(_script);

		std::string line;

		while (std::getline(ssScript, line))
		{
			if (line.size() == 0)
			{
				continue;
			}

			std::stringstream instruction(line);

			std::string action;
			instruction >> action;

			if (action == "load_vertex_shader")
			{
				std::string shaderName;

				instruction >> shaderName;

				LoadVertexShader(HashedString(shaderName));
			}
			else if (action == "load_vertex_shader-crc")
			{
				uint32_t shaderCRC;

				instruction >> shaderCRC;

				LoadVertexShader(HashedString(shaderCRC));
			}
			else if (action == "load_fragment_shader")
			{
				std::string shaderName;

				instruction >> shaderName;

				LoadFragmentShader(HashedString(shaderName));
			}
			else if (action == "load_fragment_shader-crc")
			{
				uint32_t shaderCRC;

				instruction >> shaderCRC;

				LoadFragmentShader(HashedString(shaderCRC));
			}
			else if (action == "load_geometry_shader")
			{
				std::string shaderName;

				instruction >> shaderName;

				LoadGeometryShader(HashedString(shaderName));
			}
			else if (action == "load_geometry_shader-crc")
			{
				uint32_t shaderCRC;

				instruction >> shaderCRC;

				LoadGeometryShader(HashedString(shaderCRC));
			}
			else if (action == "create_program")
			{
				std::string programName;
				std::string vertexShaderName;
				std::string fragmentShaderName;

				int numUniforms;

				instruction >> programName >> vertexShaderName >> fragmentShaderName >> numUniforms;

				auto uniforms = ReadUniforms(instruction, numUniforms);

				CreateProgram(HashedString(programName), uniforms, HashedString(vertexShaderName), HashedString(fragmentShaderName));
			}
			else if (action == "create_program_geometry")
			{
				std::string programName;
				std::string vertexShaderName;
				std::string geometryShaderName;
				std::string fragmentShaderName;

				int numUniforms;

				instruction >> programName >> vertexShaderName >> geometryShaderName >> fragmentShaderName >> numUniforms;

				auto uniforms = ReadUniforms(instruction, numUniforms);

				CreateProgram(HashedString(programName), uniforms, HashedString(vertexShaderName), HashedString(fragmentShaderName), HashedString(geometryShaderName));
			}
			else if (action == "create_program-crc")
			{
				uint32_t programCRC;
				uint32_t vertexShaderCRC;
				uint32_t fragmentShaderCRC;
				std::string uniformType, uniformName;

				int numUniforms;

				instruction >> programCRC >> vertexShaderCRC >> fragmentShaderCRC >> numUniforms;

				std::map<ShaderManager::Uniform, std::string> uniforms;
				for (int i = 0; i < numUniforms; ++i)
				{
					instruction >> uniformType >> uniformName;
					if (uniformType == "WORLD_MATRIX")
					{
						uniforms[ShaderManager::Uniform::WORLD_MATRIX] = uniformName;
					}
					else if (uniformType == "VIEW_MATRIX")
					{
						uniforms[ShaderManager::Uniform::VIEW_MATRIX] = uniformName;
					}
					else if (uniformType == "PROJECTION_MATRIX")
					{
						uniforms[ShaderManager::Uniform::PROJECTION_MATRIX] = uniformName;
					}
					else if (uniformType == "TIME")
					{
						uniforms[ShaderManager::Uniform::TIME] = uniformName;
					}
				}

				CreateProgram(HashedString(programCRC), uniforms, HashedString(vertexShaderCRC), HashedString(fragmentShaderCRC));
			}
			else if (action == "create_program_geometry")
			{
				uint32_t programCRC;
				uint32_t vertexShaderCRC;
				uint32_t geometryShaderCRC;
				uint32_t fragmentShaderCRC;
				std::string uniformType, uniformName;

				int numUniforms;

				instruction >> programCRC >> vertexShaderCRC >> geometryShaderCRC >> fragmentShaderCRC >> numUniforms;

				std::map<ShaderManager::Uniform, std::string> uniforms;
				for (int i = 0; i < numUniforms; ++i)
				{
					instruction >> uniformType >> uniformName;
					if (uniformType == "WORLD_MATRIX")
					{
						uniforms[ShaderManager::Uniform::WORLD_MATRIX] = uniformName;
					}
					else if (uniformType == "VIEW_MATRIX")
					{
						uniforms[ShaderManager::Uniform::VIEW_MATRIX] = uniformName;
					}
					else if (uniformType == "PROJECTION_MATRIX")
					{
						uniforms[ShaderManager::Uniform::PROJECTION_MATRIX] = uniformName;
					}
					else if (uniformType == "TIME")
					{
						uniforms[ShaderManager::Uniform::TIME] = uniformName;
					}
				}

				CreateProgram(HashedString(programCRC), uniforms, HashedString(vertexShaderCRC), HashedString(fragmentShaderCRC), HashedString(geometryShaderCRC));
			}


		}
	}

}
