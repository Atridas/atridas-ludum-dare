/*
 * StaticMesh.cpp
 *
 *  Created on: 02/06/2014
 *      Author: Atridas
 */

#include "stdafx.h"

#include "Mesh.hpp"

namespace qdrg {

Mesh::~Mesh() {
	// --
}


void Mesh::PrepareGPU_State(GLWrapper& _wrapper)
{
	if(m_vao.get() == nullptr) {
		assert(m_ib.get() == nullptr && m_vb.get() == nullptr);
		assert(GetVertexBuffer() != nullptr && GetIndexBuffer() != nullptr);

		m_vb = _wrapper.CreateVertexBuffer();
		m_ib = _wrapper.CreateIndexBuffer();

		_wrapper.BufferData(m_vb.get(), GetVertexBuffer(), GetVertexSize() * m_numVertices, GLWrapper::BufferUssageFrequency::STATIC, GLWrapper::BufferUssageNature::DRAW);
		_wrapper.BufferData(m_ib.get(), GetIndexBuffer(), sizeof(uint16_t)* m_numIndexes, GLWrapper::BufferUssageFrequency::STATIC, GLWrapper::BufferUssageNature::DRAW);

		std::vector<GLWrapper::VertexAttrib> attribList;
		if (HasPosition()) {
			GLWrapper::VertexAttrib attrib(
				0,
				3,
				GetVertexSize(),
				PositionOffset(),
				GLWrapper::Type::FLOAT,
				GLWrapper::Normalization::UNMODIFIED,
				m_vb.get());
			attribList.push_back(attrib);
		}
		if (HasNormal()) {
			GLWrapper::VertexAttrib attrib(
				1,
				3,
				GetVertexSize(),
				NormalOffset(),
				GLWrapper::Type::FLOAT,
				GLWrapper::Normalization::UNMODIFIED,
				m_vb.get());
			attribList.push_back(attrib);
		}
		if (HasTexCoords1()) {
			GLWrapper::VertexAttrib attrib(
				2,
				2,
				GetVertexSize(),
				TexCoords1Offset(),
				GLWrapper::Type::FLOAT,
				GLWrapper::Normalization::UNMODIFIED,
				m_vb.get());
			attribList.push_back(attrib);
		}
		if (HasTexCoords2()) {
			GLWrapper::VertexAttrib attrib(
				3,
				2,
				GetVertexSize(),
				TexCoords2Offset(),
				GLWrapper::Type::FLOAT,
				GLWrapper::Normalization::UNMODIFIED,
				m_vb.get());
			attribList.push_back(attrib);
		}
		if (HasExtraVector1()) {
			GLWrapper::VertexAttrib attrib(
				4,
				4,
				GetVertexSize(),
				ExtraVector1Offset(),
				GLWrapper::Type::FLOAT,
				GLWrapper::Normalization::UNMODIFIED,
				m_vb.get());
			attribList.push_back(attrib);
		}

		m_vao = _wrapper.CreateVertexArrayObject(attribList, m_ib.get());

		if(!IsDynamic()) {
			DeleteRAM_State();
		}

	} else {
		assert(m_ib.get() != nullptr && m_vb.get() != nullptr);
		assert(GetVertexBuffer() == nullptr && GetIndexBuffer() == nullptr);
	}
}

const void Mesh::Draw(GLWrapper& _wrapper) const
{
	assert(m_vao.get() != nullptr);

	_wrapper.Draw(m_vao.get(), GLWrapper::Primitive::TRIANGLES, 0, m_numIndexes);

}

} /* namespace qdrg */
