
#ifndef RENDERER_VERTEX_STRUCTS_HPP_
#define RENDERER_VERTEX_STRUCTS_HPP_

#include "Common/Common.hpp"

namespace qdrg {

#define RENDERER_VERTEX_STRUCTS_HPP__HAS_POSITION(V) \
	glm::vec3 p; \
	constexpr static bool HasPosition() { return true; } \
	constexpr uint16_t static PositionOffset() { return offsetof(V, p); }

#define RENDERER_VERTEX_STRUCTS_HPP__HAS_NORMAL(V) \
	glm::vec3 n; \
	constexpr static bool HasNormal() { return true; } \
	constexpr uint16_t static NormalOffset() { return offsetof(V, n); }

#define RENDERER_VERTEX_STRUCTS_HPP__HAS_TEX_COORDS_1(V) \
	glm::vec2 t1; \
	constexpr static bool HasTexCoords1() { return true; } \
	constexpr uint16_t static TexCoords1Offset() { return offsetof(V, t1); }

#define RENDERER_VERTEX_STRUCTS_HPP__HAS_TEX_COORDS_2(V) \
	glm::vec2 t2; \
	constexpr static bool HasTexCoords2() { return true; } \
	constexpr uint16_t static TexCoords2Offset() { return offsetof(V, t2); }

#define RENDERER_VERTEX_STRUCTS_HPP__HAS_EXTRA_VECTOR_1(V) \
	glm::vec4 e1; \
	constexpr static bool HasExtraVector1() { return true; } \
	constexpr uint16_t static ExtraVector1Offset() { return offsetof(V, e1); }



#define RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_POSITION() \
	constexpr static bool HasPosition() { return false; } \
	uint16 static PositionOffset() { assert(false); return -1; }

#define RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_NORMAL() \
	constexpr static bool HasNormal() { return false; } \
	uint16_t static NormalOffset() { assert(false); return -1; }

#define RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_1() \
	constexpr static bool HasTexCoords1() { return false; } \
	uint16_t static TexCoords1Offset() { assert(false); return -1; }

#define RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_2() \
	constexpr static bool HasTexCoords2() { return false; } \
	uint16_t static TexCoords2Offset() { assert(false); return -1; }

#define RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_EXTRA_VECTOR_1() \
	constexpr static bool HasExtraVector1() { return false; } \
	uint16_t static ExtraVector1Offset() { assert(false); return -1; }


struct VertexP {
	RENDERER_VERTEX_STRUCTS_HPP__HAS_POSITION(VertexP)
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_NORMAL()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_1()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_2()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_EXTRA_VECTOR_1()

	VertexP() = default;
	VertexP(glm::vec3 _p):p(_p){};
};


struct VertexPN {
	RENDERER_VERTEX_STRUCTS_HPP__HAS_POSITION(VertexPN)
	RENDERER_VERTEX_STRUCTS_HPP__HAS_NORMAL(VertexPN)
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_1()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_2()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_EXTRA_VECTOR_1()

	VertexPN() = default;
	VertexPN(glm::vec3 _p, glm::vec3 _n):p(_p),n(_n){};
};

struct VertexPNT1 {
	RENDERER_VERTEX_STRUCTS_HPP__HAS_POSITION(VertexPNT1)
	RENDERER_VERTEX_STRUCTS_HPP__HAS_NORMAL(VertexPNT1)
	RENDERER_VERTEX_STRUCTS_HPP__HAS_TEX_COORDS_1(VertexPNT1)
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_2()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_EXTRA_VECTOR_1()

	VertexPNT1() = default;
	VertexPNT1(glm::vec3 _p, glm::vec3 _n, glm::vec2 _t):p(_p),n(_n),t1(_t){};
};

struct VertexPT1 {
	RENDERER_VERTEX_STRUCTS_HPP__HAS_POSITION(VertexPT1)
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_NORMAL()
	RENDERER_VERTEX_STRUCTS_HPP__HAS_TEX_COORDS_1(VertexPT1)
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_2()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_EXTRA_VECTOR_1()

	VertexPT1() = default;
	VertexPT1(glm::vec3 _p, glm::vec2 _t) :p(_p), t1(_t){};
};

struct VertexPE1 {
	RENDERER_VERTEX_STRUCTS_HPP__HAS_POSITION(VertexPE1)
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_NORMAL()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_1()
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_2()
	RENDERER_VERTEX_STRUCTS_HPP__HAS_EXTRA_VECTOR_1(VertexPE1)

	VertexPE1() = default;
	VertexPE1(glm::vec3 _p, glm::vec4 _e) :p(_p), e1(_e){};
};

struct VertexPT1E1 {
	RENDERER_VERTEX_STRUCTS_HPP__HAS_POSITION(VertexPT1E1)
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_NORMAL()
	RENDERER_VERTEX_STRUCTS_HPP__HAS_TEX_COORDS_1(VertexPT1E1)
	RENDERER_VERTEX_STRUCTS_HPP__DOES_NOT_HAVE_TEX_COORDS_2()
	RENDERER_VERTEX_STRUCTS_HPP__HAS_EXTRA_VECTOR_1(VertexPT1E1)

	VertexPT1E1() = default;
	VertexPT1E1(glm::vec3 _p, glm::vec2 _t, glm::vec4 _e) :p(_p), t1(_t), e1(_e){};
};


}

#endif /* RENDERER_VERTEX_STRUCTS_HPP_ */
