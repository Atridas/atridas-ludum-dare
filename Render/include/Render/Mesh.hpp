/*
 * StaticMesh.hpp
 *
 *  Created on: 02/06/2014
 *      Author: Atridas
 */

#ifndef RENDERER_MESH_HPP_
#define RENDERER_MESH_HPP_

#include "Common/Common.hpp"

#include <typeinfo>

#include "VertexStructs.hpp"
#include "GLWrapper.hpp"

namespace qdrg {

template<class V>
class ConcreteMesh;

class Mesh {
public:
	virtual ~Mesh();

	uint16_t GetNumVertices() const {
		return m_numVertices;
	}
	uint32_t GetNumIndexes() const {
		return m_numIndexes;
	}

	void PrepareGPU_State(GLWrapper& _wrapper);

	const void Draw(GLWrapper& _wrapper) const;

	const GLWrapper::VertexArrayObject* GetVao() const {
		return m_vao.get();
	}

	virtual bool IsDynamic() const { return false; }

	virtual void FlushDynamicModifications(GLWrapper&) {}

	template<class V>
	ConcreteMesh<V>* CastToConcreteMesh();
	template<class V>
	const ConcreteMesh<V>* CastToConcreteMesh() const;
	template<class V>
	bool HasVertexType() const;

protected:
	Mesh(uint_fast16_t _numVertices, uint_fast32_t _numIndexes) :
			m_numVertices(_numVertices), m_numIndexes(_numIndexes) {
	}

	virtual bool HasPosition() const = 0;
	virtual bool HasNormal() const = 0;
	virtual bool HasTexCoords1() const = 0;
	virtual bool HasTexCoords2() const = 0;
	virtual bool HasExtraVector1() const = 0;

	virtual uint_fast16_t PositionOffset() const = 0;
	virtual uint_fast16_t NormalOffset() const = 0;
	virtual uint_fast16_t TexCoords1Offset() const = 0;
	virtual uint_fast16_t TexCoords2Offset() const = 0;
	virtual uint_fast16_t ExtraVector1Offset() const = 0;

	virtual uint8_t* GetVertexBuffer() const = 0;
	virtual uint16_t* GetIndexBuffer() const = 0;

	virtual uint_fast16_t GetVertexSize() const = 0;
	virtual const std::type_info& GetVertexType() const = 0;

	virtual void DeleteRAM_State() = 0;

	const GLWrapper::VertexBuffer* GetVertexBufferObject() const { return m_vb.get(); }

private:
	uint_fast16_t m_numVertices;
	uint_fast32_t m_numIndexes;

	std::unique_ptr<GLWrapper::VertexArrayObject> m_vao;
	std::unique_ptr<GLWrapper::IndexBuffer> m_ib;
	std::unique_ptr<GLWrapper::VertexBuffer> m_vb;
};

template<class V>
class ConcreteMesh: public Mesh {
public:
	ConcreteMesh(std::unique_ptr<V[]> _vertexBuffer, uint_fast16_t _numVertices,
			std::unique_ptr<uint16_t[]> _indexBuffer, uint_fast32_t _numIndexes)
	:Mesh(_numVertices, _numIndexes)
	,m_vertexBuffer(std::move(_vertexBuffer))
	,m_indexBuffer(std::move(_indexBuffer))
	{ }

	virtual ~ConcreteMesh() = default;

	virtual void ReplaceVertex(const V& _vertex, uint16_t _position) {};

protected:

	constexpr bool HasPosition() const {
		return V::HasPosition();
	}
	constexpr bool HasNormal() const {
		return V::HasNormal();
	}
	constexpr bool HasTexCoords1() const {
		return V::HasTexCoords1();
	}
	constexpr bool HasTexCoords2() const {
		return V::HasTexCoords2();
	}
	constexpr bool HasExtraVector1() const {
		return V::HasExtraVector1();
	}

	constexpr uint_fast16_t PositionOffset() const {
		return V::PositionOffset();
	}
	constexpr uint_fast16_t NormalOffset() const {
		return V::NormalOffset();
	}
	constexpr uint_fast16_t TexCoords1Offset() const {
		return V::TexCoords1Offset();
	}
	constexpr uint_fast16_t TexCoords2Offset() const {
		return V::TexCoords2Offset();
	}
	constexpr uint_fast16_t ExtraVector1Offset() const {
		return V::ExtraVector1Offset();
	}

	uint8_t* GetVertexBuffer() const {
		return (uint8_t*) m_vertexBuffer.get();
	}
	uint16_t* GetIndexBuffer() const {
		return m_indexBuffer.get();
	}

	uint_fast16_t GetVertexSize() const {
		return sizeof(V);
	}
	;
	const std::type_info& GetVertexType() const {
		return typeid(ConcreteMesh);
	}
	;

	void DeleteRAM_State() {
		m_vertexBuffer.reset();
		m_indexBuffer.reset();
	}

private:
	std::unique_ptr<V[]> m_vertexBuffer;
	std::unique_ptr<uint16_t[]> m_indexBuffer;

};


template<class V>
class ConcreteDynamicMesh: public ConcreteMesh<V> {
public:
	ConcreteDynamicMesh(std::unique_ptr<V[]> _vertexBuffer, uint_fast16_t _numVertices,
			std::unique_ptr<uint16_t[]> _indexBuffer, uint_fast32_t _numIndexes);

	~ConcreteDynamicMesh();

	bool IsStatic() const { return true; }

	void ReplaceVertex(const V& _vertex, uint16_t _position);
	void FlushDynamicModifications(GLWrapper&);

private:

	struct NewVertexCommand {
		V vertex;
		uint16_t vertexIndex;

		NewVertexCommand():vertex(), vertexIndex(0) {};
	};


	NewVertexCommand m_NewVertices[5], *m_NewVerticesDynamic;
	int m_NewVerticesDynamicSize, m_ValidNewVertices;
};


#include "Mesh.inl"

} /* namespace qdrg */


#endif /* RENDERER_MESH_HPP_ */
