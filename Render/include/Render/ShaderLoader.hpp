/*
 * ShaderLoader.h
 *
 *  Created on: 01/06/2014
 *      Author: Atridas
 */

#ifndef RENDERER_SHADERLOADER_HPP_
#define RENDERER_SHADERLOADER_HPP_

#include "Common/Common.hpp"

namespace qdrg {

class ShaderLoader {
public:
	ShaderLoader();
	~ShaderLoader();

	std::string LoadShader(HashedString _Key);
};

} /* namespace qdrg */
#endif /* RENDERER_SHADERLOADER_HPP_ */
