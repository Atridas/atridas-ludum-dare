/*
 * GLWrapper.h
 *
 *  Created on: 29/05/2014
 *      Author: Atridas
 */

#pragma once

#ifndef RENDERER_GLWRAPPER_HPP_
#define RENDERER_GLWRAPPER_HPP_

#include <Common/Common.hpp>

// ??
typedef unsigned int GLuint;

namespace qdrg {

class GLWrapper {
public:

	struct VertexBuffer;
	struct IndexBuffer;

	struct VertexArrayObject;

	struct Texture2DObject;

	static const int MAX_TEXTURE_UNITS = 16;

	struct BufferReplaceData {
		uintptr_t offset, size;
		void *data;
	};

	enum class BufferUssageFrequency {
		STATIC, STREAM, DYNAMIC
	};

	enum class BufferUssageNature {
		DRAW, READ, COPY
	};

	enum class Primitive {
		TRIANGLES, LINES
	};

	enum class Type {
		BYTE,
		INT16,
		UINT16,
		INT32,
		UINT32,
		FLOAT
	};

	enum class Normalization {
		NORMALIZE,
		UNMODIFIED
	};

	enum class TextureFilter {
		NEAREST,
		LINEAR,
		TRILINEAR
	};

	struct VertexAttrib {
		uint16_t index, size, stride, offset;
		Type type;
		Normalization normalize;
		const VertexBuffer* buffer;

		VertexAttrib(uint16_t _index, uint16_t _size, uint16_t _stride, uint16_t _offset, Type _type, Normalization _normalize, const VertexBuffer* _buffer)
		:index(_index)
		,size(_size)
		,stride(_stride)
		,offset(_offset)
		,type(_type)
		,normalize(_normalize)
		,buffer(_buffer)
		{}
	};

	enum class DepthMode
	{
		DISABLED,
		ENABLED,
		READ_ONLY
	};

public:
	GLWrapper();
	virtual ~GLWrapper();

	void DeleteUnusedResources();

	void SetClearColor(const glm::vec3& _color) {SetClearColor(glm::vec4(_color, 1));};
	void SetClearColor(const glm::vec4& _color);

	void SetViewport(const glm::ivec2& _origin, const glm::ivec2& _size);
	void Clear(bool _ColorBuffer = true, bool _DepthBuffer = true, bool _StencilBuffer = false);

	void SetDepthTestMode(DepthMode _depthMode);

	std::unique_ptr<VertexBuffer> CreateVertexBuffer();
	std::unique_ptr<IndexBuffer>  CreateIndexBuffer();

	void BufferData(const VertexBuffer* _buffer, const void* _bufferData, size_t _bufferSize, BufferUssageFrequency _freq = BufferUssageFrequency::STATIC, BufferUssageNature _nature = BufferUssageNature::DRAW);
	void BufferData(const IndexBuffer* _buffer, const void* _bufferData, size_t _bufferSize, BufferUssageFrequency _freq = BufferUssageFrequency::STATIC, BufferUssageNature _nature = BufferUssageNature::DRAW);

	void ReplaceBufferData(const VertexBuffer* _buffer, const BufferReplaceData* _bufferReplaceData, uint_fast32_t _numChunks);

	std::unique_ptr<VertexArrayObject> CreateVertexArrayObject(const std::vector<VertexAttrib>& _vertexAttribs, const IndexBuffer* _indexBuffer = nullptr);


	std::unique_ptr<Texture2DObject> CreateTexture2D(bool _withMipmaps);

	void SetTextureData(const Texture2DObject* _texture, int _internalFormat, int _externalFormat, int _type, int levels, int width, int height, int *widths, int *heights, uint8_t **data);
	void SetTextureCompressedData(const Texture2DObject* _texture, int _format, int levels, int width, int height, int *widths, int *heights, int *sizes, uint8_t **data);

	void SetTextureFilter(const Texture2DObject* _texture, TextureFilter _filterMode);

	void BindTexture(int _unit, const Texture2DObject* _texture);

	void Draw(const VertexArrayObject* _vao, Primitive _primitive, uint32_t _firstIndex, uint32_t _indexCount);
	void DrawUnindexed(const VertexArrayObject* _vao, Primitive _primitive, uint32_t _firstIndex, uint32_t _indexCount);

private:
	GLWrapper(const GLWrapper&) {};
	GLWrapper& operator=(const GLWrapper&) {};
	void BindVertexBuffer(const VertexBuffer* _vb);
	void BindIndexBuffer(const IndexBuffer* _vb);
	void BindVertexArrayObject(const VertexArrayObject* _vao);
	void SetTextureUnit(int _unit);

	struct GenericGLObject;

	static void AddObjectToBeDeleted(
			const GLWrapper::GenericGLObject& _object,
			GLWrapper::GenericGLObject** objectsToBeDeleted_,
			int& objectsToBeDeletedValidElements_,
			int& objectsToBeDeletedSize_
			);
	void AddBufferToBeDeleted   (const GenericGLObject& _buffer)   { AddObjectToBeDeleted(_buffer , &m_BuffersToBeDeleted , m_BuffersToBeDeletedSize , m_BuffersToBeDeletedValidElements ); }
	void AddVAOToBeDeleted      (const GenericGLObject& _vao)      { AddObjectToBeDeleted(_vao    , &m_VAOsToBeDeleted    , m_VAOsToBeDeletedSize    , m_VAOsToBeDeletedValidElements    ); }
	void AddTextureToBeDeleted  (const GenericGLObject& _texture)  { AddObjectToBeDeleted(_texture, &m_TexturesToBeDeleted, m_TexturesToBeDeletedSize, m_TexturesToBeDeletedValidElements); }

	glm::vec4 m_ClearColor;
	glm::ivec2 m_ViewportOrigin, m_ViewportSize;

	DepthMode m_CurrentDepthMode;

	GenericGLObject* m_BuffersToBeDeleted;
	int m_BuffersToBeDeletedSize, m_BuffersToBeDeletedValidElements;
	GenericGLObject* m_VAOsToBeDeleted;
	int m_VAOsToBeDeletedSize, m_VAOsToBeDeletedValidElements;
	GenericGLObject* m_TexturesToBeDeleted;
	int m_TexturesToBeDeletedSize, m_TexturesToBeDeletedValidElements;

	const VertexBuffer* m_bindedVertexBuffer;
	const IndexBuffer*  m_bindedIndexBuffer;
	const VertexArrayObject* m_bindedVAO;
	int m_CurrentTextureUnit;
	const Texture2DObject* m_bindedTexture2Ds[MAX_TEXTURE_UNITS];
};

#include "GLWrapper.inl"

} /* namespace qdrg */
#endif /* RENDERER_GLWRAPPER_HPP_ */
