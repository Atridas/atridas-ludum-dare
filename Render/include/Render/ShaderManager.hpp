/*
 * ShaderManager.hpp
 *
 *  Created on: 01/06/2014
 *      Author: Atridas
 */

#ifndef RENDERER_SHADERMANAGER_HPP_
#define RENDERER_SHADERMANAGER_HPP_

#include "Common/Common.hpp"

namespace qdrg {

	struct ShaderManagerData;
	class GLWrapper;

	class ShaderManager {
	public:

		enum class Uniform {
			WORLD_MATRIX,
			VIEW_MATRIX,
			PROJECTION_MATRIX,

			ALBEDO_TEXTURE,

			COLOR,

			TIME,

			NUM_UNIFORMS
		};

		virtual ~ShaderManager();

		void DeleteResources();

		void UseProgram(HashedString _program);

		void SetWorldMatrix(const glm::mat4& m);
		void SetViewMatrix(const glm::mat4& m);
		void SetProjectionMatrix(const glm::mat4& m);

		void SetColor(const glm::vec4& c);
		void SetTime(float t);

	protected:
		ShaderManager(ShaderManagerData* _dataLoc);

		ShaderManagerData* m_data;
		bool m_ResourcesDeleted;
		HashedString m_CurrentProgram;
		size_t m_CurrentProgramIndex;
	};

} /* namespace qdrg */
#endif /* RENDERER_SHADERMANAGER_HPP_ */
