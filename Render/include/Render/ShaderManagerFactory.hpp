#pragma once

#include "Common/Common.hpp"
#include "ShaderManager.hpp"

namespace qdrg {

	class ShaderManager;

	class ShaderManagerFactory {
	public:
		virtual ~ShaderManagerFactory() {};

		virtual void LoadVertexShader(HashedString _ShaderKey) = 0;
		virtual void LoadGeometryShader(HashedString _ShaderKey) = 0;
		virtual void LoadFragmentShader(HashedString _ShaderKey) = 0;

		virtual void CreateProgram(HashedString _ProgramKey, const std::map<ShaderManager::Uniform, std::string>& _Uniforms, HashedString _Vertex, HashedString _Fragment, HashedString _Geometry = HashedString()) = 0;

		void ExecuteScript(const std::string& _script);

		virtual std::unique_ptr<ShaderManager> CreateShaderManager(GLWrapper& wrapper) = 0;

		static std::unique_ptr<ShaderManagerFactory> CreateShaderManagerFactory();

	protected:
		ShaderManagerFactory() = default;
	};

}
