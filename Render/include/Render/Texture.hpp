#pragma once

#include "Common/Common.hpp"

#include "GLWrapper.hpp"


namespace qdrg {

	class Texture
	{
		static const int MAX_MIPMAP_LEVELS = 16;

	public:
		Texture();
		~Texture();

		void SetPath(const std::string _Path) { m_Path = _Path; }
		void LoadFromDisk();
		void PrepareGPU_State(GLWrapper& _wrapper);

		const GLWrapper::Texture2DObject* GetTextureObject() const { return m_TextureObject.get(); };

	private:
		std::string m_Path;
		std::unique_ptr<uint8_t[]> m_Data[MAX_MIPMAP_LEVELS];
		int m_Widths[MAX_MIPMAP_LEVELS];
		int m_Heights[MAX_MIPMAP_LEVELS];
		int m_Sizes[MAX_MIPMAP_LEVELS];
		int m_Levels, m_Format, m_Width, m_Height;

		std::unique_ptr<GLWrapper::Texture2DObject> m_TextureObject;
	};

	class TextureManager
	{
	public:
		virtual ~TextureManager() {};
		virtual const Texture* GetTexture(HashedString _Name) const = 0;
	};
}
