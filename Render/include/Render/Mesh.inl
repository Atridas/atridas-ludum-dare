/*
 * Mesh.inl
 *
 *  Created on: 14/06/2014
 *      Author: Atridas
 */

#ifndef RENDERER_MESH_INL_
#define RENDERER_MESH_INL_

//#include "Mesh.hpp"

//namespace qdrg {

template<class V>
ConcreteMesh<V>* Mesh::CastToConcreteMesh() {
	//assert(HasVertexType<V>());
	return dynamic_cast<ConcreteMesh<V>*>(this);
}

template<class V>
const ConcreteMesh<V>* Mesh::CastToConcreteMesh() const {
	//assert(HasVertexType<V>());
	return dynamic_cast<const ConcreteMesh<V>*>(this);
}

template<class V>
bool Mesh::HasVertexType() const {
	return typeid(V) == GetVertexType();
}

template<class V>
ConcreteDynamicMesh<V>::ConcreteDynamicMesh(std::unique_ptr<V[]> _vertexBuffer, uint_fast16_t _numVertices,
			std::unique_ptr<uint16_t[]> _indexBuffer, uint_fast32_t _numIndexes)
	:ConcreteMesh<V>(std::move(_vertexBuffer), _numVertices, std::move(_indexBuffer), _numIndexes)
	,m_NewVerticesDynamic(nullptr)
	,m_NewVerticesDynamicSize(0)
	,m_ValidNewVertices(0)
	{}


template<class V>
ConcreteDynamicMesh<V>::~ConcreteDynamicMesh()
{
	if(m_NewVerticesDynamicSize > 0) {
		delete[] m_NewVerticesDynamic;
	}
}

template<class V>
void ConcreteDynamicMesh<V>::ReplaceVertex(const V& _vertex, uint16_t _position)
{
	assert(m_ValidNewVertices <= m_NewVerticesDynamicSize + 5);

	if(m_ValidNewVertices == 0) {
		m_NewVertices[0].vertex = _vertex;
		m_NewVertices[0].vertexIndex = _position;
		m_ValidNewVertices= 1;
	} else if(m_ValidNewVertices < 4) {
		bool inserted = false;

		NewVertexCommand aux;

		for(int i = 0; i < m_ValidNewVertices; i++) {
			if(inserted) {
				NewVertexCommand aux2 = m_NewVertices[i+1];
				m_NewVertices[i+1] = aux;
				aux = aux2;
			} else {
				if(m_NewVertices[i].vertexIndex == _position) {
					m_NewVertices[i].vertex = _vertex;
					return;
				} else if(m_NewVertices[i].vertexIndex > _position) {
					aux = m_NewVertices[i];
					m_NewVertices[i].vertex = _vertex;
					m_NewVertices[i].vertexIndex = _position;
					inserted = true;
				}
			}
		}
		if(inserted) {
			m_NewVertices[m_ValidNewVertices] = aux;
			m_ValidNewVertices++;
		}
	} else if(m_NewVerticesDynamicSize == 0) {
		m_NewVerticesDynamic = new NewVertexCommand[5];
		m_NewVerticesDynamicSize = 5;
		ReplaceVertex(_vertex, _position);
	} else if(m_ValidNewVertices == m_NewVerticesDynamicSize + 5) {

		NewVertexCommand *aux = new NewVertexCommand[m_NewVerticesDynamicSize + 5];
		for(int i = 0; i < m_NewVerticesDynamicSize; i++) {
			aux[i] = m_NewVerticesDynamic[i];
		}
		delete[] m_NewVerticesDynamic;
		m_NewVerticesDynamic = aux;
		m_NewVerticesDynamicSize += 5;
		ReplaceVertex(_vertex, _position);

	} else {
		bool inserted = false;

		NewVertexCommand aux;

		for(int i = 0; i < m_ValidNewVertices; i++) {
			if(inserted) {
				if(i < 4) {
					NewVertexCommand aux2 = m_NewVertices[i+1];
					m_NewVertices[i+1] = aux;
					aux = aux2;
				} else {
					NewVertexCommand aux2 = m_NewVerticesDynamic[i-4];
					m_NewVerticesDynamic[i-4] = aux;
					aux = aux2;
				}
			} else {
				if(i < 5) {
					if(m_NewVertices[i].vertexIndex == _position) {
						m_NewVertices[i].vertex = _vertex;
						return;
					} else if(m_NewVertices[i].vertexIndex > _position) {
						aux = m_NewVertices[i];
						m_NewVertices[i].vertex = _vertex;
						m_NewVertices[i].vertexIndex = _position;
						inserted = true;
					}
				} else {
					if(m_NewVerticesDynamic[i-5].vertexIndex == _position) {
						m_NewVerticesDynamic[i-5].vertex = _vertex;
						return;
					} else if(m_NewVerticesDynamic[i-5].vertexIndex > _position) {
						aux = m_NewVerticesDynamic[i-5];
						m_NewVerticesDynamic[i-5].vertex = _vertex;
						m_NewVerticesDynamic[i-5].vertexIndex = _position;
						inserted = true;
					}
				}
			}
		}
		if(inserted) {
			m_NewVerticesDynamic[m_ValidNewVertices-5] = aux;
			m_ValidNewVertices++;
		}
	}
}

template<class V>
void ConcreteDynamicMesh<V>::FlushDynamicModifications(GLWrapper& wrapper)
{
	if(m_ValidNewVertices > 0) {
		GLWrapper::BufferReplaceData *buffer = new GLWrapper::BufferReplaceData[m_ValidNewVertices];

		for(int i = 0; i < m_ValidNewVertices; i++) {
			if(i < 5) {
				buffer[i].offset = m_NewVertices[i].vertexIndex * sizeof(V);
				buffer[i].size = sizeof(V);
				buffer[i].data = &(m_NewVertices[i].vertex);
			} else {
				buffer[i].offset = m_NewVerticesDynamic[i-5].vertexIndex * sizeof(V);
				buffer[i].size = sizeof(V);
				buffer[i].data = &(m_NewVerticesDynamic[i-5].vertex);
			}
		}

		wrapper.ReplaceBufferData(Mesh::GetVertexBufferObject(), buffer, m_ValidNewVertices);

		m_ValidNewVertices = 0;
		delete[] buffer;
	}
}

//};

#endif /* MESH_INL_ */
