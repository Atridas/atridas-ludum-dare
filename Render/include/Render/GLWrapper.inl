/*
 * GLWrapper.inl
 *
 *  Created on: 07/06/2014
 *      Author: Atridas
 */

#ifndef RENDERER_GLWRAPPER_INL_
#define RENDERER_GLWRAPPER_INL_

#include "GLWrapper.hpp"

struct GLWrapper::GenericGLObject{
	GenericGLObject(GLuint _bo)
	:bo(_bo)
	{ };
	GenericGLObject():bo(0) {};

	GLuint bo;
};

struct GLWrapper::VertexBuffer {
	~VertexBuffer() {
		wrapper->AddBufferToBeDeleted(GenericGLObject(bo));
	}

private:
	VertexBuffer(GLuint _bo, GLWrapper *_wrapper)
			:bo(_bo)
			,wrapper(_wrapper)
			{ };


	GLuint bo;
	GLWrapper *wrapper;

	friend class GLWrapper;
};

struct GLWrapper::IndexBuffer{
	~IndexBuffer() {
		wrapper->AddBufferToBeDeleted(GenericGLObject(bo));
	}

private:
	IndexBuffer(GLuint _bo, GLWrapper *_wrapper)
			:bo(_bo)
			,wrapper(_wrapper)
			{ };


	GLuint bo;
	GLWrapper *wrapper;

	friend class GLWrapper;
};

struct GLWrapper::VertexArrayObject{
	~VertexArrayObject() {
		wrapper->AddVAOToBeDeleted(GenericGLObject(vao));
	}

private:
	VertexArrayObject(GLWrapper *_wrapper);

	GLuint vao;
	GLWrapper *wrapper;
	bool indexed;

	friend class GLWrapper;
};


struct GLWrapper::Texture2DObject
{
	~Texture2DObject() {
		wrapper->AddTextureToBeDeleted(GenericGLObject(texture));
	}

private:
	Texture2DObject(GLuint _to, GLWrapper *_wrapper, bool _withMipmaps) : texture(_to), wrapper(_wrapper), withMipmaps(_withMipmaps) {};

	GLuint texture;
	GLWrapper *wrapper;
	bool withMipmaps;

	friend class GLWrapper;
};


#endif /* GLWRAPPER_INL_ */
