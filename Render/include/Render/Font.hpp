#pragma once

#include "Common/Common.hpp"

#include "VertexStructs.hpp"

namespace qdrg {

	class Font
	{
	public:
		Font() {};
		~Font() {};

		void ParseTextFile(const std::string& _Path);
		void ParseText(const std::string& _fnt);

		//  0      3---------2
		//  0      |         |
		//  0      |         |
		//  0      |         |
		//  0      |         |
		//  0      |         |
		//  0      |         |
		//  0      0---------1
		int FillVertexBufferWithText(const std::string& _text, VertexPT1* vb, int maxLen);

	private:

		// common info
		int lineHeight, base, scaleW, scaleH, pages, packed, alphaChnl, redChnl, greenChnl, blueChnl;

		std::vector<HashedString> texturesPerPage;

		struct Char {
			uint16_t x, y, width, height;
			int16_t xoffset, yoffset, xadvance;
			uint8_t page, chnl;
		};

		std::unordered_map<wchar_t, Char> characters;


		std::unordered_map<wchar_t, std::unordered_map<wchar_t, int>> kernings;

	};

}
