#pragma once

#include "Common/Common.hpp"

namespace qdrg {

	class Camera
	{
	public:
		Camera();
		~Camera();

		void SetWithCenter(const glm::vec3& _Position, const glm::vec3& _Center, const glm::vec3& _Up)
		{
			m_ViewValid = false; m_Position = _Position; m_Center = _Center; m_Up = _Up;
		};
		void SetWithDirection(const glm::vec3& _Position, const glm::vec3& _Direction, const glm::vec3& _Up)
		{
			m_ViewValid = false; m_Position = _Position; m_Center = _Position + _Direction; m_Up = _Up;
		};


		void SetPerspective(float _Fov, float _Width, float _Height, float _Near, float _Far)
		{
			m_ProjectionValid = false; m_Fov = _Fov; m_Width = _Width; m_Height = _Height; m_Near = _Near; m_Far = _Far;
		}

		void SetPosition(const glm::vec3& _Position) { m_ViewValid = false; m_Position = _Position; }
		void SetCenter(const glm::vec3& _Center) { m_ViewValid = false; m_Center = _Center; }
		void SetUp(const glm::vec3& _Up) { m_ViewValid = false; m_Up = _Up; }

		void SetFov(float _Fov) { m_ProjectionValid = false; m_Fov = _Fov; }
		void SetWidth(float _Width) { m_ProjectionValid = false; m_Width = _Width; }
		void SetHeight(float _Height) { m_ProjectionValid = false; m_Height = _Height; }
		void SetNear(float _Near) { m_ProjectionValid = false; m_Near = _Near; }
		void SetFar(float _Far) { m_ProjectionValid = false; m_Far = _Far; }

		const glm::vec3& GetPosition() const { return m_Position; }
		const glm::vec3& GetCenter() const { return m_Center; }
		const glm::vec3& GetUp() const { return m_Up; }

		float GetFov() const { return m_Fov; }
		float GetWidth() const { return m_Width; }
		float GetHeight() const { return m_Height; }
		float GetNear() const { return m_Near; }
		float GetFar() const { return m_Far; }



		const glm::mat4& GetViewMatrix() const;
		const glm::mat4& GetProjectionMatrix() const;

	private:

		glm::vec3 m_Position, m_Center, m_Up;
		float m_Fov, m_Width, m_Height, m_Near, m_Far;

		mutable bool m_ViewValid, m_ProjectionValid;

		mutable glm::mat4 m_View, m_Projection;

	};

	static_assert(std::is_standard_layout<Camera>::value, "Camera should have standard layout");

}
